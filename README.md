# Plateforme web de restauration pour restaurants indépendants

Un projet de gestion de menu en ligne pour restaurants. Créé dans le cadre du cours 420-DM1-DM à l'automne 2020.

## Table des matières

-   [Informations](#informations)
-   [Technologies](#technologies)
-   [Installation](#installation)
-   [Fonctionnalités](#fonctionnalités)
-   [Statut](#statut)
-   [Contact](#contact)

## Informations

Ce projet est un projet de fin de DEC, ayant pour but de démontrer l'aptitude des étudiants face au marché du travail.

Le projet, une fois achevé, permettra à des restaurants indépendants de pouvoir se créer un compte utilisateur, s'enregistrer comme restaurant, et finalement y ajouter ses informations de restaurant ainsi que les articles de son menu.

Des clients pourront naviguer sur la page de leur menu, sélectionner des articles de celle-ci, les ajouter à un panier, et pourront créer une commande en ligne pouvant être livrée, ou être préparée pour emporter.

## Technologies

-   MongoDB (mongoose) v5.10.15
-   Express.js v4.17.1
-   React.js v17.0.1
-   Node.js v12.18.3
-   Typescript v4.1.5

## Installation

### Préalables

-   Vous devez avoir installé Node.js sur votre machine et l'avoir mis dans votre "path". https://nodejs.org/en/ 

### Téléchargement du projet

Téléchargez le projet à partir de https://gitlab.com/SoDynamiX/projet-plateforme-web-de-restauration

Demandez aux administrateurs du projet de vous autoriser l'accès à la base de données (nous avons besoin de votre adresse courriel). Vous recevrez par courriel un lien d'invitation à notre organisation, vous permettant donc d'accéder aux données de la plateforme.

#### Backend

Entrez les commandes suivantes dans un terminal pointant vers le répertoire "Application/app-restaurant/backend":

```
npm i
npm i nodemon
npm i -g typescript
```

Vous êtes maintenant prêt à démarrer le backend du projet:

```
npm run start
```

#### Frontend

Entrez les commandes suivantes dans un terminal pointant vers le répertoire "Application/app-restaurant/frontend":

```
npm i
npm i -g typescript
```

Vous êtes maintenant prêt à démarrer le frontend du projet:

```
npm run start
```

Pour accéder au frontend, dans un navigateur, allez à l'adresse **localhost:3000**


### Tests du projet

#### Insomnia
Notre frontend est en plein développement et de nouvelles fonctionnalités arriveront dans un futur proche. Cependant, il existe des méthodes vous permettant de tester des requêtes HTTP sur notre backend.

Pour pouvoir envoyer des requêtes HTTP manuellement à notre application:

- Installez Insomnia Core: https://insomnia.rest/download/
- Créez une requête
- Choisissez la méthode de requête adéquate
- Entrez la route adéquate pour la requête souhaitée
- Sélectionnez "JSON" comme format de texte
- Si nécessaire, entrez les données demandées par la requête. Exemple de création d'un "Restaurant":
    ```
    {
    "nom": "La cantine à Benoît",
    "numeroTelephone": "819-474-8888",
    "cheminLogo": "C:\test",
    "courriel": "Cantinebenoit@hotmail.com"
    }
    ```
- Cliquez sur "Send" pour envoyer votre requête

#### Jest
L'entièreté des méthodes de tous les contrôleurs ont eu un test écrit pour tester tous les chemins possibles. À noter que certains contrôleurs n'ont pas de tests pour leurs méthodes getOneObject<T>ById et getAllObjets<T>. L'utilisation de MongoDB comme base de données nous permet de sauter certaines méthodes qui seraient normalement requises pour un contrôleur traditionnel. Par exemple, si on veut accéder à un objet avec la méthode getOneObjet<T> et que cet objet est embeded dans un autre objet, en MongoDB et que l'on détient le document parent du document qu'on veut chercher, il suffit simplement d'accéder directement à l'objet en faisant, par exemple: Objet.listeQuelconque.find(objet => objet.id === monIdRecherche). 

Pour accéder aux résultats des tests unitaires, il suffit de naviguer dans le dossier "Application/app-restaurant/backend" et d'y entrer cette commande:

```
npm test
```

Après une longue attente, tous les tests seront exécutés et le résultat final sera affiché. Il vous sera ainsi possible de voir quelles méthodes ont échouées et les résultats de ces échecs.

Le code des tests unitaires se situe dans le dossier "Application/app-restaurant/backend/tests".

### Accès à la documentation du projet

Pour avoir accès à la documentation de notre application (toujours dans "Application/app-restaurant/backend"):

```
npm run openapi
```

Dans un navigateur, allez à l'adresse **localhost:7777**


## Fonctionnalités

Fonctionnalités déjà réalisées:

-   Backend pour le restaurant, les articles, les commandes, les clients et les adresses.
-   Frontend pour la page d'accueil ainsi qu'une grande partie de la page d'achat et de modification d'un menu.

## Statut

Le projet est: _in progress_

## Quelques notes concernant le cours 420-DM2
Voici les fonctionnalités implémentées dans la version du 5 mars 2021:
```
    -   Ajout, modification ainsi que suppression d'article.
    -   Ajout d'extras, catégories variantes et de contraintes alimentaires.
    -   Ajout ainsi que suppression d'articles dans le panier.
```

Voici quelques fonctionnalités ne fonctionnant pas dans le cycle courant:
```
    -   Lorsqu'on modifie un article et qu'on change sa catégorie, l'article ne change pas de catégorie.
    -   Lorsqu'on ajoute une variante, le programme plante.
    -   Lors de l'ajout d'un article dans le panier et qu'on clique sur une carte dans l'une des catégories de l'article et qu'on procède à la prochaine catégorie, les autres cartes sont mises hors service.
```

Toute fonctionnalité présentant un chemin d'image utilise une image générique pour représenter le produit final. La gestion d'image sera prise en charge par un serveur externe n'ayant que la seule utilité d'envoyer des images sur un réseau local où le serveur "backend" sera hébergé.

Toutes les méthodes getOneObject<T>ByID ainsi que getAllObjets<T>ById ne sont pas présentement utilisées dans l'application. Étant donné la structure imposée par une base de données par document, il n'est pas nécessaire d'avoir des fonctions comme getOne et getAll. 

Pour des questions d'implémentation d'interfaces, les méthodes getOne et getAll ne seront pas mises en commentaire.

Dans une base de donnée par document, étant donné la structure imbriquée d'objets un à l'intérieur de l'autre, il est possible de simplement accéder à une partie spécifique d'une entité comme on le ferait en code avec un objet standard. Par exemple, si une personne détient une voiture, en SQL il nous faudrait sauvegarder l'id de la voiture de la personne dans la table "personne", mais en base de données par document, il suffit simplement d'accéder à la voiture en faisant: personne.voiture.

**À BIEN NOTER**


Aucune vérification de champs n'a été faite ni dans le "backend", ni dans le "frontend". Entrer de fausses données causera le plantage de l'application. Procédez à vos propres risques.




## Contact

Créé par [@SoDynamiX](https://gitlab.com/SoDynamiX), [@Tatsuni](https://gitlab.com/Tatsuni) et [@SanapoDeSens](https://gitlab.com/SanapoDeSens)

