### Résumé du problème
*Résumez précisément en quoi consiste le problème:*

### Étape à reproduire
*Comment peut-on reproduire le problème:*


### Quel est le comportement *erroné* actuel
*Qu'est-ce qui se passe qui n'est pas correct:*


###  Quel est le comportement *correct* attendu
*Que devrait-il se passer au lieu de se qui se passe présentement:*


### Journaux ou impressions d'écran
*Ajoutez les journaux et impressions d'écran permettant de clarifier ou de visualiser plus facilement le problème:*


### Correctifs possibles
*Si vous pensez avoir une piste de solution ou une suggestion pour diriger la correction du problème, écrivez la ici:*


### Précision du type de bug
*Veuillez sélectionner ci-dessous le type de bug convenant le plus au bug précisé plus haut. Si aucun 'label' ne le représente, n'en sélectionnez pas.*

/label ~Bug
