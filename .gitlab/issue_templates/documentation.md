### Erreurs de documentation trouvées
*Utilisez la formule suivante pour indiquer une ou plusieurs erreurs de documentation trouvées dans notre projet:*

*"Le chemin du document où se trouve l'erreur" | "Précision sur l'erreur (numéro de ligne/paragraphe)" | "Quel est le problème" | "La phrase/section contenant l'erreur" | "La correction proposée"*

*Exemple: "projet/application/readme.md" | Ligne 19 | Il manque de précision sur un terme. | "J'aime les pommes!" | "J'aime les pommes vertes!" *

/label ~Documentation
