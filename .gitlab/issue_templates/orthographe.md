### Erreurs d'orthographes trouvées
*Utilisez la formule suivante pour indiquer une ou plusieurs erreurs d'orthographes trouvées dans notre projet:*

*"Le chemin du document où se trouve l'erreur" | "Précision sur l'erreur (numéro de ligne/paragraphe)" | "La phrase/section contenant l'erreur" | "La correction proposée"*

*Exemple: "projet/application/readme.md" | Ligne 19 | "J'aime les pomme!" | "J'aime les pommes!" *

/label ~Orthographe
