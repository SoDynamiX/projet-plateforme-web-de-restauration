import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import * as routers from "./routes"
import * as dotenv from 'dotenv';
import * as createError from 'http-errors';
import * as middlewares from "./middlewares";
import { Request, Response, NextFunction } from "express";

dotenv.config({ debug: true });

const app = express();
const port = process.env.PORT || 5000;

const options: cors.CorsOptions = {
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
  ],
  credentials: true,
  methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
  origin: 'http://localhost:3000',
  preflightContinue: false,
};

app.use(express.json());

const uri = process.env.ATLAS_URI;
if (!uri) {throw new Error("Uri is null.")};
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, 
    useUnifiedTopology: true}
);
const connection = mongoose.connection;
connection.once("open", () =>{
    console.log("MongoDB database connection established successfully");
})

//Initialisation des routes
app.use("/clients", cors(options), routers.ClientRouter);
app.use("/commandes", cors(options), routers.CommandesRouter);
app.use("/restaurants", cors(options), routers.RestaurantRouter);

app.use((req) => { console.log(`404 For URL ${req.url}`); throw new createError.NotFound(); });
app.use(middlewares.ErrorMiddleware.defaultErrorHandler);

app.listen(port, () => {
    console.log('Server is running on port 5000');
})