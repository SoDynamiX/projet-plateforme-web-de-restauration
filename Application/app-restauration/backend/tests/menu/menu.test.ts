import mongoose from 'mongoose';
import { MenuController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IMenu, IRestaurant } from "../../models/interfaces";
import RestaurantSchema from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from "../../seeder/restaurant";

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    restaurantToTestWith = restaurantList[0];
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes les méthodes menu avec connexion BD.", () => {
    describe("créer un menu.", () => {
        test("devrait créer un menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {
                    nom: "menuTest",
                    disponibilite: true
                }
            }
            let response = new MockResponse();

            await MenuController.createMenu((request as unknown) as any, response as any);
            let expectedMenu = response.body as IMenu;

            expect(expectedMenu.nom).toBe("menuTest");
            expect(expectedMenu.disponibilite).toBe(true);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await MenuController.createMenu((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: { // We remove "nom" to trigger a 422.
                    disponibilite: true
                }
            }
            let response = new MockResponse();

            try {
                await MenuController.createMenu((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour le menu.", () => {
        test("devrait mettre à jour le menu", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {
                    nom: "newMenuTest",
                    disponibilite: false
                }
            }
            let response = new MockResponse();

            await MenuController.updateMenuById((request as unknown) as any, response as any);
            let expectedMenu = response.body as IMenu;

            expect(expectedMenu.nom).not.toBe("menuTest");
            expect(expectedMenu.disponibilite).not.toBe(true);
        });

        test("devrait retourner un 404 (NotFound) pour un id de restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await MenuController.deleteMenuById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un menu non existant.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await MenuController.deleteMenuById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        })

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant un champ pour la mise à jour.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {
                    disponibilite: false
                }
            }
            let response = new MockResponse();

            try {
                await MenuController.updateMenuById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec ses champs invalides.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {
                    nom: 1245124,
                    disponibilite: false
                }
            }
            let response = new MockResponse();

            try {
                await MenuController.updateMenuById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("effacer le menu", () => {
        test("devrait effacer le menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: ""
            }
            let response = new MockResponse();

            await MenuController.deleteMenuById((request as unknown) as any, response as any);
       
            expect(restaurantToTestWith.menu).toBeNull();
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await MenuController.deleteMenuById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un menu non existant.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();
           
            try {
                await MenuController.deleteMenuById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
    });
})
