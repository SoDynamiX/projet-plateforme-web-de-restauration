import mongoose from 'mongoose';
import { JourneeCategorieController, JourneeRestaurantController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IJourneeCategorie, IRestaurant } from "../../models/interfaces";
import RestaurantSchema, { Categorie, Menu, HoraireCategorie, JourneeHoraireCategorie } from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from '../../seeder/restaurant';


interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let createdJourneeCategorie: IJourneeCategorie;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.

// We create the new models.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    
    // Add a menu to our test restaurant.
    restaurantToTestWith = restaurantList[0];
    restaurantToTestWith.menu = new Menu({
        nom: "testMenu",
        disponibilite: true,
    });

    // Add a category to the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie.push(new Categorie({
        nom: "categorieTest",
        cheminImage: "noice"
    }));

    // Add a schedule to the category previously added.
    restaurantToTestWith.menu.listeCategorie[0].listeHoraireCategorie.push(new HoraireCategorie({
        nom: "horaireTest"
    }));
    
    await restaurantToTestWith.save();
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes journée horaire catégorie avec connexion BD.", () => {
    describe("créer une journée catégorie.", () => {
        test("devrait créer une journée catégorie.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id
                },
                body: {
                    nom: "journeeCategorieTest",
                    heureDebut: new Date(0,0,0,8,0),
                    heureFin: new Date(0,0,0,16,0)
                }
            }
            let response = new MockResponse();

            await JourneeCategorieController.createJourneeCategorie((request as unknown) as any, response as any);
            createdJourneeCategorie = response.body as IJourneeCategorie;

            restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0].listeJournee.push(createdJourneeCategorie);

            expect(createdJourneeCategorie.nom).toBe("journeeCategorieTest");
            expect(createdJourneeCategorie.heureDebut.getHours()).toBe(8);
            expect(createdJourneeCategorie.heureFin.getHours()).toBe(16);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.createJourneeCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.createJourneeCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.createJourneeCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id horaire catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.createJourneeCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec des champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id
                },
                body: { 
                    nom: 123123,
                    heureDebut: new Date(0,0,0,8,0),
                    heureFin: new Date(0,0,0,16,0)
                } 
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.createJourneeCategorie((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour une journée catégorie.", () => {
        test("devrait mettre à jour la journée catégorie.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id,
                    journeeid: restaurantToTestWith.menu?.listeCategorie[0]
                        .listeHoraireCategorie[0].listeJournee[0]._id
                },
                body: {
                    nom: "newJourneeCategorieTest",
                    heureDebut: new Date(0,0,0,10,0),
                    heureFin: new Date(0,0,0,18,0)
                }
            }
            let response = new MockResponse();

            await JourneeCategorieController.updateJourneeCategorieById((request as unknown) as any, response as any);
            let expectedJournee = response.body as IJourneeCategorie;

            expect(expectedJournee.nom).toBe("newJourneeCategorieTest");
            expect(expectedJournee.heureDebut.getHours()).toBe(10);
            expect(expectedJournee.heureFin.getHours()).toBe(18);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.updateJourneeCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.updateJourneeCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.updateJourneeCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id horaire catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.updateJourneeCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id journée catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id,
                    journeeid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.updateJourneeCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id,
                    journeeid: restaurantToTestWith.menu?.listeCategorie[0]
                        .listeHoraireCategorie[0].listeJournee[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    heureDebut: 123,
                    heureFin: 123
                } 
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.updateJourneeCategorieById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec des champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id,
                    journeeid: restaurantToTestWith.menu?.listeCategorie[0]
                        .listeHoraireCategorie[0].listeJournee[0]._id
                },
                body: { 
                    nom: 123123123,
                    heureDebut: 123123,
                    heureFin: "123123"
                }
            }
            let response = new MockResponse();

            try {
                await JourneeCategorieController.updateJourneeCategorieById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });
});