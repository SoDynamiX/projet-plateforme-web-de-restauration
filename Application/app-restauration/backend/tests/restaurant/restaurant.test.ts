import mongoose from 'mongoose';
import { RestaurantController } from "../../controllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IRestaurant } from "../../models/interfaces";
import RestaurantSchema from "../../models/restaurant.model";
import { getNewlyCreatedRestaurants } from "../../seeder/restaurant";

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let IdNotPresent = "6021425ea9aa3845c8750319"; // For 404 purposes.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(5)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    restaurantToTestWith = restaurantList[0];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes les méthodes restaurant avec connexion BD.", () => {
    describe("recevoir un restaurant", () => {
        test("devrait retourner un restaurant.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: ""
            }
            let response = new MockResponse();

            await RestaurantController.findOneRestaurant((request as unknown) as any, response as any);
            let expectedRestaurant = response.body as IRestaurant;

            expect(expectedRestaurant.id.toString()).toBe(restaurantToTestWith._id.toString());
        });

        test("devrait retourner un 404", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await RestaurantController.findOneRestaurant((request as unknown) as any, response as any);
            } catch (e) {
                console.log(e);
                expect(e.toString()).toBe(NotFound);
            }
        })
    })

    describe("créer un restaurant.", () => {
        test("devrait créer un restaurant.", async () => {
            let request: IMockRequest = {
                params: {},
                body: {
                    nom: "wowtestrestaurant",
                    numeroTelephone: "8198198119",
                    cheminLogo: "C:/wow",
                    courriel: "restauranttest@hotmail.com"
                }
            }
            let response = new MockResponse();
            
            await RestaurantController.createRestaurant((request as unknown) as any, response as any)
            let expectedClient = response.body as IRestaurant;

            expect(expectedClient.nom).toBe("wowtestrestaurant");
            expect(expectedClient.numeroTelephone).toBe("8198198119");
            expect(expectedClient.cheminLogo).toBe("C:/wow");
            expect(expectedClient.courriel).toBe("restauranttest@hotmail.com");
        });

        test("devrait retourner un 422 (UnprocessableEntity)", async () => {
            let request: IMockRequest = {
                params: {},
                // We remove "numeroTelephone" to trigger a 422.
                body: {
                    nom: "wowtestrestaurant",
                    cheminLogo: "C:/wow",
                    courriel: "restauranttest@hotmail.com"
                }
            }
            let response = new MockResponse();

            try {
                await RestaurantController.createRestaurant((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });
    
    describe("recevoir tous les restaurants.", () => {
        test("devrait retourner tous les restaurants.", async () => {
            let request: IMockRequest = {
                params: {},
                body: {}
            }
            let response = new MockResponse();

            await RestaurantController.findAllRestaurants((request as unknown) as any, response as any);
            let expectedRestaurantList = response.body as IRestaurant[];
            
            expect(expectedRestaurantList.length).toBe(6);
        });
    });

    describe("mettre à jour un restaurant.", () => {
        test("devrait mettre à jour un restaurant.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {
                    nom: "absolutely",
                    cheminLogo: "ridiculous",
                    courriel: "omgomg@hotmail.com"
                }
            }
            let response = new MockResponse();

            await RestaurantController.updateRestaurantById((request as unknown) as any, response as any);
            let expectedRestaurant = response.body as IRestaurant;
            
            // Verify each field.
            expect(expectedRestaurant.nom).not.toBe(restaurantToTestWith.nom);
            expect(expectedRestaurant.cheminLogo).not.toBe(restaurantToTestWith.cheminLogo);
            expect(expectedRestaurant.courriel).not.toBe(restaurantToTestWith.courriel);
        });

        test("devrait retourner un 404 (NotFound)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await RestaurantController.updateRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                // We remove "nom" to trigger a 422.
                body: {
                    cheminLogo: "ridiculous",
                    courriel: "omgomg"
                }
            }
            let response = new MockResponse();

            try {
                await RestaurantController.updateRestaurantById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }         
        });
    });

    describe("effacer un restaurant.", () => {
        test("devrait effacer un restaurant.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {}
            }
            let response = new MockResponse();

            await RestaurantController.deleteRestaurantById((request as unknown) as any, response as any);

            expect(await Restaurant.countDocuments({ _id: restaurantToTestWith._id})).toBe(0);
        });

        test("devrait retourner un 404 (NotFound)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await RestaurantController.deleteRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        })
    })
})
