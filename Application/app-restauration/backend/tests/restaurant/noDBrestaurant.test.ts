import { RestaurantController } from "../../controllers/index";
import { IRestaurant } from "../../models/interfaces";

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

// Variables used for testing.
let clientToTestWith : IRestaurant;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes. This ID was pulled from our own database to represent a true mongoID.

// Variable used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
let InternalServerError = "InternalServerError: Internal Server Error"

describe("toutes les méthodes restaurant sans connexion avec BD.", () => {
    describe("recevoir un client.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await RestaurantController.findOneRestaurant((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        });
    });

    describe("créer un client.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: {
                    nom: "wowtestrestaurant",
                    numeroTelephone: "8198198119",
                    cheminLogo: "C:/wow",
                    courriel: "restauranttest@hotmail.com"
                }
            }
            let response = new MockResponse();

            try {
                await RestaurantController.createRestaurant((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        });
    });

    describe("recevoir tous les clients.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await RestaurantController.findAllRestaurants((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            } 
        });
    });

    describe("mettre à jour un restaurant.", () => {
        test("devrait retourner un 500 (InternalServerError).", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await RestaurantController.updateRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            } 
        });
    });

    describe("effacer un restaurant.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await RestaurantController.deleteRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        });
    });
})
