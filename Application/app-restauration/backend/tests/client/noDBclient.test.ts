import { ClientController } from "../../controllers/index";
import { IClient } from "../../models/interfaces";

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

// Variables used for client testing.
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes. This ID was pulled from our own database to represent a true mongoID.

// Variable used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
let InternalServerError = "InternalServerError: Internal Server Error"

describe("toutes méthodes client sans connexion avec BD.", () => {
    describe("recevoir un client", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ClientController.getOneClient((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        })
    })

    describe("créer un client", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: {
                    prenom: "xavier",
                    nom: "blanchettenoel",
                    courriel: "xavierblanchettenoel@hotmail.com",
                    nomUtilisateur: "tatsuni",
                    motDePasseHashed: "youthoughteheh",
                    numeroTelephone: "8198198119",
                    statut: true
                }
            }
            let response = new MockResponse();

            try {
                await ClientController.createClient((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        })
    })

    describe("recevoir tous les clients.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ClientController.getAllClients((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        })
    })

    describe("mettre à jour un client.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ClientController.updateClientById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        })
    })

    describe("effacer un client.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ClientController.deleteClientById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        })
    })
})
