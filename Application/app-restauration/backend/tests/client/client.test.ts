import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { ClientController } from "../../controllers/index";
import { IClient } from "../../models/interfaces";
import { ClientSchema } from "../../models/client.model";
import { getNewlyCreatedClients } from "../../seeder/client";

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for client testing.
let clientToTestWith : IClient;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.
const Client = mongoose.model<IClient>("Client", ClientSchema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let clientList = (await getNewlyCreatedClients(5)).map(c => new Client(c));
    await Promise.all(clientList.map(client => client.save()));
    clientToTestWith = clientList[0];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes client avec connexion BD.", () => {
    describe("recevoir un client", ()  => {
        test("devrait retourner un client.", async () => {
            let request: IMockRequest = {
                params: {clientid: clientToTestWith._id},
                body: ""
            }
            let response = new MockResponse();
  
            await ClientController.getOneClient((request as unknown) as any, response as any);
            let expectedClient = response.body as IClient;
            
            expect(expectedClient.id.toString()).toBe(clientToTestWith._id.toString());  
        });

        test("devrait retourner un 404", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await ClientController.getOneClient((request as unknown) as any, response as any);
            } catch (e) {
                console.log(e);
                expect(e.toString()).toBe(NotFound);
            }
        })
    });
    
    describe("créer un client.", () => {
        test("devrait créer un client.", async () => {
            let request: IMockRequest = {
                params: {},
                body: {
                    prenom: "xavier",
                    nom: "blanchettenoel",
                    courriel: "xavierblanchettenoel@hotmail.com",
                    numeroTelephone: "8198198119",
                    statut: true
                }
            }
            let response = new MockResponse();
            
            await ClientController.createClient((request as unknown) as any, response as any)
            let expectedClient = response.body as IClient;

            expect(expectedClient.prenom).toBe("xavier");
            expect(expectedClient.nom).toBe("blanchettenoel");
            expect(expectedClient.courriel).toBe("xavierblanchettenoel@hotmail.com");
            expect(expectedClient.numeroTelephone).toBe("8198198119");
        });

        test("devrait retourner un 422 (UnprocessableEntity)", async () => {
            let request: IMockRequest = {
                params: {},
                // We remove "numeroTelephone" to trigger a 422.
                body: {
                    prenom: "xavier",
                    nom: "blanchettenoel",
                    courriel: "xavierblanchettenoel@hotmail.com",
                    nomUtilisateur: "tatsuni",
                    motDePasseHashed: "youthoughteheh",
                    statut: true
                }
            }
            let response = new MockResponse();

            try {
                await ClientController.createClient((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }         
        })
    });

    describe("recevoir tous les clients.", () => {
        test("devrait retourner tous les clients.", async () => {
            let request: IMockRequest = {
                params: {},
                body: {}
            }
            let response = new MockResponse();

            await ClientController.getAllClients((request as unknown) as any, response as any);
            let expectedClientList = response.body as IClient[];
            
            expect(expectedClientList.length).toBe(6);
        });
    });

    describe("mettre à jour un client.", () => {
        test("devrait mettre à jour un client.", async () => {
            let request: IMockRequest = {
                params: {clientid: clientToTestWith._id},
                body: {
                    courriel: "michelsimoneau@hotmail.com",
                    numeroTelephone: "4504504550",
                    statut: false
                }
            }
            let response = new MockResponse();

            await ClientController.updateClientById((request as unknown) as any, response as any);
            let expectedClient = response.body as IClient;
            
            // Verify each field.
            expect(expectedClient.prenom).toBe(clientToTestWith.prenom);
            expect(expectedClient.nom).toBe(clientToTestWith.nom);
            expect(expectedClient.courriel).not.toBe(clientToTestWith.courriel);
            expect(expectedClient.numeroTelephone).not.toBe(clientToTestWith.numeroTelephone);
            expect(expectedClient.statut.toString).not.toBe(clientToTestWith.statut);
        });

        test("devrait retourner un 404 (NotFound)", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await ClientController.updateClientById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        })

        test("devrait retourner un 422 (UnprocessableEntity)", async () => {
            let request: IMockRequest = {
                params: {clientid: clientToTestWith._id},
                // We remove "numeroTelephone" to trigger a 422.
                body: {
                    prenom: "xavier",
                    nom: "blanchettenoel",
                    courriel: "xavierblanchettenoel@hotmail.com",
                    nomUtilisateur: "tatsuni",
                    statut: true
                }
            }
            let response = new MockResponse();

            try {
                await ClientController.updateClientById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }         
        })        
    });

    describe("effacer un client.", () => {
        test("devrait effacer un client.", async () => {
            let request: IMockRequest = {
                params: {clientid: clientToTestWith._id},
                body: {}
            }
            let response = new MockResponse();

            await ClientController.deleteClientById((request as unknown) as any, response as any);

            expect(await Client.countDocuments({ _id: clientToTestWith._id})).toBe(0);
        });

        test("devrait retourner un 404 (NotFound)", async () => {
            let request: IMockRequest = {
                params: {clientid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await ClientController.deleteClientById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        })
    });
});
