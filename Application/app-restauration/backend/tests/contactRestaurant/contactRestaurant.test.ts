import mongoose from 'mongoose';
import { ContactRestaurantController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IContactRestaurant, IRestaurant } from "../../models/interfaces";
import RestaurantSchema from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from '../../seeder/restaurant';

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let createdContactRestaurant: IContactRestaurant;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.


// We create the new models.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    restaurantToTestWith = restaurantList[0];
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes les méthodes contact restaurant avec connexion BD", () => {
    describe("créer un contact restaurant.", () => {
        test("devrait créer un contact restaurant.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {
                    prenom: "xavier",
                    nom: "blanchette",
                    courriel: "courrieltest@hotmail.com",
                    numeroTelephone: "8198198119"
                }
            }
            let response = new MockResponse();

            await ContactRestaurantController.createContactRestaurant((request as unknown) as any, response as any);
            createdContactRestaurant = response.body as IContactRestaurant;

            restaurantToTestWith.listeContactRestaurant.push(createdContactRestaurant);

            expect(createdContactRestaurant.prenom).toBe("xavier");
            expect(createdContactRestaurant.nom).toBe("blanchette")
            expect(createdContactRestaurant.courriel).toBe("courrieltest@hotmail.com");
            expect(createdContactRestaurant.numeroTelephone).toBe("8198198119");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContactRestaurantController.createContactRestaurant((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: { // We remove "nom" to trigger a 422.
                    prenom: "xavier",
                    courriel: "courrieltest@hotmail.com",
                    numeroTelephone: "8198198119"
                }
            }
            let response = new MockResponse();

            try {
                await ContactRestaurantController.createContactRestaurant((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour un contact restaurant.", () => {
        test("devrait mettre à jour le contact restaurant.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    contactid: restaurantToTestWith.listeContactRestaurant[0]._id
                }, 
                body: {
                    prenom: "michel",
                    nom: "simoneau",
                    courriel: "testdecourriel@hotmail.com",
                    numeroTelephone: "4504504550"
                }
            }
            let response = new MockResponse();

            await ContactRestaurantController.updateContactRestaurantById((request as unknown) as any, response as any);
            let expectedContactRestaurant = response.body as IContactRestaurant;

            expect(expectedContactRestaurant.prenom).toBe("michel");
            expect(expectedContactRestaurant.nom).toBe("simoneau");
            expect(expectedContactRestaurant.courriel).toBe("testdecourriel@hotmail.com");
            expect(expectedContactRestaurant.numeroTelephone).toBe("4504504550");
        });

        test("devrait retourner un 404 (NotFound) pour un id de restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    contactid: restaurantToTestWith.listeContactRestaurant[0]._id
                }, 
                body: {
                    prenom: "michel",
                    nom: "simoneau",
                    courriel: "testdecourriel@hotmail.com",
                    numeroTelephone: "4504504550"
                }
            }
            let response = new MockResponse();

            try {
                await ContactRestaurantController.updateContactRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id de contact invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    contactid: IdNotPresent
                }, 
                body: {
                    prenom: "michel",
                    nom: "simoneau",
                    courriel: "testdecourriel@hotmail.com",
                    numeroTelephone: "4504504550"
                }
            }
            let response = new MockResponse();

            try {
                await ContactRestaurantController.updateContactRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprossableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    contactid: restaurantToTestWith.listeContactRestaurant[0]._id
                }, 
                body: { // We remove "nom" to trigger a 422.
                    prenom: "michel",
                    courriel: "testdecourriel@hotmail.com",
                    numeroTelephone: "4504504550"
                }
            }
            let response = new MockResponse();

            try {
                await ContactRestaurantController.updateContactRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprossableEntity) pour une requête avec des champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    contactid: restaurantToTestWith.listeContactRestaurant[0]._id
                }, 
                body: { // We remove "nom" to trigger a 422.
                    prenom: "michel",
                    nom: 123123123,
                    courriel: "testdecourriel@hotmail.com",
                    numeroTelephone: "4504504550"
                }
            }
            let response = new MockResponse();

            try {
                await ContactRestaurantController.updateContactRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("effacer un contact restaurant.", () => {
        test("devrait effacer le contact restaurant.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    contactid: restaurantToTestWith.listeContactRestaurant[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            await ContactRestaurantController.deleteContactRestaurantById((request as unknown) as any, response as any);
            
            // We can use findById here since it's been infered it works from previous tests.
            let restaurant = await Restaurant.findById(restaurantToTestWith._id);

            expect(await restaurant?.listeContactRestaurant
                .find(c => c._id == restaurantToTestWith.listeContactRestaurant[0]._id))
                .toBe(undefined);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await ContactRestaurantController.deleteContactRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id contact restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantWithoutMenu._id,
                    contactid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();
           
            try {
                await ContactRestaurantController.deleteContactRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
    });
});
