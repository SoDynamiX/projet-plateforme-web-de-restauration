import mongoose from 'mongoose';
import { HoraireCategorieController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IHoraireCategorie, IRestaurant } from "../../models/interfaces";
import RestaurantSchema, { Categorie, Menu, HoraireCategorie } from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from '../../seeder/restaurant';


interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let createdHoraireCategorie: IHoraireCategorie;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.

// We create the new models.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    
    // Add a menu to our test restaurant.
    restaurantToTestWith = restaurantList[0];
    restaurantToTestWith.menu = new Menu({
        nom: "testMenu",
        disponibilite: true,
    });

    // Add a category to the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie.push(new Categorie({
        nom: "categorieTest",
        cheminImage: "noice"
    }));

    await restaurantToTestWith.save();
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes horaire catégorie avec connexion BD.", () => {
    describe("créer une horaire catégorie.", () => {
        test("devrait créer un horaire catégorie.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: {
                    nom: "horaireCategorieTest",
                }
            }
            let response = new MockResponse();

            await HoraireCategorieController.createHoraireCategorie((request as unknown) as any, response as any);
            createdHoraireCategorie = response.body as IHoraireCategorie;

            restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie.push(createdHoraireCategorie);

            expect(createdHoraireCategorie.nom).toBe("horaireCategorieTest");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.createHoraireCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.createHoraireCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.createHoraireCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: {} // Since a schedule only has a name, we send an empty object.
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.createHoraireCategorie((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour un horaire catégorie.", () => {
        test("devrait mettre à jour un horaire catégorie.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id
                },
                body: {
                    nom: "newHoraireCategorieTest",
                }
            }
            let response = new MockResponse();

            await HoraireCategorieController.updateHoraireCategorieById((request as unknown) as any, response as any);
            let expectedHoraire = response.body as IHoraireCategorie;

            expect(expectedHoraire.nom).toBe("newHoraireCategorieTest");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.updateHoraireCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.updateHoraireCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.updateHoraireCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id horaire catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.updateHoraireCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id
                },
                body: {} // Since a schedule only has a name, we send an empty object.
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.updateHoraireCategorieById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec des champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id
                },
                body: { nom: 123123123 }
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.updateHoraireCategorieById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("effacer un horaire restaurant.", () => {
        test("devrait effacer l'horaire restaurant.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    horaireid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id
                },
                body: {}
            }
            let response = new MockResponse();

            await HoraireCategorieController.deleteHoraireCategorieById((request as unknown) as any, response as any);

            expect(await HoraireCategorie.countDocuments({ _id: createdHoraireCategorie._id }));
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.deleteHoraireCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.deleteHoraireCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.deleteHoraireCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0].listeHoraireCategorie[0]._id,
                    horaireid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await HoraireCategorieController.deleteHoraireCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
    });
});
