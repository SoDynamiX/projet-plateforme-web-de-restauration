import mongoose from 'mongoose';
import { JourneeRestaurantController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IRestaurant, IJourneeRestaurant } from "../../models/interfaces";
import RestaurantSchema, { Horaire } from "../../models/restaurant.model";
import { getNewlyCreatedRestaurants } from "../../seeder/restaurant";
import { create } from 'domain';
import { createJourneeCategorie } from '../../controllers/restaurantControllers/journeeCategorie';
import { ExpectationFailed } from 'http-errors';


interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let createdJourneeHoraire: IJourneeRestaurant
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(1)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    restaurantToTestWith = restaurantList[0];

    restaurantToTestWith.listeHoraire.push(new Horaire({
        nom: "testHoraire"
    }));
    await restaurantToTestWith.save();
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes journee horaire avec connexion BD.", () => {
    describe("créer une journee horaire.", () => {
        test("devrait créer une journee horaire.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: restaurantToTestWith.listeHoraire[0]._id
                },
                body: {
                    nom: "journeeHoraireTest",
                    heureOuverture: new Date(0,0,0,8,30),
                    heureFermeture: new Date(0,0,0,18,0),
                    contrainteHoraire: new Date(0,0,0,0)
                }
            }
            let response = new MockResponse();

            await JourneeRestaurantController.createJourneeRestaurant((request as unknown) as any, response as any);
            createdJourneeHoraire = response.body as IJourneeRestaurant;

            restaurantToTestWith.listeHoraire[0].listeJournee.push(createdJourneeHoraire);

            expect(createdJourneeHoraire.heureOuverture.getHours()).toBe(8);
            expect(createdJourneeHoraire.heureOuverture.getMinutes()).toBe(30);
            expect(createdJourneeHoraire.heureFermeture.getHours()).toBe(18);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await JourneeRestaurantController.createJourneeRestaurant((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id horaire restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await JourneeRestaurantController.createJourneeRestaurant((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: restaurantToTestWith.listeHoraire[0]._id
                },
                body: {
                    nom: 123123,
                    heureOuverture: new Date(0,0,0,8,30),
                    heureFermeture: new Date(0,0,0,18,0),
                    contrainteHoraire: new Date(0,0,0,0)
                } 
            }
            let response = new MockResponse();

            try {
                await JourneeRestaurantController.createJourneeRestaurant((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour une journée horaire.", () => {
        test("devrait mettre à jour la journée horaire.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: restaurantToTestWith.listeHoraire[0]._id,
                    journeeid: createdJourneeHoraire._id
                },
                body: {
                    nom: "newJourneeHoraireTest",
                    heureOuverture: new Date(0,0,0,7,45),
                    heureFermeture: new Date(0,0,0,20,30),
                    contrainteHoraire: new Date(0,0,0,8,0)
                }
            }
            let response = new MockResponse();

            await JourneeRestaurantController.updateJourneeRestaurantById((request as unknown) as any, response as any);
            let expectedJournee = response.body as IJourneeRestaurant;

            expect(expectedJournee.nom).toBe("newJourneeHoraireTest");
            expect(expectedJournee.heureOuverture.getHours()).toBe(7);
            expect(expectedJournee.heureOuverture.getMinutes()).toBe(45);
            expect(expectedJournee.heureFermeture.getHours()).toBe(20);
            expect(expectedJournee.heureFermeture.getMinutes()).toBe(30);
            expect(expectedJournee.contrainteHoraire.getHours()).toBe(8);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await JourneeRestaurantController.updateJourneeRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id horaire restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await JourneeRestaurantController.updateJourneeRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id journee horaire invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: restaurantToTestWith.listeHoraire[0]._id,
                    journeeid: restaurantToTestWith.listeHoraire[0].listeJournee[0]._id
                },
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await JourneeRestaurantController.updateJourneeRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
        
        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: restaurantToTestWith.listeHoraire[0]._id,
                    journeeid: restaurantToTestWith.listeHoraire[0].listeJournee[0]._id
                },
                body: {
                    heureOuverture: 123123123,
                    heureFermeture: 123123123,
                    contrainteHoraire: 123123123
                } 
            }
            let response = new MockResponse();

            try {
                await JourneeRestaurantController.updateJourneeRestaurantById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec des champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: restaurantToTestWith.listeHoraire[0]._id,
                    journeeid: restaurantToTestWith.listeHoraire[0].listeJournee[0]._id
                },
                body: { 
                    nom: 123123123123,
                    heureOuverture: "123123123",
                    heureFermeture: "123123123",
                    contrainteHoraire: "123123123"
                } 
            }
            let response = new MockResponse();

            try {
                await JourneeRestaurantController.updateJourneeRestaurantById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });
});