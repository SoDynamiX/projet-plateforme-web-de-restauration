import mongoose from 'mongoose';
import { ContrainteController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IContrainte, IRestaurant } from "../../models/interfaces";
import RestaurantSchema, { Categorie, Menu, Article, Contrainte } from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from '../../seeder/restaurant';


interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let createdContrainte: IContrainte;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.


// We create the new models.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    
    // Add a menu to our test restaurant.
    restaurantToTestWith = restaurantList[0];
    restaurantToTestWith.menu = new Menu({
        nom: "testMenu",
        disponibilite: true,
    });

    // Add a category to the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie.push(new Categorie({
        nom: "categorieTest",
        cheminImage: "noice"
    }));

    // Add an article to the category of the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie[0].listeArticle.push(new Article({
        nom: "articleTest",
        description: "descriptionTest",
        cheminImage: "cheminImageTest", 
        prix: 12.99,
        disponibilite: true,
        salleSeulement: true
    }));

    await restaurantToTestWith.save();
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes contrainte avec connexion BD.", () => {
    describe("créer une contrainte.", () => {
        test("devrait créer une contrainte.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: {
                    nom: "contrainteTest",
                    cheminImage: "cheminImageTest"
                }
            }
            let response = new MockResponse();

            await ContrainteController.createContrainte((request as unknown) as any, response as any);
            createdContrainte = response.body as IContrainte;

            restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]
                .listeContrainte.push(createdContrainte);

            expect(createdContrainte.nom).toBe("contrainteTest");
            expect(createdContrainte.cheminImage).toBe("cheminImageTest");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.createContrainte((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.createContrainte((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.createContrainte((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.createContrainte((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    cheminImage: "noice"
                }
            }
            let response = new MockResponse();

            try {
                await ContrainteController.createContrainte((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour une contrainte.", () => {
        test("devrait mettre à jour une contrainte.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    contrainteid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0].listeContrainte[0]._id
                },
                body: {
                    nom: "newContrainteTest",
                    cheminImage: "newCheminImageTest"
                }
            }
            let response = new MockResponse();

            await ContrainteController.updateContrainteById((request as unknown) as any, response as any);
            let expectedContrainte = response.body as IContrainte;

            expect(expectedContrainte.nom).toBe("newContrainteTest");
            expect(expectedContrainte.cheminImage).toBe("newCheminImageTest");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id contrainte invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    contrainteid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    contrainteid: restaurantToTestWith.menu?.listeCategorie[0]
                                    .listeArticle[0].listeContrainte[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    cheminImage: "noice"
                }
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec ses champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    contrainteid: restaurantToTestWith.menu?.listeCategorie[0]
                                    .listeArticle[0].listeContrainte[0]._id
                },
                body: {
                    nom: 123123,
                    cheminImage: "wow"
                }
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("effacer une contrainte.", () => {
        test("devrait effacer une contrainte.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    contrainteid: restaurantToTestWith.menu?.listeCategorie[0]
                                    .listeArticle[0].listeContrainte[0]._id
                },
                body: {}
            }
            let response = new MockResponse();

            await ContrainteController.deleteContrainteById((request as unknown) as any, response as any);

            expect(await Contrainte.countDocuments({ _id: createdContrainte._id })).toBe(0);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id contrainte invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    contrainteid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ContrainteController.updateContrainteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
    });
});