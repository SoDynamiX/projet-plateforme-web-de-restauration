import mongoose from 'mongoose';
import { ExtraController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IRestaurant, IExtra } from "../../models/interfaces";
import RestaurantSchema, { Article, Categorie, Extra, Menu } from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from '../../seeder/restaurant';


interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let createdExtra: IExtra;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.


// We create the new models.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    
    // Add a menu to our test restaurant.
    restaurantToTestWith = restaurantList[0];
    restaurantToTestWith.menu = new Menu({
        nom: "testMenu",
        disponibilite: true,
    });

    // Add a category to the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie.push(new Categorie({
        nom: "categorieTest",
        cheminImage: "noice"
    }));

    // Add an article to the article list of the category of the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie[0].listeArticle.push(new Article({
        nom: "articleTest",
        description: "descriptionTest",
        cheminImage: "cheminImageTest", 
        prix: 12.99,
        disponibilite: true,
        salleSeulement: true
    }));

    await restaurantToTestWith.save();
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});


describe("toutes méthodes extras avec connexion BD.", () => {
    describe("créer un extra.", () => {
        test("devrait créer un extra.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: {
                    nom: "extraTest",
                    prix: 12.99
                }
            }
            let response = new MockResponse();
    
            await ExtraController.createExtra((request as unknown) as any, response as any);
            createdExtra = response.body as IExtra;

            expect(createdExtra.nom).toBe("extraTest");
            expect(createdExtra.prix).toBe(12.99);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.createExtra((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.createExtra((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.createExtra((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    prix: 12.99
                }
            }
            let response = new MockResponse();

            try {
                await ExtraController.createExtra((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        }); 
    });

    describe("mettre à jour un extra.", () => {
        test("devrait mettre à jour un extra.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    extraid: createdExtra._id
                },
                body: {
                    nom: "newExtraTest",
                    prix: 14.99
                }
            }
            let response = new MockResponse();
    
            await ExtraController.updateExtraById((request as unknown) as any, response as any);
            let expectedExtra = response.body as IExtra;

            expect(expectedExtra.nom).toBe("newExtraTest");
            expect(expectedExtra.prix).toBe(14.99);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.updateExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.updateExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.updateExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.updateExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id extra invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    extraid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.updateExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    extraid: createdExtra._id
                },
                body: { // We remove "nom" to trigger a 422.
                    prix: 12.99
                }
            }
            let response = new MockResponse();

            try {
                await ExtraController.updateExtraById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        }); 

        
        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec ses champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    extraid: createdExtra._id
                },
                body: { 
                    nom:123123123,
                    prix: 12.99
                }
            }
            let response = new MockResponse();

            try {
                await ExtraController.updateExtraById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        }); 
    });

    describe("effacer un extra.", () => {
        test("devrait effacer un extra.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    extraid: createdExtra._id
                },
                body: {}
            }
            let response = new MockResponse();

            await ExtraController.deleteExtraById((request as unknown) as any, response as any);

            expect(await Extra.countDocuments({ _id: createdExtra._id })).toBe(0);

        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.deleteExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.deleteExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.deleteExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ExtraController.deleteExtraById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
    });
});