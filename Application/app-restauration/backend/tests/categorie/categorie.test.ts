import mongoose from 'mongoose';
import { CategorieController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { ICategorie, IRestaurant } from "../../models/interfaces";
import RestaurantSchema, { Categorie, Menu } from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from '../../seeder/restaurant';

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let createdCategorie: ICategorie;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.


// We create the new models.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    
    restaurantToTestWith = restaurantList[0];
    restaurantToTestWith.menu = new Menu({
        nom: "testMenu",
        disponibilite: true
    })
    await restaurantToTestWith.save();
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes catégorie avec connexion BD", () => {
    describe("créer une catégorie.", () => {
        test("devrait créer une catégorie.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {
                    nom: "blanchette",
                    cheminImage: "wow"
                }
            }
            let response = new MockResponse();

            await CategorieController.createCategorie((request as unknown) as any, response as any);
            createdCategorie = response.body as ICategorie;

            restaurantToTestWith.menu?.listeCategorie.push(createdCategorie);

            expect(createdCategorie.nom).toBe("blanchette");
            expect(createdCategorie.cheminImage).toBe("wow");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieController.createCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieController.createCategorie((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: { // We remove "nom" to trigger a 422.
                    prenom: "xavier",
                    courriel: "courrieltest@hotmail.com",
                    numeroTelephone: "8#198198119"
                }
            }
            let response = new MockResponse();

            try {
                await CategorieController.createCategorie((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour une catégorie.", () => {
        test("devrait mettre à jour une catégorie.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: createdCategorie._id
                }, 
                body: {
                    nom: "simoneau",
                    cheminImage: "wowwow"
                }
            }
            let response = new MockResponse();

            await CategorieController.updateCategorieById((request as unknown) as any, response as any);
            let expectedCategorie = response.body as ICategorie;


            expect(expectedCategorie.nom).toBe("simoneau");
            expect(expectedCategorie.cheminImage).toBe("wowwow");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieController.updateCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieController.updateCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieController.updateCategorieById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour un champ manquant dans pour la mise à jour.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: createdCategorie._id
                },
                body: { // We remove "nom" to trigger a 422.                    
                    cheminImage: "wowwow"
                }
            }
            let response = new MockResponse();

            try {
                await CategorieController.updateCategorieById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec ses champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: createdCategorie._id
                },
                body: { 
                    nom: 123,
                    cheminImage: "wowwow"
                }
            }
            let response = new MockResponse();

            try {
                await CategorieController.updateCategorieById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("effacer une catégorie.", () => {
        describe("devrait effacer une catégorie.", () => {
            test("devrait effacer le menu.", async () => {
                let request: IMockRequest = {
                    params: {
                        restaurantid: restaurantToTestWith._id,
                        categorieid: createdCategorie._id
                    },
                    body: ""
                }
                let response = new MockResponse();
                
                await CategorieController.deleteCategorieById((request as unknown) as any, response as any);

                expect(await Categorie.countDocuments({ _id: createdCategorie._id})).toBe(0);
            });

            test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
                let request: IMockRequest = {
                    params: {restaurantid: IdNotPresent},
                    body: ""
                }
                let response = new MockResponse();
    
                try {
                    await CategorieController.deleteCategorieById((request as unknown) as any, response as any)
                } catch (e) {
                    expect(e.toString()).toBe(NotFound);
                }
            });
    
            test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
                let request: IMockRequest = {
                    params: {restaurantid: restaurantWithoutMenu._id},
                    body: ""
                }
                let response = new MockResponse();
    
                try {
                    await CategorieController.deleteCategorieById((request as unknown) as any, response as any)
                } catch (e) {
                    expect(e.toString()).toBe(NotFound);
                }
            });
    
            test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
                let request: IMockRequest = {
                    params: {
                        restaurantid: restaurantToTestWith._id,
                        categorieid: IdNotPresent
                    },
                    body: ""
                }
                let response = new MockResponse();
    
                try {
                    await CategorieController.deleteCategorieById((request as unknown) as any, response as any)
                } catch (e) {
                    expect(e.toString()).toBe(NotFound);
                }
            });
        });
    });
});
