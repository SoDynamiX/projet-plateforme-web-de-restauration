import mongoose from 'mongoose';
import { HoraireController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IRestaurant, IHoraireRestaurant } from "../../models/interfaces";
import RestaurantSchema, { Horaire } from "../../models/restaurant.model";
import { getNewlyCreatedRestaurants } from "../../seeder/restaurant";

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let createdHoraireRestaurant: IHoraireRestaurant
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(1)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    restaurantToTestWith = restaurantList[0];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes horaire restaurant avec connexion BD.", () => {
    describe("créer un horaire restaurant.", () => {
        test("devrait créer un horaire restaurant.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {
                    nom: "horaireRestaurantTest"
                }
            }
            let response = new MockResponse();

            await HoraireController.createHoraireRestaurant((request as unknown) as any, response as any);
            createdHoraireRestaurant = response.body as IHoraireRestaurant;

            expect(createdHoraireRestaurant.nom).toBe("horaireRestaurantTest");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await HoraireController.createHoraireRestaurant((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantToTestWith._id},
                body: {} // Since a schedule only takes a name, we send an empty object.
            }
            let response = new MockResponse();

            try {
                await HoraireController.createHoraireRestaurant((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour un horaire restaurant.", () => {
        test("devrait mettre à jour l'horaire restaurant.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: createdHoraireRestaurant._id
                },
                body: { nom: "newHoraireRestaurantTest" }
            }
            let response = new MockResponse();

            await HoraireController.updateHoraireRestaurantById((request as unknown) as any, response as any);
            let expectedHoraire = response.body as IHoraireRestaurant;

            expect(expectedHoraire.nom).toBe("newHoraireRestaurantTest");
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await HoraireController.updateHoraireRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id horaire restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await HoraireController.updateHoraireRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: createdHoraireRestaurant._id
                },
                body: {} // Since a schedule only takes a name, we send an empty object.
            }
            let response = new MockResponse();

            try {
                await HoraireController.updateHoraireRestaurantById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec ses champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: createdHoraireRestaurant._id
                },
                body: { nom: 123123 }
            }
            let response = new MockResponse();

            try {
                await HoraireController.updateHoraireRestaurantById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("effacer un horaire restaurant.", () => {
        test("devrait effacer un horaire restaurant.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: createdHoraireRestaurant._id
                },
                body: ""
            }
            let response = new MockResponse();

            await HoraireController.deleteHoraireRestaurantById((request as unknown) as any, response as any);

            expect(await Horaire.countDocuments({ _id: createdHoraireRestaurant._id }));
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await HoraireController.updateHoraireRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id horaire restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    horaireid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();
            
            try {
                await HoraireController.updateHoraireRestaurantById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
    });
});