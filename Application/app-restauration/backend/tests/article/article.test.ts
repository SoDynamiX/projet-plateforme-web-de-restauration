import mongoose from 'mongoose';
import { ArticleController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IArticle, IRestaurant } from "../../models/interfaces";
import RestaurantSchema, { Categorie, Menu, Article } from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from '../../seeder/restaurant';

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let createdArticle: IArticle;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.


// We create the new models.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    
    // Add a menu to our test restaurant.
    restaurantToTestWith = restaurantList[0];
    restaurantToTestWith.menu = new Menu({
        nom: "testMenu",
        disponibilite: true,
    });

    // Add a category to the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie.push(new Categorie({
        nom: "categorieTest",
        cheminImage: "noice"
    }));

    await restaurantToTestWith.save();
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes articles avec connexion BD.", () => {
    describe("créer un article.", () => {
        test("devrait créer un article.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: {
                    nom: "articleTest",
                    description: "descriptionTest",
                    cheminImage: "cheminImageTest", 
                    prix: 12.99,
                    disponibilite: true,
                    salleSeulement: true
                }
            }
            let response = new MockResponse();

            await ArticleController.createArticle((request as unknown) as any, response as any);
            createdArticle = response.body as IArticle;
            
            restaurantToTestWith.menu?.listeCategorie[0].listeArticle.push(createdArticle);

            expect(createdArticle.nom).toBe("articleTest");
            expect(createdArticle.description).toBe("descriptionTest");
            expect(createdArticle.cheminImage).toBe("cheminImageTest");
            expect(createdArticle.prix).toBe(12.99);
            expect(createdArticle.disponibilite).toBe(true);
            expect(createdArticle.salleSeulement).toBe(true);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.createArticle((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.createArticle((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.createArticle((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    prenom: "xavier",
                    courriel: "courrieltest@hotmail.com",
                    numeroTelephone: "8198198119"
                }
            }
            let response = new MockResponse();

            try {
                await ArticleController.createArticle((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("mettre à jour un article.", () => {
        test("devrait mettre à jour l'article.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id 
                },
                body: {
                    nom: "newArticleTest",
                    description: "newDescriptionTest",
                    cheminImage: "newCheminImageTest", 
                    prix: 14.99,
                    disponibilite: false,
                    salleSeulement: false
                }
            }
            let response = new MockResponse();

            await ArticleController.updateArticleById((request as unknown) as any, response as any);
            let expectedArticle = response.body as IArticle;

            expect(expectedArticle.nom).toBe("newArticleTest");
            expect(expectedArticle.description).toBe("newDescriptionTest");
            expect(expectedArticle.cheminImage).toBe("newCheminImageTest");
            expect(expectedArticle.prix).toBe(14.99);
            expect(expectedArticle.disponibilite).toBe(false);
            expect(expectedArticle.salleSeulement).toBe(false);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.updateArticleById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.updateArticleById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.updateArticleById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.updateArticleById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    description: "newDescriptionTest",
                    cheminImage: "newCheminImageTest", 
                    prix: 14.99,
                    disponibilite: false,
                    salleSeulement: false
                }
            }
            let response = new MockResponse();

            try {
                await ArticleController.updateArticleById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec ses champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    nom: 123123,
                    description: "newDescriptionTest",
                    cheminImage: "newCheminImageTest", 
                    prix: 14.99,
                    disponibilite: false,
                    salleSeulement: false
                }
            }
            let response = new MockResponse();

            try {
                await ArticleController.updateArticleById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        });
    });

    describe("effacer un article.", () => {
        test("devrait effacer l'article.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: {}
            }
            let response = new MockResponse();

            await ArticleController.deleteArticleById((request as unknown) as any, response as any);

            expect(await Article.countDocuments({ _id: createdArticle._id })).toBe(0);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.deleteArticleById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.deleteArticleById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.deleteArticleById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.deleteArticleById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
    });
});