import { ArticleController } from "../../controllers/restaurantControllers";
import { Article } from "../../models/restaurant.model";

interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

// Variables used for testing.
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes. This ID was pulled from our own database to represent a true mongoID.

// Variable used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
let InternalServerError = "InternalServerError: Internal Server Error"

describe("toutes méthodes article sans connexion avec BD.", () => {
    describe("créer un article.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.createArticle((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        });
    });

    describe("mettre à jour un article.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.updateArticleById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        });
    });

    describe("effacer un article.", () => {
        test("devrait retourner un 500 (InternalServerError)", async () => {
            let request: IMockRequest = {
                params: {restaurantid: IdNotPresent},
                body: ""
            }
            let response = new MockResponse();

            try {
                await ArticleController.deleteArticleById((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(InternalServerError);
            }
        });
    });
});