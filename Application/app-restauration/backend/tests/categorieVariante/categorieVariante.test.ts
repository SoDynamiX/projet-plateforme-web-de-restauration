import mongoose from 'mongoose';
import { CategorieVarianteController } from "../../controllers/restaurantControllers/index";
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IRestaurant, ICategorieVariante } from "../../models/interfaces";
import RestaurantSchema, { Article, Categorie, CategorieVariante, Menu } from "../../models/restaurant.model";
import getNewlyCreatedRestaurants from '../../seeder/restaurant';


interface IMockRequest {
    params: {};
    body: any;
}

class MockResponse {
    public body: any;
    public _status?: number;
    send = (content: any) => {this.body = content}
    status = (status: number) => {this._status = status; return this}
    end = () => {}
}

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

let mongoServer: MongoMemoryServer;

// Variables used for testing.
let restaurantToTestWith : IRestaurant;
let restaurantWithoutMenu: IRestaurant;
let createdCategorieVariante: ICategorieVariante;
let IdNotPresent = "601c4b2491cf18139c8574c6"; // For 404 purposes.


// We create the new models.
const Restaurant = mongoose.model<IRestaurant>("Restaurant", RestaurantSchema.schema)

// Variables used for throw testing.
// Since we use a library to wrap throws in functions
// we have to check the message the error sends back.
const NotFound = "NotFoundError: Not Found";
const UnprocessableEntity = "UnprocessableEntityError: Unprocessable Entity";

beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) console.error(err);
    });

    let restaurantList = (await getNewlyCreatedRestaurants(2)).map(c => new Restaurant(c));
    await Promise.all(restaurantList.map(restaurant => restaurant.save()));
    
    // Add a menu to our test restaurant.
    restaurantToTestWith = restaurantList[0];
    restaurantToTestWith.menu = new Menu({
        nom: "testMenu",
        disponibilite: true,
    });

    // Add a category to the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie.push(new Categorie({
        nom: "categorieTest",
        cheminImage: "noice"
    }));

    // Add an article to the article list of the category of the menu of our test restaurant.
    restaurantToTestWith.menu.listeCategorie[0].listeArticle.push(new Article({
        nom: "articleTest",
        description: "descriptionTest",
        cheminImage: "cheminImageTest", 
        prix: 12.99,
        disponibilite: true,
        salleSeulement: true
    }));

    await restaurantToTestWith.save();
    restaurantWithoutMenu = restaurantList[1];
});

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("toutes méthodes catégorie variante avec connexion BD.", () => {
    describe("créer une catégorie variante.", () => {
        test("devrait créer une catégorie variante.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: {
                    nom: "categorieVarianteTest",
                    varianteMinimum: 1,
                    varianteMaximum: 2
                }
            }
            let response = new MockResponse();
        
            await CategorieVarianteController.createCategorieVariante((request as unknown) as any, response as any);
            createdCategorieVariante = response.body as ICategorieVariante;

            expect(createdCategorieVariante.nom).toBe("categorieVarianteTest");
            expect(createdCategorieVariante.varianteMinimum).toBe(1);
            expect(createdCategorieVariante.varianteMaximum).toBe(2);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.createCategorieVariante((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.createCategorieVariante((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.createCategorieVariante((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.createCategorieVariante((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    varianteMinimum: 1,
                    varianteMaximum: 2
                }
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.createCategorieVariante((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        }); 
    });

    describe("mettre à jour une catégorie variante.", () => {
        test("devrait mettre à jour une catégorie variante.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    categorievarianteid: createdCategorieVariante._id
                },
                body: {
                    nom: "newCategorieVarianteTest",
                    varianteMinimum: 10,
                    varianteMaximum: 20
                }
            }
            let response = new MockResponse();

            await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any);
            let expectedCategorieVariante = response.body as ICategorieVariante;

            expect(expectedCategorieVariante.nom).toBe("newCategorieVarianteTest");
            expect(expectedCategorieVariante.varianteMinimum).toBe(10);
            expect(expectedCategorieVariante.varianteMaximum).toBe(20);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id categorie vartiante invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    categorievarianteid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête manquant des champs.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: { // We remove "nom" to trigger a 422.
                    varianteMinimum: 1,
                    varianteMaximum: 2
                }
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.createCategorieVariante((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        }); 

        test("devrait retourner un 422 (UnprocessableEntity) pour une requête avec ses champs invalides.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id
                },
                body: { 
                    nom: 123123,
                    varianteMinimum: "1",
                    varianteMaximum: "2"
                }
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.createCategorieVariante((request as unknown) as any, response as any);
            } catch (e) {
                expect(e.toString()).toBe(UnprocessableEntity);
            }
        }); 
    });

    describe("effacer une catégorie variante.", () => {
        test("devrait effacer une catégorie variante.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    categorievarianteid: createdCategorieVariante._id
                },
                body: {}
            }
            let response = new MockResponse();

            await CategorieVarianteController.deleteCategorieVarianteById((request as unknown) as any, response as any);

            expect(await CategorieVariante.countDocuments({ _id: createdCategorieVariante._id })).toBe(0);
        });

        test("devrait retourner un 404 (NotFound) pour un id restaurant invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: IdNotPresent,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour restaurant sans menu.", async () => {
            let request: IMockRequest = {
                params: {restaurantid: restaurantWithoutMenu._id},
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id catégorie invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id article invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: IdNotPresent
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });

        test("devrait retourner un 404 (NotFound) pour un id categorie vartiante invalide.", async () => {
            let request: IMockRequest = {
                params: {
                    restaurantid: restaurantToTestWith._id,
                    categorieid: restaurantToTestWith.menu?.listeCategorie[0]._id,
                    articleid: restaurantToTestWith.menu?.listeCategorie[0].listeArticle[0]._id,
                    categorievarianteid: createdCategorieVariante._id
                },
                body: ""
            }
            let response = new MockResponse();

            try {
                await CategorieVarianteController.updateCategorieVarianteById((request as unknown) as any, response as any)
            } catch (e) {
                expect(e.toString()).toBe(NotFound);
            }
        });
    });
});