# Note: Working directory is that of Restauration package.json

now=$(date +%s)

echo Generating Postman collection file at timestamp $now;

echo Going to aggregate YAML $ref keys into one big YAML file;

YAMLInputFile=$(realpath $(dirname .)/openapi/restauration.yml);
TMP_AggregateYAMLOutput=/tmp/RestaurationYAMLAggregate-$now.yml;

echo Looking for source YAML file at $YAMLInputFile;
echo Writing temporary YAML aggregate file at $TMP_AggregateYAMLOutput;
./node_modules/json-yaml-ref-resolver/bin/index.js $YAMLInputFile $TMP_AggregateYAMLOutput;

PostmanColOutput=$(realpath $(dirname .)/openapi/dest/postman-collection-$now.json);
echo Compiling Postman collection from $TMP_AggregateYAMLOutput;
echo Writing Postman collection file to $PostmanColOutput;

./node_modules/openapi-to-postmanv2/bin/openapi2postmanv2.js -s $TMP_AggregateYAMLOutput -o $PostmanColOutput;

echo Removing tmp aggregate file;
rm $TMP_AggregateYAMLOutput;

echo All done!
