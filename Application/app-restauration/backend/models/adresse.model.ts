import {Schema, model} from "mongoose";
import {IAdresse} from "./interfaces";

export const AdresseSchema = new Schema({
    googleid: {type: String, required: true}
});

export default model<IAdresse>("adresse", AdresseSchema);