import { Schema, model } from "mongoose";
import { AdresseSchema } from "./adresse.model";
import { ClientSchema } from "./client.model";
import Article from "./restaurant.model";
import { IPanier, ICommande } from "./interfaces";

const PanierSchema = new Schema({
    listeArticle: {
        type: Schema.Types.ObjectId,
        ref: "Article"
    },
    dateCreation: {type: Date, required: true},
    prixTotal: {type: Number, required: true}
})

export const CommandeSchema = new Schema({
    dateCreation:{type:Date, required:true},
    panier: {type: PanierSchema, required: true},
    client: {
        type: Schema.Types.ObjectId,
        ref: "Client"
    },
    adresse: {
        type: Schema.Types.ObjectId,
        ref: "Adresse",
        default: null
    }
});

export const Panier = model<IPanier>("panier", PanierSchema);
export default model<ICommande>("commande", CommandeSchema);