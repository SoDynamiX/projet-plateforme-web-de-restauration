import {PHONE_REGEX, normalizePhoneNumber, EMAIL_REGEX, normalizeEmail, transformIdProperty, removeVersionKey} from "./helpers/validationHelpers"
import { Schema, model } from "mongoose";
import { AdresseSchema } from "./adresse.model";

//TODO Le Schema veut pas s'importe sans être un model..
//import { CommandeSchema } from "./commande.model";
import { IClient } from "./interfaces";

export const ClientSchema = new Schema({
    prenom:{type: String, required: true},
    nom:{type: String, required: true},
    adresse: {
        type: Schema.Types.ObjectId,
        ref: "Adresse",
        default: null
    },
    courriel:{type: String, required: true},
    numeroTelephone:{
        type: String,
        required: true,
        validate: { // Quand la méthode validate est appelée, on utilise le regex pour tester le numéro de téléphone.
            validator: (value: string) => PHONE_REGEX.test(value)
        }
    },
    historiqueCommande: {type: [/* CommandeSchema */], default: null},
    statut:{type: Boolean, required: true},
});
export default model<IClient>("client", ClientSchema);
