import { Document } from "mongoose";

export interface IBaseEntity extends Document {
    _id: string;
    id: string;
    toPublic(): IBaseEntity;
}

export interface IAdresse extends IBaseEntity {
    googleid: string;
}

export interface IArticle extends IBaseEntity {
    nom: string;
    description: string;
    cheminImage: string;
    prix: number;
    listeContrainte: [IContrainte];
    listeExtra: [IExtra];
    listeCategorieVariante: [ICategorieVariante];
    disponibilite: boolean;
    salleSeulement: boolean;
}

export interface ICategorie extends IBaseEntity {
    nom: string;
    cheminImage: string;
    listeArticle: [IArticle];
    listeHoraireCategorie: [IHoraireCategorie]
}

export interface ICategorieVariante extends IBaseEntity {
    nom: string;
    listeVariante: [IVariante];
    varianteMinimum: number;
    varianteMaximum: number;
}

export interface IClient extends IBaseEntity {
    prenom: string;
    nom: string;
    adresse: IAdresse;
    courriel: string;
    numeroTelephone: string;
    historiqueCommande: [ICommande];
    statut: boolean;
}

export interface ICommande extends IBaseEntity {
    panier: IPanier;
    dateConfirmation: Date;
    heureConfirmation: Date;
    client: IClient;
    adresse: IAdresse;
}

export interface IContactRestaurant extends IBaseEntity {
    prenom: string;
    nom: string;
    cheminImage: string;
    courriel: string;
    numeroTelephone: string;
}

export interface IContrainte extends IBaseEntity {
    nom: string;
    cheminImage: string;
}

export interface IExtra extends IBaseEntity {
    nom: string;
    prix: number;
    cheminImage: string;
}

export interface IHoraireCategorie extends IBaseEntity {
    nom: string;
    listeJournee: [IJourneeCategorie];
    EstActif: boolean;
}

export interface IHoraireRestaurant extends IBaseEntity {
    nom: string;
    listeJournee: [IJourneeRestaurant];
    EstActif: boolean;
}

export interface IJourneeCategorie extends IBaseEntity {
    nom: string;
    heureDebut: Date;
    heureFin: Date;
}

export interface IJourneeRestaurant extends IBaseEntity {
    nom: string;
    heureOuverture: Date;
    heureFermeture: Date;
    contrainteHoraire: Date;
}

export interface IMenu extends IBaseEntity {
    nom: string;
    listeCategorie: [ICategorie];
    disponibilite: boolean;
}

export interface IPanier extends IBaseEntity {
    listeArticle: [IArticle];
    dateCreation: Date;
    prixTotal: number;
}

export interface IRestaurant extends IBaseEntity {
    nom: string;
    adresse: IAdresse | null;
    numeroTelephone: string;
    cheminLogo: string;
    courriel: string;
    listeHoraire: [IHoraireRestaurant];
    listeContactRestaurant: [IContactRestaurant];
    menu: IMenu | null;
}

export interface IVariante extends IBaseEntity {
    nom: string;
    prix: number;
    cheminImage: string;
}