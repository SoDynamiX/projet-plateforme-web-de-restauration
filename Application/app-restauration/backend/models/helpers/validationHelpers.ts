import { IBaseEntity } from '../interfaces';
export const PHONE_REGEX = /\d{10,11}/;

export const normalizePhoneNumber = (phoneNumber: string): string => {
  return phoneNumber.trim().replace(/\D/g, '');
};

// Shamelessly taken from https://stackoverflow.com/a/46181/6622623
export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const normalizeEmail = (email: string): string => email.trim().toLowerCase();

export const normalizeHexColorString = (color: string): string => {
  let str = color.trim().toLowerCase();
  if (str[0] !== '#') {
    str = `#${str}`;
  }

  return str;
};

/**
 * Remplace le paramètre _id par le paramètre id.
 * @param doc Le document sur lequel travailler.
 */
export function transformIdProperty(doc: any): void {
  doc.id = doc._id;
  delete doc._id;
}

/**
 * Removes the `__v` property used for internal versionning / locking.
 * Acts in place on the provided object.
 * @param doc Le document sur lequel travailler.
 */
export function removeVersionKey(doc: any): void {
  delete doc.__v;
}