import {PHONE_REGEX, normalizePhoneNumber, EMAIL_REGEX, normalizeEmail, transformIdProperty, removeVersionKey} from "./helpers/validationHelpers"
import {Schema, model} from "mongoose"
import {AdresseSchema} from "./adresse.model"
import {IArticle, ICategorie, ICategorieVariante, IContactRestaurant, 
    IContrainte, IExtra, IHoraireCategorie, IHoraireRestaurant, 
    IJourneeCategorie, IJourneeRestaurant, IMenu, IRestaurant, IVariante} 
    from "./interfaces";

const ContactRestaurantSchema = new Schema({
    prenom:{type:String, required: true},
    nom:{type:String, required: true},
    courriel: {
        type:String,
        required: true,
        validate: { // Quand la méthode validate est appelé, on utilise le regex pour tester le courriel.
            validator: (value: string) => EMAIL_REGEX.test(value)
        }
    },
    numeroTelephone:{
        type: String,
        required: true,
        validate: { // Quand la méthode validate est appelée, on utilise le regex pour tester le numéro de téléphone.
            validator: (value: string) => PHONE_REGEX.test(value)
        }
    },
})

const ListeContrainteSchema = new Schema({
    nom:{type:String, required: true},
    cheminImage:{type:String, required: false},
})

const ListeExtraSchema = new Schema({
    nom:{type:String, required: true},
    prix:{type:Number, required: true},
    cheminImage:{type:String, required: false, default: null}
})

const ListeVarianteSchema = new Schema({
    nom:{type:String, required: true},
    prix:{type:Number, required: true},
    cheminImage:{type:String, required: false, default: null}
})

const ListeCategorieVarianteSchema = new Schema({
    nom:{type:String, required: true},
    listeVariante:{type:[ListeVarianteSchema], default: null},
    varianteMinimum:{type:Number, required: true},
    varianteMaximum:{type:Number, required: true},
})

const ListeArticleSchema = new Schema({
    nom:{type:String, required: true},
    description:{type:String, required: true},
    cheminImage:{type:String, required: false},
    prix:{type:Number, required: true},
    listeContrainte:{type:[ListeContrainteSchema], default: null},
    listeExtra:{type:[ListeExtraSchema], default: null},
    listeCategorieVariante:{type:[ListeCategorieVarianteSchema], default: null},
    disponibilite:{type:Boolean, required: true},
    salleSeulement:{type:Boolean, required: true},
})

const ListeJourneeCategorieSchema = new Schema({
    nom:{type:String, required: true},
    heureDebut:{type:Date, required: true},
    heureFin:{type:Date, required: true}
})

const HoraireCategorieSchema = new Schema({
    nom:{type:String, required: true},
    listeJournee:{type:[ListeJourneeCategorieSchema], default: null},
    EstActif: {type:Boolean, default: false}
})

const CategorieSchema = new Schema({
    nom:{type:String, required: true},
    cheminImage:{type:String, required: false},
    listeArticle: {type:[ListeArticleSchema], default: null},
    listeHoraireCategorie:{type:[HoraireCategorieSchema], default: null},
})

const MenuSchema = new Schema({
    nom: {type: String, required: true},
    listeCategorie: {type:[CategorieSchema], default: null},
    disponibilite:{type:Boolean, required: true},
})

const ListeJourneeHoraireSchema = new Schema({
    nom:{type:String, required: true},
    heureOuverture:{type:Date, required: true},
    heureFermeture:{type:Date, required: true},
    contrainteHoraire:{type:Date, required: true}
})

const HoraireRestaurantSchema = new Schema({
    nom:{type:String, required: true},
    listeJournee: {type:[ListeJourneeHoraireSchema], default: null},
    EstActif: {type:Boolean, default: false}
})

const RestaurantSchema = new Schema({
    nom: {type:String, required: true},
    // Pour l'instant, l'adresse reste une string puisque dans le futur
    // nous implémenterons un API google permettant de recevoir des adresses avec une string unique fournie par Google.
    adresse: {type: String, default: null},
    numeroTelephone:{
        type: String,
        required: true,
        validate: { // Quand la méthode validate est appelée, on utilise le regex pour tester le numéro de téléphone.
            validator: (value: string) => PHONE_REGEX.test(value)
        }
    },
    cheminLogo:{type:String, required: false},
    courriel: {
        type: String,
        required: true,
        validate: { // Quand la méthode validate est appelé, on utilise le regex pour tester le courriel.
            validator: (value: string) => EMAIL_REGEX.test(value)
        }
    },
    listeHoraire: {type:[HoraireRestaurantSchema], default: null},
    listeContactRestaurant: {type:[ContactRestaurantSchema], default: null},
    menu: {type:MenuSchema, default: null}
}); 

// La méthode "pre" permet de bind une fonction à éxécuter avant la méthode validate.
//RestaurantSchema.pre("validate", function (this: IRestaurant) { 
//    // En JS, plusieurs éléments peuvent être "falsy". Si on fait "" == true, on reçoit false.
//    // Donc on vérifie si le paramètre "numeroTelephone" est "truthy" (donc qu'il contient bel-et-bien une string non vide).
//    // Si la string est vide, le run-time short circuit avant les "&&" et passe à la ligne suivante.
//    // Donc au final, si la variable numeroTelephone est valide, la méthode normalisePhoneNumber sera appelée,
//    // sinon il passera à la ligne suivante.
//    this.nom = this.nom && this.nom.trim();
//    this.numeroTelephone = this.numeroTelephone && normalizePhoneNumber(this.numeroTelephone);
//    this.courriel = this.courriel && normalizeEmail(this.courriel);
//});
//
//ContactRestaurantSchema.pre("validate", function (this: IContactRestaurant) {
//    this.prenom = this.prenom && this.prenom.trim();
//    this.nom = this.nom && this.nom.trim();
//    this.courriel = this.courriel && normalizeEmail(this.courriel);
//    this.numeroTelephone = this.numeroTelephone && normalizePhoneNumber(this.numeroTelephone);
//});

/*
RestaurantSchema.method("toPublic", function toPublicRestaurant(this: IRestaurant) {
    const publicObject = this.toObject();
    //transformIdProperty(publicObject);
    //removeVersionKey(publicObject);   
    return publicObject;
})
*/

export const ContactRestaurant = model<IContactRestaurant>("contactRestaurant", ContactRestaurantSchema);
export const Contrainte = model<IContrainte>("contrainte", ListeContrainteSchema);
export const Extra = model<IExtra>("extra", ListeExtraSchema);
export const Variante = model<IVariante>("variante", ListeVarianteSchema);
export const CategorieVariante = model<ICategorieVariante>("categorieVariante", ListeCategorieVarianteSchema);
export const Article = model<IArticle>("article", ListeArticleSchema);
export const JourneeHoraireCategorie = model<IJourneeCategorie>("journeeCategorie", ListeJourneeCategorieSchema);
export const HoraireCategorie = model<IHoraireCategorie>("horaireCategorie", HoraireCategorieSchema);
export const Categorie = model<ICategorie>("categorie", CategorieSchema);
export const Menu = model<IMenu>("menu", MenuSchema);
export const JourneeHoraireRestaurant = model<IJourneeRestaurant>("journeeRestaurant", ListeJourneeHoraireSchema);
export const Horaire = model<IHoraireRestaurant>("horaireRestaurant", HoraireRestaurantSchema);
export default model<IRestaurant>("restaurant", RestaurantSchema);