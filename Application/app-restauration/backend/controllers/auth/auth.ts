import e, { Request, Response, NextFunction } from 'express';
import * as crypto from 'crypto';
import * as createError from 'http-errors';
import { ClientSchema } from "../../models/client.model"
import Client from "../../models/client.model";
import { ClientController } from "../index";
import { IClient } from "../../models/interfaces";
import STATUS_CODES from '../../models/helpers/STATUS_CODES';

/**
 * Prend les informations de l'utilisateur, vérifie si le client existe déjà sinon un nouveau client est créé.
 * @param req La requête HTTP courante. 
 * @param req.body `IClient` information utilisée pour créer le client.
 * @param res La réponse envoyée au client. 
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {Conflict} si le client existe déjà dans la base de donnée.
 * @throws {BadRequest} si l'information donnée est invalide.
 */
export const register = async (req: Request, res: Response) => {
    if (
        req.body.prenom && req.body.nom && req.body.courriel && req.body.statut
    ) {
        let existingClient: IClient | null;
        try {
            existingClient = await Client.findOne({ googleid: req.params.googleid });
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError();
        }

        if (!existingClient) {
            const client = ClientController.createClient(req, res);
            res.send(client);
        } else {
            throw new createError.Conflict();
        }
    } else {
        throw new createError.BadRequest();
    }
}

// export const login = async (req: Request, res: Response) => {
//     if (req.user) {
//         req.logIn(req.user, (err) => {
//             if (!err) {
//                 if (req.user) {
//                     res.send((req.user as IClient).toPublic());
//                 } else {
//                     throw new createError.InternalServerError();
//                 }
//             } else {
//                 // next(err);
//             }
//         })
//     } else {
//         throw new createError.Forbidden();
//     }
// }

// export const getCurrentSession = async (req: Request, res: Response) => {
//     if (req.user) {
//         res.send(req.user);
//     } else {
//         throw new createError.Forbidden();
//     }
// }