import * as ClientController from "./clients";
import * as CommandeController from "./commandes";
import * as RestaurantController from "./restaurant";

export {
    ClientController,
    CommandeController,
    RestaurantController,
};