import { Request, Response, NextFunction, response } from "express";
import * as createError from "http-errors";
import Client from "../models/client.model";
import STATUSCODE from "../models/helpers/STATUS_CODES";
import { IClient } from "../models/interfaces";

/**
 * Renvoie tous les clients de la base de donnée.
 * Renvoie un `Client[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 */
export const getAllClients = async (req: Request, res: Response) => {
    let clients: IClient[] | null;
    try {
        clients = await Client.find();
    } catch (e) {
        console.log(e);
        throw new createError.InternalServerError();
    }
    
    res.send(clients);
}

/**
 * Retourne le client recherché avec l'id donné dans la base de donnée.
 * Renvoie un `Client`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id est invalide.
 */
export const getOneClient = async (req: Request, res: Response) => {
    let client: IClient | null;
    try {
        client = await Client.findById(req.params.clientid);
    } catch (e) {
        console.log(e);
        throw new createError.InternalServerError();
    }
    if (client) { // For void safety.
        res.send(client);
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Créer un client et le sauvegarde à la base de donnée.
 * @param req La requête HTTP courante. 
 * @param req.body `IClient` information utilisée pour créer le client.
 * @param res La réponse envoyée au client. 
 * 
 * @throws {UnprocessableEntity} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {BadRequest} si l'information donnée est invalide.
 */
export const createClient = async (req: Request, res: Response) => {
    const client = new Client({
        prenom: req.body.prenom,
        nom: req.body.nom,
        adresse: req.body.adresse,
        courriel: req.body.courriel,
        nomUtilisateur: req.body.nomUtilisateur,
        motDePasseHashed: req.body.motDePasseHashed,
        numeroTelephone: req.body.numeroTelephone,
        historiqueCommande: req.body.historiqueCommande,
        statut: req.body.statut,
    });

    try {
        await client.validate();
    } catch (e) {
        console.log(e);
        throw new createError.UnprocessableEntity();
    }

    try {
        await client.save();
        res.status(STATUSCODE.CREATED).send(client);
    } catch (e) {
        console.log(e);
        throw new createError.InternalServerError();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_CLIENT_UPDATE_FIELD = [
    "courriel","motDePasseHashed", 
    "numeroTelephone", "statut"
];

/**
 * Met à jour et sauvegarde les informations d'un client recherché par son id.
 * @param req La requête HTTP courante.
 * @param req.body `IClient` l'information utilisée pour mettre à jour le client.
 * @param res La réponse envoyée au client. 
 * 
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du client donnée est invalide.
 */
export const updateClientById = async (req: Request, res: Response) => {
    let clientToUpdate: IClient | null;
    try {
        clientToUpdate = await Client.findById(req.params.clientid);
    } catch (e) {
        console.log(e);
        throw new createError.InternalServerError();
    }

    if (clientToUpdate) { // For void safety.
        for (const k in req.body) {
            if (ALLOWED_CLIENT_UPDATE_FIELD.includes(k)) {
                (clientToUpdate as any)[k] = req.body[k];
            } else {
                throw new createError.UnprocessableEntity();
            }
        }

        try {
            await clientToUpdate.validate();
        } catch (e) {
            console.log(e);
            throw new createError.UnprocessableEntity();
        }

        try {
            clientToUpdate.save();
            res.status(STATUSCODE.OK).send(clientToUpdate);
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Efface le client de la base de donnée.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client. 
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 */
export const deleteClientById = async (req: Request, res: Response) => {
    let clientToDelete: IClient | null;
    try {
        clientToDelete = await Client.findById(req.params.clientid);
    } catch (e) {
        console.log(e);
        throw new createError.InternalServerError();
    }
    
    if (clientToDelete) {
        try {
            await Client.deleteOne({ _id: req.params.clientid });
            res.end();
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError();
        }
    } else {
        throw new createError.NotFound();
    }
}