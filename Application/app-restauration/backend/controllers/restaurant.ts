import { Request, Response, NextFunction } from 'express';
import * as createError from 'http-errors';
import STATUSCODE from "../models/helpers/STATUS_CODES";
import { IRestaurant } from '../models/interfaces';
import Restaurant from "../models/restaurant.model";

/**
 * Renvoie tous les restaurants de la base de donnée.
 * Renvoie un `Restaurant[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 */
export const findAllRestaurants = async (req: Request, res: Response) => {
    let restaurants: IRestaurant[] | null;
    try {
        restaurants = await Restaurant.find();
    } catch (e) {
        throw new createError.InternalServerError();
    }
    res.send(restaurants);
}
/**
 * Renvoie le restaurant recherché avec l'id donné dans la request.
 * Renvoie un `Restaurant`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id est invalide.
 */
export const findOneRestaurant = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        res.send(restaurant);
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Créer un restaurant et le sauvegarde à la base de donnée.
 * @param req La requête HTTP courante.
 * @param req.body `IRestaurant` information utilisée pour créer le restaurant.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 */
export const createRestaurant = async (req: Request, res: Response) => {
    const restaurant = new Restaurant({
        nom: req.body.nom,
        adresse: req.body.adresse,
        numeroTelephone: req.body.numeroTelephone,
        cheminLogo: req.body.cheminLogo,
        courriel: req.body.courriel,
        listeHoraire: req.body.listeHoraire,
        listeContactRestaurant: req.body.listeContactRestaurant,
        menu: req.body.menu
    });

    try {
        await restaurant.validate();
    } catch (e) {
        console.log(e);
        throw new createError.UnprocessableEntity();
    }

    try {
        await restaurant.save();
        res.status(STATUSCODE.CREATED).send(restaurant);
    } catch (e) {
        console.log(e);
        throw new createError.InternalServerError();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_RESTAURANT_UPDATE_FIELD = [
    "nom", "numeroTelephone",
    "cheminLogo", "courriel"
];

/**
 * Met à jour et sauvegarde les informations d'un restaurant recherché par son id.
 * @param req La requête HTTP courante.
 * @param req.body `IRestaurant` l'information utilisée pour mettre à jour le restaurant.
 * @param res La réponse envoyée au client.
 * 
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si le restaurant ne passe pas la validation.
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 */
export const updateRestaurantById = async (req: Request, res: Response) => {
    let restaurantToUpdate: IRestaurant | null;
    try {
        restaurantToUpdate = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurantToUpdate) {
        for (const k in req.body) { // For each field (k est standard pour key en TS).
            if (ALLOWED_RESTAURANT_UPDATE_FIELD.includes(k)) {
                (restaurantToUpdate as any)[k] = req.body[k];
            } else {
                throw new createError.UnprocessableEntity();
            }
        }

        try {
            await restaurantToUpdate.validate();
        } catch (e) {
            console.log(e);
            throw new createError.UnprocessableEntity();
        }

        try {
            await restaurantToUpdate.save();
            res.status(STATUSCODE.OK).send(restaurantToUpdate);
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface le restaurant de la base de donnée.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 */
export const deleteRestaurantById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        try {
            await Restaurant.deleteOne({ _id: req.params.restaurantid });
            res.end();
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError();
        }
    } else {
        throw new createError.NotFound();
    }
}