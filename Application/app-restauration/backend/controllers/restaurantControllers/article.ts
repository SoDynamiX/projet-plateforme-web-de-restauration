import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { Article } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";


/**
 * Renvoie tous les articles d'une catégorie.
 * Renvoi une `Article[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si la liste d'articles de l'horaire est vide.
 */

export const getAllArticles = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id === req.params.categorieid);

            if (categorie) {
                const listeArticle = categorie.listeArticle;

                if (listeArticle) {
                    res.send(listeArticle);
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Renvoie l'article recherché avec l'id donné dans la request.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 */

export const getOneArticle = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id === req.params.categorieid);

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id === req.params.articleid);

                if (article) {
                    res.send(article);
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Créer un article et le sauvegarde dans la liste d'article de la catégorie.
 * @param req La requête HTTP courante.
 * @param req.body `IArticle` information utilisée pour créer l'article.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde.
 * @throws {UnprocessableEntity} si l'article ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 */
export const createArticle = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }


    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const newArticle = new Article({
                    nom: req.body.nom,
                    description: req.body.description,
                    cheminImage: req.body.cheminImage,
                    prix: req.body.prix,
                    listeContrainte: req.body.listeContrainte,
                    listeExtra: req.body.listeExtra,
                    listeCategorieVariante: req.body.listeCategorieVariante,
                    disponibilite: req.body.disponibilite,
                    salleSeulement: req.body.salleSeulement
                })

                try {
                    await newArticle.validate()
                } catch (e) {
                    console.log(e);
                    throw new createError.UnprocessableEntity();
                }

                try {
                    categorie.listeArticle.push(newArticle);
                    await restaurant.save();
                    res.status(STATUSCODE.CREATED).send(newArticle);
                } catch (e) {
                    console.log(e);
                    throw new createError.InternalServerError();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Efface l'article de la liste d'articles de la catégorie donnée.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 */
export const deleteArticleById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const index = categorie.listeArticle
                    .findIndex(article => article.id == req.params.articleid.toString());

                if (index != -1) {
                    categorie.listeArticle.splice(index, 1);

                    try {
                        await restaurant.save();
                        res.end();
                    } catch (e) {
                        console.log(e);
                        throw new createError.InternalServerError();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_ARTICLE_UPDATE_FIELD = [
    "nom", "description",
    "cheminImage", "prix",
    "disponibilite", "salleSeulement"
];

/**
 * Met à jour et sauvegarde les informations d'un article dans une catégorie d'articles.
 * @param req La requête HTTP courante.
 * @param req.body `IArticle` information utilisée pour mettre à jour l'article donné.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si l'article ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * 
 */
export const updateArticleById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const articleToUpdate = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());
                if (articleToUpdate) {
                    for (const k in req.body) {
                        if (ALLOWED_ARTICLE_UPDATE_FIELD.includes(k)) {
                            (articleToUpdate as any)[k] = req.body[k];
                        } else {
                            throw new createError.UnprocessableEntity();
                        }
                    }

                    try {
                        await articleToUpdate.validate();
                    } catch (e) {
                        console.log(e);
                        throw new createError.UnprocessableEntity();
                    }

                    try {
                        await restaurant.save();
                        res.status(STATUSCODE.OK).send(articleToUpdate);
                    } catch (e) {
                        console.log(e);
                        throw new createError.InternalServerError();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
