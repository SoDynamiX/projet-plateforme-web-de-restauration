import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { CategorieVariante } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";

/**
 * Renvoie toutes les catégories de variantes d'un article.
 * Renvoie un `CategorieVariante[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si la liste de catégorie variantes de l'article est vide.
 */

export const getAllCategorieVariantes = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    if (article.listeCategorieVariante) {
                        res.send(article.listeCategorieVariante);
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Renvoie la catégorie variante recherchée avec l'id donné dans la request.
 * Renvoie une `CategorieVariante`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la catégorie de variantes est invalide.
 */

export const getOneCategorieVariante = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const categorieVariante = article.listeCategorieVariante
                        .find(categorieVariante => categorieVariante.id == req.params.categorievarianteid);

                    if (categorieVariante) {
                        res.send(categorieVariante);
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Créer une catégorie de variantes dans la liste de catégories variantes de l'article donné.
 * @param req La requête HTTP courante.
 * @param req.body `ICategorieVariante` information utilisée pour créer une catégorie variante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si la catégorie variante ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 */
export const createCategorieVariante = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const newCategorieVariante = new CategorieVariante({
                        nom: req.body.nom,
                        listeVariante: req.body.listeVariante,
                        varianteMinimum: req.body.varianteMinimum,
                        varianteMaximum: req.body.varianteMaximum,
                    })

                    try {
                        await newCategorieVariante.validate();
                    } catch (e) {
                        console.log(e);
                        throw new createError.UnprocessableEntity();
                    }

                    try {
                        article.listeCategorieVariante.push(newCategorieVariante);
                        await restaurant.save();
                        res.status(STATUSCODE.CREATED).send(newCategorieVariante);
                    } catch (e) {
                        console.log(e);
                        throw new createError.InternalServerError();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface la catégorie de variantes de la liste de catégorie variantes de l'article donné
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la catégorie variante est invalide.
 */
export const deleteCategorieVarianteById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const index = article.listeCategorieVariante
                        .findIndex(categorieVariante => categorieVariante.id == req.params.categorievarianteid);

                    if (index != -1) {
                        article.listeCategorieVariante.splice(index, 1);

                        try {
                            await restaurant.save();
                            res.end();
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_VARIANT_CATEGORY_UPDATE_FIELD = [
    "nom", "varianteMinimum",
    "varianteMaximum"
];
/**
 * Met à jour et sauvegarde les informations de la catégorie de variantes donnée.
 * @param req La requête HTTP courante.
 * @param req.body `ICategorieVariante` information utilisée pour mettre à jour la catégorie de variantes.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si la catégorie de variantes ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la catégorie de variantes est invalide.
 */
export const updateCategorieVarianteById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const categorieVarianteToUpdate = article.listeCategorieVariante
                        .find(categorieVariante => categorieVariante.id == req.params.categorievarianteid);

                    if (categorieVarianteToUpdate) {
                        for (const k in req.body) {
                            if (ALLOWED_VARIANT_CATEGORY_UPDATE_FIELD.includes(k)) {
                                (categorieVarianteToUpdate as any)[k] = req.body[k];
                            } else {
                                throw new createError.UnprocessableEntity();
                            }
                        }

                        try {
                            await categorieVarianteToUpdate.validate();
                        } catch (e) {
                            console.log(e);
                            throw new createError.UnprocessableEntity();
                        }

                        try {
                            await restaurant.save();
                            res.status(STATUSCODE.OK).send(categorieVarianteToUpdate);
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}