import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { JourneeHoraireCategorie } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";

/**
 * Renvoie toutes les journées d'un horaire d'une catégorie.
 * Renvoie un `JourneeCategorie[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'horaire est invalide.
 * @throws {NotFound} si la liste de journées de l'horaire est vide.
 */
export const getAllJourneeCategories = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const horaireCategorie = categorie.listeHoraireCategorie
                    .find(horaireCategorie => horaireCategorie.id == req.params.horaireid.toString());

                if (horaireCategorie) {
                    if (horaireCategorie.listeJournee) {
                        res.send(horaireCategorie);
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Renvoie la journée recherchée d'un horaire avec l'id donnée dans la request.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'horaire de la catégorie est invalide.
 * @throws {NotFound} si l'id de la journées de l'horaire est invalide.
 */
export const getOneJourneeCategorie = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const horaireCategorie = categorie.listeHoraireCategorie
                    .find(horaireCategorie => horaireCategorie.id == req.params.horaireid.toString());

                if (horaireCategorie) {
                    const journeeCategorie = horaireCategorie.listeJournee
                        .find(journee => journee.id == req.params.journeecategorieid);
                    if (journeeCategorie) {
                        res.send(journeeCategorie);
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Créer une journée catégorie et la sauvegarde à l'horaire.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si la journée ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de la l'horaire de la catégorie est invalide.
 */
export const createJourneeCategorie = async (req: Request, res: Response) => {

    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const horaireCategorie = categorie.listeHoraireCategorie
                    .find(horaireCategorie => horaireCategorie.id == req.params.horaireid.toString());
                if (horaireCategorie) {
                    const newJourneeCategorie = new JourneeHoraireCategorie({
                        nom: req.body.nom,
                        heureDebut: req.body.heureDebut,
                        heureFin: req.body.heureFin
                    })

                    try {
                        horaireCategorie.validate()
                    } catch (e) {
                        console.log(e);
                        throw new createError.UnprocessableEntity();
                    }


                    try {
                        horaireCategorie.listeJournee.push(newJourneeCategorie);
                        await restaurant.save();
                        res.status(STATUSCODE.CREATED).send(newJourneeCategorie);
                    } catch (e) {
                        console.log(e);
                        throw new createError.InternalServerError();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface la journée de l'horaire de la catégorie.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'horaire est invalide.
 * @throws {NotFound} si l'id de la journée est invalide.
 * 
 */
export const deleteJourneeCategorieById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const horaireCategorie = categorie.listeHoraireCategorie
                    .find(horaireCategorie => horaireCategorie.id == req.params.horaireid.toString());

                if (horaireCategorie) {
                    const journeeToDelete = horaireCategorie.listeJournee
                        .find(journee => journee.id == req.params.journeeid.toString());

                    if (journeeToDelete) {
                        const indexJournee = horaireCategorie.listeJournee.indexOf(journeeToDelete);
                        horaireCategorie.listeJournee.splice(indexJournee, 1);

                        try {
                            await restaurant.save();
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_JOURNEECATEGORIE_UPDATE_FIELD = ["nom", "heureDebut", "heureFin"];

/**
 * Met à jour et sauvegarde les informations de la journée de la catégorie.
 * @param req La requête HTTP courante.
 * @param req.body `IJourneeCategorie` information utilisée pour mettre à jour la journée.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si la journée ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'horaire de la catégorie est invalide.
 * @throws {NotFound} si l'id de la journée est invalide.
 */
export const updateJourneeCategorieById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const horaireCategorie = categorie.listeHoraireCategorie
                    .find(horaire => horaire.id == req.params.horaireid.toString());

                if (horaireCategorie) {
                    const journeeToUpdate = horaireCategorie.listeJournee
                        .find(journee => journee.id == req.params.journeeid.toString());

                    if (journeeToUpdate) {
                        for (const k in req.body) {
                            if (ALLOWED_JOURNEECATEGORIE_UPDATE_FIELD.includes(k)) {
                                (journeeToUpdate as any)[k] = req.body[k];
                            } else {
                                throw new createError.UnprocessableEntity();
                            }
                        }

                        try {
                            await journeeToUpdate.validate();
                        } catch (e) {
                            console.log(e);
                            throw new createError.UnprocessableEntity();
                        }

                        try {
                            await restaurant.save();
                            res.status(STATUSCODE.OK).send(journeeToUpdate);
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
