import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { JourneeHoraireRestaurant } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";


/**
 * Renvoie tous les journées horaires d'un restaurant recherché.
 * Renvoie un `JourneeHoraire[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si l'id de l'horaire du restaurant recherché est invalide.
 * @throws {NotFound} si la liste de journée de l'horaire est vide.
 */
export const getAllJourneeRestaurants = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const horaire = restaurant.listeHoraire
            .find(horaire => horaire.id == req.params.horaireid.toString());
        if (horaire) {
            const journees = horaire.listeJournee;

            if (journees) {
                res.send(journees);
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Retourne la journée horaire recherchée avec l'id donné dans la request.
 * Renvoie une `JourneeHoraire`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si l'id de l'horaire est invalide.
 * @throws {NotFound} si l'id de la journée horaire est invalide.
 */
export const getOneJourneeRestaurant = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const horaire = restaurant.listeHoraire
            .find(horaire => horaire.id == req.params.horaireid.toString());
        if (horaire) {
            const journee = horaire.listeJournee
                .find(journee => journee.id === req.params.journeeid);
            if (journee) {
                res.send(journee);
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Créer une journée horaire et la sauvegarde dans un horaire.
 * @param req La requête HTTP courante.
 * @param req.body `IJourneeHoraire` information utilisée pour créer la journée horaire.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si l'id de l'horaire est invalide.
 */
export const createJourneeRestaurant = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const horaire = restaurant.listeHoraire
            .find(horaire => horaire.id == req.params.horaireid.toString());
        if (horaire) {
            const newJourneeRestaurant = new JourneeHoraireRestaurant({
                nom: req.body.nom,
                heureOuverture: req.body.heureOuverture,
                heureFermeture: req.body.heureFermeture,
                contrainteHoraire: req.body.contrainteHoraire,
            })

            try {
                newJourneeRestaurant.validate()
                horaire.listeJournee.push(newJourneeRestaurant);
            } catch (e) {
                console.log(e);
                throw new createError.UnprocessableEntity();
            }

            try {
                await restaurant.save();
                res.status(STATUSCODE.CREATED).send(newJourneeRestaurant);
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface la journée de l'horaire et sauvegarde le restaurant.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si l'id de l'horaire donné est invalide.
 * @throws {NotFound} si l'id de la journée horaire donné est invalide.
 */
export const deleteJourneeRestaurantById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const horaire = restaurant.listeHoraire
            .find(horaire => horaire.id == req.params.horaireid.toString());
        if (horaire) {
            const index = horaire.listeJournee
                .findIndex(journee => journee.id === req.params.journeeid);
            if (index != -1) {
                horaire.listeJournee.splice(index, 1);

                try {
                    restaurant.save();
                    res.end();
                } catch (e) {
                    console.log(e);
                    throw new createError.InternalServerError();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_RESTAURANT_DAY_UPDATE_FIELD = [
    "nom", "heureOuverture",
    "heureFermeture", "contrainteHoraire"
];

/**
 * Met à jour et sauvegarde les informations d'une journée horaire à un horaire.
 * @param req La requête HTTP courante.
 * @param req.body `IJourneeHoraire` l'information utilisée pour mettre à jour la journée horaire.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si la journée à mettre à jour ne passe pas la validation..
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si l'id de l'horaire donné est invalide.
 * @throws {NotFound} si l'id de la journée horaire donné est invalide.
 */
export const updateJourneeRestaurantById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const horaire = restaurant.listeHoraire
            .find(horaire => horaire.id == req.params.horaireid.toString());
        if (horaire) {
            const journeeToUpdate = horaire.listeJournee
                .find(journee => journee.id == req.params.journeeid.toString());
            if (journeeToUpdate) {
                for (const k in req.body) {
                    if (ALLOWED_RESTAURANT_DAY_UPDATE_FIELD.includes(k)) {
                        (journeeToUpdate as any)[k] = req.body[k];
                    } else {
                        throw new createError.UnprocessableEntity();
                    }
                }

                try {
                    await journeeToUpdate.validate();
                } catch (e) {
                    console.log(e);
                    throw new createError.UnprocessableEntity();
                }

                try {
                    await restaurant.save();
                    res.status(STATUSCODE.OK).send(journeeToUpdate);
                } catch (e) {
                    console.log(e);
                    throw new createError.InternalServerError();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}