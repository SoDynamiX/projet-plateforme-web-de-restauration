import { IRestaurant, IMenu } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { Categorie } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";

/**
 * Renvoie toutes les catégories d'un menu.
 * Renvoie un `Categorie[]`
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si la liste de catégories du menu est vide.
 */

export const getAllCategories = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categories = restaurant.menu.listeCategorie;
            if (categories) {
                res.send(categories);
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Renvoie l'horaire recherché d'un menu avec l'id donné par la request.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 */

export const getOneCategorie = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id === req.params.categorieid);
            if (categorie) {
                res.send(categorie);
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Créer une catégorie et la sauvegarde au menu d'un restaurant.
 * @param req La requête HTTP courante.
 * @param req.body `ICategorie` information utilisée pour créer la catégorie.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * 
 */
export const createCategorie = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid.toString());
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const newCategorie = new Categorie({
                nom: req.body.nom,
                cheminImage: req.body.cheminImage,
                listeArticle: req.body.listeArticle,
                listeHoraireCategorie: req.body.horaireCategorie
            })

            try {
                await newCategorie.validate();
            } catch (e) {
                console.log(e);
                throw new createError.UnprocessableEntity();
            }

            try {
                restaurant.menu.listeCategorie.push(newCategorie);
                await restaurant.save();
                res.status(STATUSCODE.CREATED).send(newCategorie);
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface la catégorie de la liste de catégorie du menu donné.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 */
export const deleteCategorieById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const index = restaurant.menu.listeCategorie
                .findIndex(categorie => categorie._id == req.params.categorieid.toString());

            if (index != -1) {
                restaurant.menu.listeCategorie.splice(index, 1);

                try {
                    await restaurant.save();
                    res.end();
                } catch (e) {
                    console.log(e);
                    throw new createError.InternalServerError();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}


// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_CATEGORY_UPDATE_FIELD = ["nom", "cheminImage"];
/**
 * Met à jour et sauvegarde les informations d'une catégorie d'un menu.
 * @param req La requête HTTP courante.
 * @param req.body `ICategorie` information utilisée pour mettre à jour la catégorie d'un menu.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si la catégorie ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 */
export const updateCategorieById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorieToUpdate = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorieToUpdate) {
                for (const k in req.body) {
                    if (ALLOWED_CATEGORY_UPDATE_FIELD.includes(k)) {
                        (categorieToUpdate as any)[k] = req.body[k];
                    } else {
                        throw new createError.UnprocessableEntity();
                    }
                }

                try {
                    await categorieToUpdate.validate();
                } catch (e) {
                    console.log(e);
                    throw new createError.UnprocessableEntity();
                }

                try {
                    await restaurant.save();
                    res.status(STATUSCODE.OK).send(categorieToUpdate);
                } catch (e) {
                    console.log(e);
                    throw new createError.InternalServerError();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}