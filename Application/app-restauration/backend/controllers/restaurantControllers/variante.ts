import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { Variante } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";

/**
 * Renvoie toutes les variantes d'une catégorie de variantes.
 * Renvoie un `Variante[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la catégorie de variantes est invalide.
 * @throws {NotFound} si la liste de variantes de la catégorie est vide.
 */
export const getAllVariantes = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const categorieVariante = article.listeCategorieVariante
                        .find(categorieVariante => categorieVariante.id == req.params.categorievarianteid.toString());

                    if (categorieVariante) {
                        if (categorieVariante.listeVariante) {
                            res.send(categorieVariante.listeVariante);
                        } else {
                            throw new createError.NotFound();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Renvoie la variante recherchée avec l'id donné dans la request.
 * Renvoie une `Variante`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la catégorie de variantes est invalide.
 * @throws {NotFound} si l'id de la variante est invalide.
 */
export const getOneVariante = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) { // For void safety,
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const categorieVariante = article.listeCategorieVariante
                        .find(categorieVariante => categorieVariante.id == req.params.categorievarianteid.toString());

                    if (categorieVariante) {
                        const variante = categorieVariante.listeVariante
                            .find(variante => variante.id == req.params.varianteid.toString());

                        if (variante) {
                            res.send(variante);
                        } else {
                            throw new createError.NotFound();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Créer une variante dans la liste de variantes de la catégorie de variantes.
 * @param req La requête HTTP courante.
 * @param req.body `IVariante` information utilisée pour créer une variante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si la variante ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la catégorie de variantes est invalide.
 */
export const createVariante = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const categorieVariante = article.listeCategorieVariante
                        .find(categorieVariante => categorieVariante.id == req.params.categorievarianteid.toString());

                    if (categorieVariante) {
                        const newVariante = new Variante({
                            nom: req.body.nom,
                            prix: req.body.prix,
                            cheminImage: req.body.cheminImage
                        })

                        try {
                            await newVariante.validate();

                        } catch (e) {
                            console.log(e);
                            throw new createError.UnprocessableEntity();
                        }

                        try {
                            categorieVariante.listeVariante.push(newVariante);
                            await restaurant.save();
                            res.status(STATUSCODE.CREATED).send(newVariante);
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface la variante de la liste de variantes de la liste de catégories de variantes.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la catégorie variante est invalide.
 * @throws {NotFound} si l'id de la variante est invalide.
 */
export const deleteVarianteById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const categorieVariante = article.listeCategorieVariante
                        .find(categorieVariante => categorieVariante.id == req.params.categorievarianteid.toString());

                    if (categorieVariante) {
                        const index = categorieVariante.listeVariante
                            .findIndex(variante => variante.id == req.params.varianteid.toString());

                        if (index) {
                            categorieVariante.listeVariante.splice(index, 1);
                            try {
                                await restaurant.save();
                                res.end();
                            } catch (e) {
                                console.log(e);
                                throw new createError.InternalServerError();
                            }
                        } else {
                            throw new createError.NotFound();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_VARIANT_UPDATE_FIELD = ["nom", "prix", "cheminImage"];
/**
 * Met à jour et sauvegarde les informations de la variante donnée.
 * @param req La requête HTTP courante.
 * @param req.body `IVariante` information utilisée pour la mise à jour de la variante donnée.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si la variante ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la catégorie de variantes est invalide.
 * @throws {NotFound} si l'id de la variante est invalide.
 */
export const updateVarianteById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const categorieVariante = article.listeCategorieVariante
                        .find(categorieVariante => categorieVariante.id == req.params.categorievarianteid.toString());

                    if (categorieVariante) {
                        const varianteToUpdate = categorieVariante.listeVariante
                            .find(variante => variante.id == req.params.varianteid.toString());

                        if (varianteToUpdate) {
                            for (const k in req.body) {
                                if (ALLOWED_VARIANT_UPDATE_FIELD.includes(k)) {
                                    (varianteToUpdate as any)[k] = req.body[k];
                                } else {
                                    throw new createError.UnprocessableEntity();
                                }
                            }

                            try {
                                await varianteToUpdate.validate();
                            } catch (e) {
                                console.log(e);
                                throw new createError.UnprocessableEntity();
                            }

                            try {
                                await restaurant.save();
                                res.status(STATUSCODE.OK).send(varianteToUpdate);
                            } catch (e) {
                                console.log(e);
                                throw new createError.InternalServerError();
                            }
                        } else {
                            throw new createError.NotFound();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}