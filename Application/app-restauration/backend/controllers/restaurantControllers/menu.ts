import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { Menu } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";


/**
 * Renvoie le menu du restaurant donné.
 * Renvoie un `Menu`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 */
export const getMenu = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            res.send(restaurant.menu);
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Créer un menu et le sauvegarde au restaurant donné.
 * @param req La requête HTTP courante.
 * @param req.body `IMenu` information utilisée pour créer un menu.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si le menu ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 */
export const createMenu = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const newMenu = new Menu({
            nom: req.body.nom,
            listeCategorie: req.body.listeCategorie,
            disponibilite: req.body.disponibilite,
        })

        try {
            await newMenu.validate();

            try {
                restaurant.menu = newMenu;
                await restaurant.save();
                res.status(STATUSCODE.OK).send(restaurant.menu);
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } catch (e) {
            console.log(e);
            throw new createError.UnprocessableEntity();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface le menu du restaurant donné.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 */
export const deleteMenuById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            try {
                await restaurant.save();
                res.end();
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_MENU_UPDATE_FIELD = ["nom", "disponibilite"];

/**
 * Met à jour et sauvegarde les informations d'un menu au restaurant donné.
 * @param req La requête HTTP courante.
 * @param req.body `IMenu` information utilisée pour mettre à jour le menu d'un restaurant donné.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si le menu ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 */
export const updateMenuById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            for (const k in req.body) {
                if (ALLOWED_MENU_UPDATE_FIELD.includes(k)) {
                    (restaurant.menu as any)[k] = req.body[k];
                } else {
                    throw new createError.UnprocessableEntity();
                }
            }

            try {
                await restaurant.menu.validate();
            } catch (e) {
                console.log(e);
                throw new createError.UnprocessableEntity();
            }

            try {
                await restaurant.save();
                res.status(STATUSCODE.OK).send(restaurant.menu);
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
