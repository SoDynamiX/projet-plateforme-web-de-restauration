import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { ContactRestaurant } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";
// getAllContactRestaurants and getOneContactRestaurant will not be used because of how document databases work.

/**
 * Renvoie tous les contacts restaurants d'un restaurant donné.
 * Renvoie un `ContactRestaurant[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si la liste de contacts du restaurant est vide.
 */
export const getAllContactRestaurants = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const contacts = restaurant.listeContactRestaurant;
        if (contacts) {
            res.send(contacts);
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Renvoie le contact restaurant recherché avec l'id donné dans la request.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si l'id du contact restaurant est invalide.
 */
export const getOneContactRestaurant = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const contact = restaurant.listeContactRestaurant
            .find(contact => contact.id === req.params.contactid);
        if (contact) {
            res.send(contact);
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Créer un contact restaurant et le sauvegarde à la liste de contacts restaurants du restaurant donné.
 * @param req La requête HTTP courante.
 * @param req.body `IContactRestaurant` information utilisée pour créer un contact restaurant.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si le contact restaurant ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 */
export const createContactRestaurant = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const newContact = new ContactRestaurant({
            prenom: req.body.prenom,
            nom: req.body.nom,
            courriel: req.body.courriel,
            numeroTelephone: req.body.numeroTelephone
        });

        try {
            await newContact.validate();
        } catch (e) {
            console.log(e);
            throw new createError.UnprocessableEntity();
        }

        try {
            restaurant.listeContactRestaurant.push(newContact);
            await restaurant.save();
            res.status(STATUSCODE.CREATED).send(newContact);
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError();
        }

    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface le contact restaurant de la liste de contacts restaurants du restaurant donné.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si l'id du contact restaurant est invalide.
 * 
 */
export const deleteContactRestaurantById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const index = restaurant.listeContactRestaurant
            .findIndex(contact => contact._id == req.params.contactid.toString());
        if (index != -1) {
            restaurant.listeContactRestaurant.splice(index, 1);

            try {
                await restaurant.save();
                res.end();
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_CONTACT_UPDATE_FIELD = [
    "prenom", "nom",
    "courriel", "numeroTelephone"
];

/**
 * Met à jour et sauvegarde les informations du contact restaurant recherché dans la liste de contacts d'un restaurant.
 * @param req La requête HTTP courante.
 * @param req.body `IContactRestaurant` information utilisée pour mettre à jour le contact restaurant recherché.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si le contact restaurant à mettre à jour ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si l'id du contact restaurant donné est invalide.
 * 
 */
export const updateContactRestaurantById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        // Using "=="" on id verification is fine here because JS will coherse the object back into string for comparison.
        const contactToUpdate = restaurant.listeContactRestaurant
            .find(contact => contact._id == req.params.contactid.toString());
        if (contactToUpdate) {
            for (const k in req.body) {
                if (ALLOWED_CONTACT_UPDATE_FIELD.includes(k)) {
                    (contactToUpdate as any)[k] = req.body[k];
                } else {
                    throw new createError.UnprocessableEntity();
                }
            }

            try {
                await contactToUpdate.validate();
            } catch (e) {
                console.log(e);
                throw new createError.UnprocessableEntity();
            }

            try {
                await restaurant.save();
                res.status(STATUSCODE.OK).send(contactToUpdate);
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
