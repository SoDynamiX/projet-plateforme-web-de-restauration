import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { Horaire } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";


/**
 * Renvoie tous les horaires d'un restaurant recherché.
 * Renvoie un `HoraireRestaurant[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si la liste d'horaire du restaurant est vide.
 */
export const getAllHoraireRestaurants = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) { // For void safety
        const horaireRestaurant = restaurant.listeHoraire;
        if (horaireRestaurant) {
            res.send(horaireRestaurant);
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Retourne le client recherché avec l'id donné dans la request.
 * Renvoie un `Horaire`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si l'id de l'horaire est invalide.
 */
export const getOneHoraireRestaurant = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const horaireRestaurant = restaurant.listeHoraire
            .find(horaire => horaire.id === req.params.horaireid);
        if (horaireRestaurant) {
            res.send(horaireRestaurant);
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Créer un horaire restaurant et le sauvegarde à la base de donnée.
 * @param req La requête HTTP courante.
 * @param req.body `IHoraireRestaurant` information utilisée pour créer l'horaire restaurant.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {NotFound} si l'id du restaurant est invalide.
 */
export const createHoraireRestaurant = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const newHoraireRestautant = new Horaire({
            nom: req.body.nom,
            listeJournee: req.body.listeJournee
        });

        try {
            await newHoraireRestautant.validate();
            restaurant.listeHoraire.push(newHoraireRestautant);
        } catch (e) {
            console.log(e);
            throw new createError.UnprocessableEntity();
        }

        try {
            await restaurant.save();
            res.status(STATUSCODE.CREATED).send(newHoraireRestautant);
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_SCHEDULE_UPDATE_FIELD = ["nom"];

/**
 * Met à jour et sauvegarde les informations d'un horaire restaurant.
 * @param req La requête HTTP courante.
 * @param req.body `IHoraireRestaurant` information utilisée pour mettre à jour l'horaire restaurant.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si l'horaire restaurant à mettre à jour ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si l'id de l'horaire donné est invalide.
 */
export const updateHoraireRestaurantById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) { // For void safety
        const horaireRestaurantToUpdate = restaurant.listeHoraire
            .find(horaire => horaire._id == req.params.horaireid.toString());

        if (horaireRestaurantToUpdate) {
            for (const k in req.body) {
                if (ALLOWED_SCHEDULE_UPDATE_FIELD.includes(k)) {
                    (horaireRestaurantToUpdate as any)[k] = req.body[k];
                } else {
                    throw new createError.UnprocessableEntity();
                }
            }

            try {
                await horaireRestaurantToUpdate.validate();
            } catch (e) {
                console.log(e);
                throw new createError.UnprocessableEntity();
            }

            try {
                await restaurant.save();
                res.status(STATUSCODE.OK).send(horaireRestaurantToUpdate);
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface l'horaire restaurant de la liste d'horaires du restaurant.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si l'id horaire restaurant est invalide.
 */
export const deleteHoraireRestaurantById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        const index = restaurant.listeHoraire
            .findIndex(horaire => horaire._id == req.params.horaireid.toString());

        if (index != -1) {
            restaurant.listeHoraire.splice(index, 1);

            try {
                await restaurant.save();
                res.end();
            } catch (e) {
                console.log(e);
                throw new createError.InternalServerError();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
