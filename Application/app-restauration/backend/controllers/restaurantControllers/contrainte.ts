import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { Contrainte } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";

/**
 * Renvoie tous les contraintes d'un article.
 * Renvoie un `Contrainte[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si la liste de contraintes de l'article est vide.
 */
export const getAllContraintes = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id === req.params.categorieid);

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id === req.params.article.toString());

                if (article) {
                    const contraintes = article.listeContrainte;

                    if (contraintes) {
                        res.send(contraintes);
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Renvoie l'contrainte recherché avec l'id donné dans la request.
 * Renvoie un `Contrainte`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la contrainte est invalide.
 */
export const getOneContrainte = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id === req.params.categorieid);

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id === req.params.article.toString());

                if (article) {
                    const contrainte = article.listeContrainte
                        .find(contrainte => contrainte.id === req.params.contrainteid);

                    if (contrainte) {
                        res.send(contrainte);
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Créer un contrainte et le sauvegarde dans la liste de contraintes de l'article.
 * @param req La requête HTTP courante.
 * @param req.body `IContrainte` information utilisée pour créer une contrainte.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'contrainte ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 */
export const createContrainte = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const newContrainte = new Contrainte({
                        nom: req.body.nom,
                        cheminImage: req.body.cheminImage
                    })

                    try {
                        await newContrainte.validate();
                    } catch (e) {
                        console.log(e);
                        throw new createError.UnprocessableEntity();
                    }

                    try {
                        article.listeContrainte.push(newContrainte);
                        await restaurant.save();
                        res.status(STATUSCODE.CREATED).send(newContrainte);
                    } catch (e) {
                        console.log(e);
                        throw new createError.InternalServerError();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_CONTRAINTE_UPDATE_FIELD = ["nom", "cheminImage"];
/**
 * Met à jour et sauvegarde les informations de la contrainte dans la liste de contraintes de l'article donné.
 * @param req La requête HTTP courante.
 * @param req.body `IContrainte` information utilisée pour mettre à jour la contrainte.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si la contrainte ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la contrainte est invalide.
 */
export const updateContrainteById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const contrainteToUpdate = article.listeContrainte
                        .find(contrainteToUpdate => contrainteToUpdate.id == req.params.contrainteid.toString());

                    if (contrainteToUpdate) {
                        for (const k in req.body) {
                            if (ALLOWED_CONTRAINTE_UPDATE_FIELD.includes(k)) {
                                (contrainteToUpdate as any)[k] = req.body[k];
                            } else {
                                throw new createError.UnprocessableEntity();
                            }
                        }

                        try {
                            await contrainteToUpdate.validate();
                        } catch (e) {
                            console.log(e);
                            throw new createError.UnprocessableEntity();
                        }

                        try {
                            await restaurant.save();
                            res.status(STATUSCODE.OK).send(contrainteToUpdate);
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface l'contrainte de la liste de contraintes de l'article donné.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de la contrainte est invalide.
 */

export const deleteContrainteById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const index = article.listeContrainte
                        .findIndex(contrainte => contrainte.id == req.params.contrainteid.toString());
                    if (index != -1) {
                        article.listeContrainte.splice(index, 1);

                        try {
                            await restaurant.save();
                            res.end();
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
