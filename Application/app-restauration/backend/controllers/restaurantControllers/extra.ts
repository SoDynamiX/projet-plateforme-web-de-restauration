import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { Extra } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";

/**
 * Renvoie tous les extras d'un article.
 * Renvoie un `Extra[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si la liste d'extras de l'article est vide.
 */
export const getAllExtras = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid);

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid);

                if (article) {
                    const extras = article.listeExtra;

                    if (extras) {
                        res.send(extras);
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Renvoie l'extra recherché avec l'id donné dans la request.
 * Renvoie un `Extra`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de l'extra est invalide.
 */
export const getOneExtra = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid);

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid);

                if (article) {
                    const extra = article.listeExtra
                        .find(extra => extra.id == req.params.extraid);

                    if (extra) {
                        res.send(extra);
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Créer un extra et le sauvegarde dans la liste d'extras de l'article.
 * @param req La requête HTTP courante.
 * @param req.body `IExtra` information utilisée pour créer un extra.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'extra ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 */
export const createExtra = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const newExtra = new Extra({
                        nom: req.body.nom,
                        prix: req.body.prix,
                        cheminImage: req.body.cheminImage
                    })

                    try {
                        await newExtra.validate();
                    } catch (e) {
                        console.log(e);
                        throw new createError.UnprocessableEntity();
                    }

                    try {
                        article.listeExtra.push(newExtra);
                        await restaurant.save();
                        res.status(STATUSCODE.CREATED).send(newExtra);
                    } catch (e) {
                        console.log(e);
                        throw new createError.InternalServerError();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_EXTRA_UPDATE_FIELD = ["nom", "prix", "cheminImage"];
/**
 * Met à jour et sauvegarde les informations de l'extra dans la liste d'extras de l'article donné.
 * @param req La requête HTTP courante.
 * @param req.body `IExtra` information utilisée pour mettre à jour l'extra.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si l'extra ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de l'extra est invalide.
 */
export const updateExtraById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const extraToUpdate = article.listeExtra
                        .find(extraToUpdate => extraToUpdate.id == req.params.extraid.toString());

                    if (extraToUpdate) {
                        for (const k in req.body) {
                            if (ALLOWED_EXTRA_UPDATE_FIELD.includes(k)) {
                                (extraToUpdate as any)[k] = req.body[k];
                            } else {
                                throw new createError.UnprocessableEntity();
                            }
                        }

                        try {
                            await extraToUpdate.validate();
                        } catch (e) {
                            console.log(e);
                            throw new createError.UnprocessableEntity();
                        }

                        try {
                            await restaurant.save();
                            res.status(STATUSCODE.OK).send(extraToUpdate);
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface l'extra de la liste d'extras de l'article donné.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'article est invalide.
 * @throws {NotFound} si l'id de l'extra est invalide.
 */

export const deleteExtraById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const article = categorie.listeArticle
                    .find(article => article.id == req.params.articleid.toString());

                if (article) {
                    const index = article.listeExtra
                        .findIndex(extra => extra.id == req.params.extraid.toString());
                    if (index != -1) {
                        article.listeExtra.splice(index, 1);

                        try {
                            await restaurant.save();
                            res.end();
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } else {
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
