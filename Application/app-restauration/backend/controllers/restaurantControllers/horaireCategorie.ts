import { IRestaurant } from "../../models/interfaces";
import * as createError from 'http-errors';
import { Request, Response, NextFunction } from 'express';
import Restaurant, { HoraireCategorie } from "../../models/restaurant.model";
import STATUSCODE from "../../models/helpers/STATUS_CODES";


/**
 * Renvoie tous les horaires d'une catégorie.
 * Renvoie un `CategorieHoraire[]`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si la liste de d'horaires d'une catégorie est vide.
 */
export const getAllHoraireCategories = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id === req.params.categorieid);

            if (categorie) {
                const horairesCategorie = categorie.listeHoraireCategorie;
                if (horairesCategorie) {
                    res.send(horairesCategorie);
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Renvoie l'horaire recherché d'une catégorie avec l'id donné dans la request.
 * Renvoie un `HoraireCategorie`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'horaire de la catégorie est invalide.
 * 
 */
export const getOneHoraireCategorie = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id === req.params.categorieid);
            if (categorie) {
                const horaireCategorie = categorie.listeHoraireCategorie
                    .find(HoraireCategorie => HoraireCategorie.id === req.params.horaireid);
                if (horaireCategorie) {
                    res.send(horaireCategorie);
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Créer un horaire pour une catégorie et le sauvegarde dans la liste d'horaires de la catégorie.
 * @param req La requête HTTP courante.
 * @param req.body `IHoraireCategorie` information utilisée pour créer l'horaire de la catégorie.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'horaire ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 */
export const createHoraireCategorie = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());

            if (categorie) {
                const newHoraireCategorie = new HoraireCategorie({
                    nom: req.body.nom,
                    listeJournee: req.body.listeJournee
                })

                try {
                    await newHoraireCategorie.validate();
                } catch (e) {
                    console.log(e);
                    throw new createError.UnprocessableEntity();
                }

                try {
                    categorie.listeHoraireCategorie.push(newHoraireCategorie);
                    await restaurant.save();
                    res.status(STATUSCODE.CREATED).send(newHoraireCategorie );
                } catch (e) {
                    console.log(e);
                    throw new createError.InternalServerError();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}
/**
 * Efface l'horaire de la catégorie donnée.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id du restaurant n'est pas valide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'horaire de la catégorie est invalide.
 */
export const deleteHoraireCategorieById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());
            if (categorie) {
                const index = categorie.listeHoraireCategorie
                    .findIndex(horaireCategorie => horaireCategorie.id == req.params.horaireid.toString());

                if (index != -1) {
                    try {
                        categorie.listeHoraireCategorie.splice(index, 1);

                        try {
                            await restaurant.save();
                            res.end();
                        } catch (e) {
                            console.log(e);
                            throw new createError.InternalServerError();
                        }
                    } catch (e) {
                        console.log(e);
                        throw new createError.NotFound();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_HORAIRECATEGORIE_UPDATE_FIELD = ["nom"];

/**
 * Met à jour et sauvegarde les informations de l'horaire de la catégorie.
 * @param req La requête HTTP courante.
 * @param req.body `IHoraireCategorie` information utilisée pour mettre à jour l'horaire de la catégorie.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {UnprocessableEntity} si l'horaire ne passe pas la validation.
 * @throws {NotFound} si l'id du restaurant donné est invalide.
 * @throws {NotFound} si le restaurant n'a pas de menu.
 * @throws {NotFound} si l'id de la catégorie est invalide.
 * @throws {NotFound} si l'id de l'horaire de la catégorie est invalide.
 */
export const updateHoraireCategorieById = async (req: Request, res: Response) => {
    let restaurant: IRestaurant | null;
    try {
        restaurant = await Restaurant.findById(req.params.restaurantid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (restaurant) {
        if (restaurant.menu) {
            const categorie = restaurant.menu.listeCategorie
                .find(categorie => categorie.id == req.params.categorieid.toString());
            if (categorie) {
                const horaireCategorieToUpdate = categorie.listeHoraireCategorie
                    .find(horaireCategorie => horaireCategorie.id == req.params.horaireid.toString());
                if (horaireCategorieToUpdate) {
                    for (const k in req.body) {
                        if (ALLOWED_HORAIRECATEGORIE_UPDATE_FIELD.includes(k)) {
                            (horaireCategorieToUpdate as any)[k] = req.body[k];
                        } else {
                            throw new createError.UnprocessableEntity();
                        }
                    }

                    try {
                        await restaurant.menu.validate();
                    } catch (e) {
                        console.log(e);
                        throw new createError.UnprocessableEntity();
                    }

                    try {
                        await restaurant.save();
                        res.status(STATUSCODE.OK).send(horaireCategorieToUpdate);
                    } catch (e) {
                        console.log(e);
                        throw new createError.InternalServerError();
                    }
                } else {
                    throw new createError.NotFound();
                }
            } else {
                throw new createError.NotFound();
            }
        } else {
            throw new createError.NotFound();
        }
    } else {
        throw new createError.NotFound();
    }
}