import * as HoraireController from "./horaireRestaurant";
import * as ContactRestaurantController from "./contactRestaurant";
import * as JourneeRestaurantController from "./journeeRestaurant";
import * as MenuController from "./menu";
import * as CategorieController from "./categorie";
import * as HoraireCategorieController from "./horaireCategorie";
import * as JourneeCategorieController from "./journeeCategorie";
import * as ArticleController from "./article";
import * as ExtraController from "./extra";
import * as CategorieVarianteController from "./categorieVariante";
import * as VarianteController from "./variante";
import * as ContrainteController from "./contrainte";

export {
    HoraireController,
    ContactRestaurantController,
    JourneeRestaurantController,
    MenuController,
    CategorieController,
    HoraireCategorieController,
    JourneeCategorieController,
    ArticleController,
    ExtraController,
    CategorieVarianteController,
    VarianteController,
    ContrainteController
};