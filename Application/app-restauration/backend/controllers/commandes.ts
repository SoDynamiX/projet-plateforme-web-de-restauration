import { Request, Response, NextFunction } from 'express';
import * as createError from 'http-errors';
import Commande, { Panier } from "../models/commande.model";
import Client from "../models/client.model";
import Adresse from "../models/adresse.model";
import STATUSCODE from "../models/helpers/STATUS_CODES";
import { ICommande } from '../models/interfaces';

/**
 * Renvoie toutes les commandes de la base de donnée.
 * Renvoie un `Commande[]`
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 */
export const findAllCommandes = async (req: Request, res: Response) => {
    let commandes: ICommande[] | null;
    try {
        commandes = await Commande.find();
    } catch (e) {
        console.log(e);
        throw new createError.InternalServerError();
    }

    res.send(commandes);
}

/**
 * Renvoie la commande recherchée avec l'id donné dans la request.
 * Renvoie une `Commande`.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id donné est invalide.
 */
export const findOneCommande = async (req: Request, res: Response) => {
    let commande: ICommande | null;
    try {
        commande = await Commande.findById(req.params.commandeid.toString());
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (commande) { // For void safety.
        res.send(commande);
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Créer une commande et la sauvegarde à la base de donnée.
 * @param req La requête HTTP courante.
 * @param req.body `ICommande` information pour créer la commande.
 * @param res La réponse envoyée au client.
 * 
 * @throws {NotFound} si l'id du client n'existe pas.
 * @throws {NotFound} si l'id de l'adresse n'existe pas.
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 */
export const createCommande = async (req:Request, res: Response) => {
    // if (!(await Client.exists({ _id : req.body.clientidid}))) { // req.body.clientid is potentially null, we have to verify it.
    //     throw new createError.NotFound();
    // }

    // if (!(await Commande.exists({ _id: req.body.adresseid}))) { // req.body.adresseid is potentially null, we have to verify it.
    //     throw new createError.NotFound();
    // }

    const commande = new Commande({
        dateCreation: Date.now(),
        panier: req.body.panier,
        client: req.body.clientid, 
        adresse: req.body.adresseid
    });

    try {
        await commande.validate();
    } catch (e) {
        console.log(e);
        throw new createError.UnprocessableEntity();
    }

    try {
        await commande.save();
        res.status(STATUSCODE.CREATED).send(commande);
    } catch (e) {
        console.log(e);
        throw new createError.InternalServerError();
    }
}

// Champs acceptés par la méthode update. Sera utilisé pour vérifier une requête et la valider.
const ALLOWED_COMMANDE_UPDATE_FIELD = ["nom"];


/**
 * Met à jour et sauvegarde les informations de la commande recherchée par l'id.
 * @param req La requête HTTP courante.
 * @param req.body `ICommande` l'information utilisée pour mettre à jour la commande.
 * @param res La réponse envoyée au client.
 * 
 * @throws {UnprocessableEntity} si l'information donnée est invalide.
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id de la commande donnée est invalide.
**/

export const updateCommandeById = async (req: Request, res: Response) => {
    let commandeToUpdate: ICommande | null;
    try {
        commandeToUpdate = await Commande.findById(req.params.commandeid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (commandeToUpdate) {
        for (const k in req.body) {
            if (ALLOWED_COMMANDE_UPDATE_FIELD.includes(k)) {
                (commandeToUpdate as any)[k] = req.body[k];
            } else {
                throw new createError.UnprocessableEntity();
            }
        }

        try {
            await commandeToUpdate.validate();

        } catch {
            throw new createError.UnprocessableEntity();
        }
        
        try {
            await commandeToUpdate.save();
            res.status(STATUSCODE.OK).send(commandeToUpdate);
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError();
        }
    } else {
        throw new createError.NotFound();
    }
}

/**
 * Efface la commande de la base de donnée.
 * @param req La requête HTTP courante.
 * @param res La réponse envoyée au client.
 * 
 * @throws {InternalServerError} si la base de donnée n'est pas accessible au moment de la sauvegarde ou de la recherche du restaurant.
 * @throws {NotFound} si l'id de la commande donnée est invalide.
 */
export const deleteCommandeById = async (req: Request, res: Response) => {
    let commandeToDelete: ICommande | null;
    try {
        commandeToDelete = await Commande.findById(req.params.commandeid);
    } catch (e) {
        throw new createError.InternalServerError();
    }

    if (commandeToDelete) {
        try {
            await Commande.deleteOne({ _id: req.params.commandeid });
            res.end();
        } catch (e) {
            console.log(e);
            throw new createError.InternalServerError
        }
    } else {
        throw new createError.NotFound();
    }
}