import { Request, Response, NextFunction } from 'express';
import { HttpError } from 'http-errors';

export const defaultErrorHandler = (
  err: Error, req: Request,

  res: Response, next: NextFunction,
): void => {
  let status = 500;
  console.log(err);
  if (err instanceof HttpError) {
    const httpErr = err as HttpError;
    status = httpErr.status || 500;
  }
  res.status(status);
  res.send({
    status,
    message: err.message,
  }).end();

  next();
};