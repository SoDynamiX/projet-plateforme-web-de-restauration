import { internet, name, phone } from "faker";
import { IClient } from "../models/interfaces";
import Client from "../models/client.model";

const createClient = async () => {
    let prenom = name.firstName();
    let nom = name.lastName();
    let adresse = null;
    let courriel = internet.email(prenom, nom);
    let nomUtilisateur = internet.userName(prenom, nom);
    let numeroTelephone = "8198198119";
    let statut = true;

    let client = new Client({
        prenom: prenom,
        nom: nom,
        adresse: adresse,
        courriel: courriel,
        nomUtilisateur: nomUtilisateur,
        numeroTelephone: numeroTelephone,
        statut: statut,
    })

    return client;
}

export const getNewlyCreatedClients = async (numberOfIterations: number): Promise<IClient[]> => {
    let clientList: Array<IClient> = [];
    
    for (let i = 0; i < numberOfIterations; i++) {
        clientList.push(await createClient());
    }

    return clientList;
}
