import { internet, name, phone, address } from "faker";
import { IRestaurant } from "../models/interfaces";
import Restaurant, { Menu } from "../models/restaurant.model";

const createBaseRestaurant = async () => {
    let restaurant = new Restaurant({
        nom: name.firstName(),
        numeroTelephone: "8198198119",
        cheminLogo: "C:/wow",
        courriel: "restauranttest@hotmail.com"
    });

    return restaurant;
}

const createRestaurantWithMenu = async () => {
    let restaurant = new Restaurant({
        nom: name.firstName(),
        numeroTelephone: "8198198119",
        cheminLogo: "C:/wow",
        courriel: "restauranttest@hotmail.com"
    });
    restaurant.menu = new Menu({
        nom: name.firstName(),
        disponibilite: true
    });

    return restaurant;
}

export const getNewlyCreatedRestaurants = async (numberOfIterations: number): Promise<IRestaurant[]> => {
    let restaurantList: Array<IRestaurant> = [];
    
    for (let i = 0; i < numberOfIterations; i++) {
        restaurantList.push(await createBaseRestaurant());
    }

    return restaurantList;
}

export default getNewlyCreatedRestaurants;
