import { internet, name, phone } from "faker";
import { ICommande } from "../models/interfaces";
import Commande from "../models/commande.model";

const createCommande = async () => {


    const commande = new Commande({
        panier: null,
    })

    return commande;
}

export const getNewlyCreatedCommande = async (numberOfIterations: number): Promise<ICommande[]> => {
    console.log("Seeding database");
    let commandeList: Array<ICommande> = [];
    
    for (let i = 0; i < numberOfIterations; i++) {
        commandeList.push(await createCommande());
    }

    return commandeList;
}

export default getNewlyCreatedCommande;
