import ClientRouter from "./clients";
import CommandesRouter from "./commandes";
import RestaurantRouter from "./restaurants";

export {
    ClientRouter,
    CommandesRouter,
    RestaurantRouter
}