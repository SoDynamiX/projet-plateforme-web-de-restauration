import { Router } from "express";
import { ClientController } from "../controllers";
import asyncHandler from "express-async-handler";

const router = Router();

const getAllClients = asyncHandler(ClientController.getAllClients);
const createClient = asyncHandler(ClientController.createClient);

const getOneClient = asyncHandler(ClientController.getOneClient);
const deleteClient = asyncHandler(ClientController.deleteClientById);
const updateClient = asyncHandler(ClientController.updateClientById);

router
    // Find all
    .get("/", getAllClients)
    // Create
    .post("/", createClient);

router
    // Find one
    .get("/:clientid", getOneClient)
    // Delete
    .delete("/:clientid", deleteClient)
    // Update
    .patch("/:clientid", updateClient);

export default router;