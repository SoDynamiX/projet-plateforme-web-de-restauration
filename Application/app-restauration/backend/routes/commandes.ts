import { Router } from "express";
import { CommandeController } from "../controllers";
import asyncHandler from "express-async-handler";

const router = Router();

///////////////////////////////////////////////////////////////////////////////
//                              COMMANDES                                    //
///////////////////////////////////////////////////////////////////////////////

const getAllCommandes = asyncHandler(CommandeController.findAllCommandes);
const createCommande = asyncHandler(CommandeController.createCommande);

const getOneCommande = asyncHandler(CommandeController.findOneCommande);
const deleteCommande = asyncHandler(CommandeController.deleteCommandeById);
const updateCommande = asyncHandler(CommandeController.updateCommandeById);

router
    // Find all
    .get("/", getAllCommandes)
    // Create
    .post("/", createCommande);

router
    // Find one 
    .get("/:commandeid", getOneCommande)
    // Delete
    .delete("/:commandeid", deleteCommande)
    // Update
    .patch("/:commandeid", updateCommande);
    

export default router;