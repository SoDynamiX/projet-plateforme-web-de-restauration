import { Router } from "express";
import { RestaurantController } from "../controllers";
import { HoraireController, ContactRestaurantController, JourneeCategorieController,
    MenuController, CategorieController, HoraireCategorieController, 
    JourneeRestaurantController, ArticleController, ExtraController, 
    CategorieVarianteController, VarianteController, ContrainteController } from "../controllers/restaurantControllers"
import asyncHandler from "express-async-handler";

const router = Router();

///////////////////////////////////////////////////////////////////////////////
//                             RESTAURANT                                    //
///////////////////////////////////////////////////////////////////////////////

const findAllRestaurants = asyncHandler(RestaurantController.findAllRestaurants);
const createRestaurant = asyncHandler(RestaurantController.createRestaurant);

const findOneRestaurant = asyncHandler(RestaurantController.findOneRestaurant);
const deleteRestaurant = asyncHandler(RestaurantController.deleteRestaurantById);
const updateRestaurant = asyncHandler(RestaurantController.updateRestaurantById);

router
    // Find all 
    .get("/", findAllRestaurants)
    // Create
    .post("/", createRestaurant);
    
router
    // Find one 
    .get("/:restaurantid", findOneRestaurant)
    // Delete
    .delete("/:restaurantid", deleteRestaurant)
    // Update
    .put("/:restaurantid", updateRestaurant);


///////////////////////////////////////////////////////////////////////////////
//                         HORAIRE RESTAURANT                                //
///////////////////////////////////////////////////////////////////////////////

const getAllHoraireRestaurants = asyncHandler(HoraireController.getAllHoraireRestaurants);
const createHoraireRestaurant = asyncHandler(HoraireController.createHoraireRestaurant);

const getOneHoraireRestaurant = asyncHandler(HoraireController.getOneHoraireRestaurant);
const deleteHoraireRestaurant = asyncHandler(HoraireController.deleteHoraireRestaurantById);
const updateHoraireRestaurant = asyncHandler(HoraireController.updateHoraireRestaurantById);

router
    // Find all 
    .get("/:restaurantid/horaires", getAllHoraireRestaurants)
    // Create
    .post("/:restaurantid/horaires", createHoraireRestaurant);
    
router
    // Find one 
    .get("/:restaurantid/horaires/:horaireid", getOneHoraireRestaurant)
    // Delete
    .delete("/:restaurantid/horaires/:horaireid", deleteHoraireRestaurant)
    // Update
    .put("/:restaurantid/horaires/:horaireid", updateHoraireRestaurant);


///////////////////////////////////////////////////////////////////////////////
//                         JOURNEE RESTAURANT                                //
///////////////////////////////////////////////////////////////////////////////

const getAllJourneeRestaurants = asyncHandler(JourneeRestaurantController.getAllJourneeRestaurants);
const createJourneeRestaurant = asyncHandler(JourneeRestaurantController.createJourneeRestaurant);

const getOneJourneeRestaurant = asyncHandler(JourneeRestaurantController.getOneJourneeRestaurant);
const deleteJourneeRestaurant = asyncHandler(JourneeRestaurantController.deleteJourneeRestaurantById);
const updateJourneeRestaurant = asyncHandler(JourneeRestaurantController.updateJourneeRestaurantById);

router
    // Find all 
    .get("/:restaurantid/horaires/:horaireid/journees", getAllJourneeRestaurants)
    // Create
    .post("/:restaurantid/horaires/:horaireid/journees", createJourneeRestaurant);
    
router
    // Find one 
    .get("/:restaurantid/horaires/:horaireid/journees/:journeeid", getOneJourneeRestaurant)
    // Delete
    .delete("/:restaurantid/horaires/:horaireid/journees/:journeeid", deleteJourneeRestaurant)
    // Update
    .put("/:restaurantid/horaires/:horaireid/journees/:journeeid", updateJourneeRestaurant);


///////////////////////////////////////////////////////////////////////////////
//                                MENU                                       //
///////////////////////////////////////////////////////////////////////////////

const createMenu = asyncHandler(MenuController.createMenu);

const getMenu = asyncHandler(MenuController.getMenu);
const deleteMenu = asyncHandler(MenuController.deleteMenuById);
const updateMenu = asyncHandler(MenuController.updateMenuById);

router
    // Create
    .post("/:restaurantid/menu", createMenu);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid", getMenu)
    // Delete
    .delete("/:restaurantid/menu/:menuid", deleteMenu)
    // Update
    .put("/:restaurantid/menu/:menuid", updateMenu);


///////////////////////////////////////////////////////////////////////////////
//                          CONTACT RESTAURANT                               //
///////////////////////////////////////////////////////////////////////////////

const getAllContactRestaurants = asyncHandler(ContactRestaurantController.getAllContactRestaurants);
const createContactRestaurant = asyncHandler(ContactRestaurantController.createContactRestaurant);

const getOneContactRestaurant = asyncHandler(ContactRestaurantController.getOneContactRestaurant);
const deleteContactRestaurant = asyncHandler(ContactRestaurantController.deleteContactRestaurantById);
const updateContactRestaurant = asyncHandler(ContactRestaurantController.updateContactRestaurantById);

router
    // Find all 
    .get("/:restaurantid/contacts", getAllContactRestaurants)
    // Create
    .post("/:restaurantid/contacts", createContactRestaurant);
    
router
    // Find one 
    .get("/:restaurantid/contacts/:contactid", getOneContactRestaurant)
    // Delete
    .delete("/:restaurantid/contacts/:contactid", deleteContactRestaurant)
    // Update
    .put("/:restaurantid/contacts/:contactid", updateContactRestaurant);


///////////////////////////////////////////////////////////////////////////////
//                              CATEGORIE                                    //
///////////////////////////////////////////////////////////////////////////////

const getAllCategories = asyncHandler(CategorieController.getAllCategories);
const createCategorie = asyncHandler(CategorieController.createCategorie);

const getOneCategorie = asyncHandler(CategorieController.getOneCategorie);
const deleteCategorie = asyncHandler(CategorieController.deleteCategorieById);
const updateCategorie = asyncHandler(CategorieController.updateCategorieById);

router
    // Find all 
    .get("/:restaurantid/menu/:menuid/categories", getAllCategories)
    // Create
    .post("/:restaurantid/menu/:menuid/categories", createCategorie);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid", getOneCategorie)
    // Delete
    .delete("/:restaurantid/menu/:menuid/categories/:categorieid", deleteCategorie)
    // Update
    .put("/:restaurantid/menu/:menuid/categories/:categorieid", updateCategorie);


///////////////////////////////////////////////////////////////////////////////
//                          HORAIRE CATEGORIE                                //
///////////////////////////////////////////////////////////////////////////////

const getAllHoraireCategories = asyncHandler(HoraireCategorieController.getAllHoraireCategories);
const createHoraireCategorie = asyncHandler(HoraireCategorieController.createHoraireCategorie);

const getOneHoraireCategorie = asyncHandler(HoraireCategorieController.getOneHoraireCategorie);
const deleteHoraireCategorie = asyncHandler(HoraireCategorieController.deleteHoraireCategorieById);
const updateHoraireCategorie = asyncHandler(HoraireCategorieController.updateHoraireCategorieById);

router
    // Find all 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/horaires", getAllHoraireCategories)
    // Create
    .post("/:restaurantid/menu/:menuid/categories/:categorieid/horaires", createHoraireCategorie);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/horaires/:horaireid", getOneHoraireCategorie)
    // Delete
    .delete("/:restaurantid/menu/:menuid/categories/:categorieid/horaires/:horaireid", deleteHoraireCategorie)
    // Update
    .put("/:restaurantid/menu/:menuid/categories/:categorieid/horaires/:horaireid", updateHoraireCategorie);


///////////////////////////////////////////////////////////////////////////////
//                          JOURNEE CATEGORIE                                //
///////////////////////////////////////////////////////////////////////////////

const getAllJourneeCategories = asyncHandler(JourneeCategorieController.getAllJourneeCategories);
const createJourneeCategorie = asyncHandler(JourneeCategorieController.createJourneeCategorie);

const getOneJourneeCategorie = asyncHandler(JourneeCategorieController.getOneJourneeCategorie);
const deleteJourneeCategorie = asyncHandler(JourneeCategorieController.deleteJourneeCategorieById);
const updateJourneeCategorie = asyncHandler(JourneeCategorieController.updateJourneeCategorieById);

router
    // Find all 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/horairecategories/:horaireid/journees", getAllJourneeCategories)
    // Create
    .post("/:restaurantid/menu/:menuid/categories/:categorieid/horairecategories/:horaireid/journees", createJourneeCategorie);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/horairecategories/:horaireid/journees/:journeeid", getOneJourneeCategorie)
    // Delete
    .delete("/:restaurantid/menu/:menuid/categories/:categorieid/horairecategories/:horaireid/journees/:journeeid", deleteJourneeCategorie)
    // Update
    .put("/:restaurantid/menu/:menuid/categories/:categorieid/horairecategories/:horaireid/journees/:journeeid", updateJourneeCategorie);


///////////////////////////////////////////////////////////////////////////////
//                               ARTICLE                                     //
///////////////////////////////////////////////////////////////////////////////

const getAllArticles = asyncHandler(ArticleController.getAllArticles);
const createArticle = asyncHandler(ArticleController.createArticle);

const getOneArticle = asyncHandler(ArticleController.getOneArticle);
const deleteArticle = asyncHandler(ArticleController.deleteArticleById);
const updateArticle = asyncHandler(ArticleController.updateArticleById);

router
    // Find all 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles", getAllArticles)
    // Create
    .post("/:restaurantid/menu/:menuid/categories/:categorieid/articles", createArticle);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid", getOneArticle)
    // Delete
    .delete("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid", deleteArticle)
    // Update
    .put("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid", updateArticle);


///////////////////////////////////////////////////////////////////////////////
//                                EXTRA                                      //
///////////////////////////////////////////////////////////////////////////////

const getAllExtras = asyncHandler(ExtraController.getAllExtras);
const createExtra = asyncHandler(ExtraController.createExtra);

const getOneExtra = asyncHandler(ExtraController.getOneExtra);
const deleteExtra = asyncHandler(ExtraController.deleteExtraById);
const updateExtra = asyncHandler(ExtraController.updateExtraById);


router
    // Find all 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/extras", getAllExtras)
    // Create
    .post("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/extras", createExtra);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/extras/:extraid", getOneExtra)
    // Delete
    .delete("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/extras/:extraid", deleteExtra)
    // Update
    .put("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/extras/:extraid", updateExtra);


///////////////////////////////////////////////////////////////////////////////
//                              CONTRAINTE                                   //
///////////////////////////////////////////////////////////////////////////////

const getAllContraintes = asyncHandler(ContrainteController.getAllContraintes);
const createContrainte = asyncHandler(ContrainteController.createContrainte);

const getOneContrainte = asyncHandler(ContrainteController.getOneContrainte);
const deleteContrainte = asyncHandler(ContrainteController.deleteContrainteById);
const updateContrainte = asyncHandler(ContrainteController.updateContrainteById);


router
    // Find all 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/contraintes", getAllContraintes)
    // Create
    .post("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/contraintes", createContrainte);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/contraintes/:contrainteid", getOneContrainte)
    // Delete
    .delete("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/contraintes/:contrainteid", deleteContrainte)
    // Update
    .put("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/contraintes/:contrainteid", updateContrainte);

///////////////////////////////////////////////////////////////////////////////
//                          CATEGORIE VARIANTE                               //
///////////////////////////////////////////////////////////////////////////////

const getAllCategorieVariantes = asyncHandler(CategorieVarianteController.getAllCategorieVariantes);
const createCategorieVariante = asyncHandler(CategorieVarianteController.createCategorieVariante);

const getOneCategorieVariante = asyncHandler(CategorieVarianteController.getOneCategorieVariante);
const deleteCategorieVariante = asyncHandler(CategorieVarianteController.deleteCategorieVarianteById);
const updateCategorieVariante = asyncHandler(CategorieVarianteController.updateCategorieVarianteById);

router
    // Find all 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes", getAllCategorieVariantes)
    // Create
    .post("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes", createCategorieVariante);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes/:categorievarianteid", getOneCategorieVariante)
    // Delete
    .delete("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes/:categorievarianteid", deleteCategorieVariante)
    // Update
    .put("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes/:categorievarianteid", updateCategorieVariante);


///////////////////////////////////////////////////////////////////////////////
//                               VARIANTE                                    //
///////////////////////////////////////////////////////////////////////////////

const getAllVariantes = asyncHandler(VarianteController.getAllVariantes);
const createVariante = asyncHandler(VarianteController.createVariante);

const getOneVariante = asyncHandler(VarianteController.getOneVariante);
const deleteVariante = asyncHandler(VarianteController.deleteVarianteById);
const updateVariante = asyncHandler(VarianteController.updateVarianteById);

router
    // Find all 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes/:categorievarianteid/variantes", getAllVariantes)
    // Create
    .post("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes/:categorievarianteid/variantes", createVariante);
    
router
    // Find one 
    .get("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes/:categorievarianteid/variantes/:varianteid", getOneVariante)
    // Delete
    .delete("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes/:categorievarianteid/variantes/:varianteid", deleteVariante)
    // Update
    .put("/:restaurantid/menu/:menuid/categories/:categorieid/articles/:articleid/categorievariantes/:categorievarianteid/variantes/:varianteid", updateVariante);


export default router;