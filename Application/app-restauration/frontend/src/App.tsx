import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { LandingPage } from "./Components/LandingPage/MainLandingPage";
import { BuyMenu } from "./Components/BuyPage/MainBuyMenu";
import { BuyMenuEditor } from "./Components/BuyPageEditor/MainBuyMenuEditor";

// Retourne un "component" d'application
const App: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={LandingPage} />
        <Route
          path="/restaurants/:restaurantid/menu/:menuid"
          exact
          component={BuyMenu}
        />
        <Route
          path="/restaurants/:restaurantid/menu/:menuid/edit"
          exact
          component={BuyMenuEditor}
        />
        {/* Force un 404 temporaire en cas d'URL invalide */}
        <Route path="/" render={() => <div>404</div>} />
      </Switch>
    </Router>
  );
};

export default App;
