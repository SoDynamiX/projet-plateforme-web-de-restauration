// Material-UI
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

// Retourne un "component" de droit d'auteur pour l'application
function CopyrightComponent() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Eat Simply
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

// Retourne un "component" de pied de page pour l'application
export const FooterComponent: React.FC = () => {
  const styles = useStyles();

  return (
    <footer className={styles.footer}>
      <Typography variant="h6" align="center" gutterBottom>
        {/* À compléter pour la présentation */}
        Footer Stuff
      </Typography>
      <Typography variant="subtitle1" align="center" color="textSecondary">
        Eat food
      </Typography>
      <CopyrightComponent />
    </footer>
  );
};
