import React from "react";
import { Link } from "react-router-dom";
import { IRestaurant } from "../interfaces";

// Material-UI
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  grilleCarte: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  carte: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  mediaCarte: {
    paddingTop: "56.25%", // Ratio 16:9
  },
  actionCarte: {
    marginTop: "auto",
  },
}));

// Retourne un "component" de carte restaurant pour la page d'accueil
export const CardComponentRestaurant: React.FC<IRestaurant> = (
  props: IRestaurant
) => {
  const styles = useStyles();
  let adresse;

  // Validation des adresses des restaurants en attendant l'implémentation de l'API Google
  {
    props.adresse == null
      ? (adresse = "Aucune adresse donnée")
      : (adresse = props.adresse);
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <Grid className={styles.grilleCarte} item xs={12} sm={6} md={3}>
        <Card className={styles.carte}>
          <CardMedia
            className={styles.mediaCarte}
            // Image de restaurant temporaire en attendant le déploiement sur le serveur
            image="https://images.unsplash.com/photo-1518550996869-f6674df1051c?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjB8fHJlc3RhdXJhbnQlMjBvdXRzaWRlfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
          />
          <Box>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {props.nom}
              </Typography>
              <Typography gutterBottom variant="subtitle1" component="h6">
                {adresse}
              </Typography>
            </CardContent>
          </Box>
          <CardActions className={styles.actionCarte}>
            <Button size="small" color="primary">
              {/* Route vers la page d'achat du menu d'un restaurant */}
              <Link to={`/restaurants/${props._id}/menu/${props.menu?._id}`}>
                Accéder au menu
              </Link>
            </Button>
          </CardActions>
        </Card>
      </Grid>
    </React.Fragment>
  );
};
