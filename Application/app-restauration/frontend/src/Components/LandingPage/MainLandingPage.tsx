import React, { useState } from "react";
import { IRestaurant } from "../interfaces";
import { CardComponentRestaurant } from "./CardComponentRestaurant";
import { NavbarComponent } from "../Navbar";
import { HeaderComponent } from "./HeaderLandingPage";
import { FooterComponent } from "../Footer";

// Material-UI
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import LinearProgress from "@material-ui/core/LinearProgress";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  margeCarteRestaurants: {
    marginBottom: "30px",
  },
}));

// Retourne un "component" de page d'accueil pour l'application
export const LandingPage: React.FC = () => {
  const styles = useStyles();
  const [listeRestaurants, setListeRestaurants] = useState([] as IRestaurant[]);
  const [enChargement, setEnChargement] = useState<boolean>(true);

  // "Hook" permettant d'aller chercher les restaurants dans la BD
  React.useEffect(() => {
    if (listeRestaurants.length === 0) {
      fetch(`http://localhost:5000/restaurants/`)
        .then((response) => response.json())
        .then((data) => setListeRestaurants(data));
    } else {
      setEnChargement(false);
    }
  }, [listeRestaurants]);

  // Barre de chargement si la liste des restaurants n'est pas chargée
  if (enChargement) return <LinearProgress />;

  return (
    <div>
      <NavbarComponent />
      <HeaderComponent />
      <Grid container spacing={1} className={styles.margeCarteRestaurants}>
        <Grid item xs>
          <Container>
            <Grid container spacing={5}>
              {/* Affiche les restaurants disponibles dans une grille */}
              {listeRestaurants.map((restaurant: IRestaurant) => {
                if (restaurant.menu?.disponibilite)
                  return (
                    <CardComponentRestaurant
                      {...restaurant}
                      key={restaurant._id}
                    />
                  );
              })}
            </Grid>
          </Container>
        </Grid>
      </Grid>
      <FooterComponent />
    </div>
  );
};
