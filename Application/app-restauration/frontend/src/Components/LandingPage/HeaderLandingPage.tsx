import React from "react";

// Material-UI
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  header: {
    backgroundColor: "theme.palette.background.paper",
    padding: theme.spacing(0, 0, 6),
    marginTop: "50px",
  },
}));

// Renvoie le header de l'application
export const HeaderComponent: React.FC = () => {
  const styles = useStyles();
  return (
    <div className={styles.header}>
      <Container maxWidth="sm">
        <Typography
          className="titre"
          component="h1"
          variant="h2"
          align="center"
          color="textPrimary"
          gutterBottom
        >
          Eat Simply.
        </Typography>
        <Typography variant="h5" align="center" color="textSecondary" paragraph>
          Naviguez à travers les menus offerts, choissisez les articles qui vous
          intéressent, et le tour est joué!
        </Typography>
      </Container>
    </div>
  );
};
