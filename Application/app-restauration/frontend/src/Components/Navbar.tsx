import React from "react";
import { Link } from "react-router-dom";
import { AvatarComponent } from "./Avatar";

// Material-UI
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  navbar: {
    backgroundColor: "#f44336 ",
  },
}));

// Retourne un "component" de barre de navigation
export const NavbarComponent: React.FC = () => {
  const styles = useStyles();

  return (
    <div>
      <AppBar position="relative" className={styles.navbar}>
        <Toolbar>
          {/* Route vers la page d'accueil */}
          <Link to={`/`}>
            <Typography variant="h6">Eat Simply</Typography>
          </Link>
          <AvatarComponent />
        </Toolbar>
      </AppBar>
    </div>
  );
};
