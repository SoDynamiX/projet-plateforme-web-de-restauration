import React from "react";

//Material UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { TransitionProps } from "@material-ui/core/transitions";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
  labelFocused: {},
}));

// Animation du "pop-up"
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

// Interface utilisée pour un "dialog" de demande de suppression d'un item
interface IProps {
  open: boolean;
  onClose: () => void;
  deleteRequest: any;
  nom: string;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
}

/**
 * Retourne un "dialog" de confirmation de suppression d'un item.
 * @param open Méthode d'ouverture de la fenêtre de création d'une catégorie.
 * @param onClose Méthode de fermeture de la fenêtre de création d'une catégorie.
 * @param deleteRequest Méthode où se fait la requête HTTP "delete".
 * @param nom Le nom de l'item pour qui la confirmation est demandée.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 */
export const DialogConfirmDelete: React.FC<IProps> = ({
  open,
  onClose,
  deleteRequest,
  nom,
  setHasChanged,
}) => {
  const styles = useStyles();

  return (
    <form>
      <Dialog
        className={styles.dialog}
        open={open}
        onClose={onClose}
        fullWidth={true}
        TransitionComponent={Transition}
      >
        <DialogTitle id="alert-dialog-title">{"Attention"}</DialogTitle>

        <DialogContent>
          <Typography variant="subtitle1">
            Voulez-vous vraiment supprimer l'item "{nom}" ?
          </Typography>
        </DialogContent>

        {/* Bouton annuler */}
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Annuler
          </Button>

          {/* Bouton supprimer */}
          <Button
            onClick={() => {
              deleteRequest();
              setHasChanged(true);
              onClose();
            }}
            color="secondary"
            variant="contained"
            autoFocus
          >
            Supprimer
          </Button>
        </DialogActions>
      </Dialog>
    </form>
  );
};
