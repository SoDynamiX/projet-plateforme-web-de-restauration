import React, { useState } from "react";
import { ImageUploadComponent } from "./ImageUploadComponent";

//Material UI
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

// ***** ENFANT DE 'StepperMenu' ******

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  backButton: {
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
  labelFocused: {},
}));

// Interface utilisée pour la section 1 du dialogue d'ajout et de modification d'un article
interface IProps {
  nomArticle: string;
  prixArticle: number;
  descriptionArticle: string;
  editCard: boolean;
  activeStep: number;
  handleBack: () => void;
  handleNext: (values: any) => void;
  isLastStep: boolean;
}

/**
 * Retourne un "dialog" pour la section 1 du "StepperMenu".
 * @param nomArticle Le nom de l'article.
 * @param prixArticle Le prix de l'article.
 * @param descriptionArticle La description de l'article.
 * @param editCard Pour savoir si on est en "Create" ou en "Update".
 * @param activeStep La valeur de l'index du "StepperMenu"
 * @param handleBack Méthode de gestion lorsqu'on recule dans le "StepperMenu."
 * @param handleNext Méthode de gestion lorsqu'on avance dans le "StepperMenu."
 * @param isLastStep Pour savoir si on est à la fin du "StepperMenu".
 */
export const DialogContentSection1: React.FC<IProps> = ({
  nomArticle,
  prixArticle,
  descriptionArticle,
  editCard,
  activeStep,
  handleBack,
  handleNext,
  isLastStep,
}) => {
  const classes = useStyles();

  //Hooks servant à "set" les nouvelles valeurs qui seront ensuite utilisées pour construire l'article temporaire
  const [newNomArticle, setNomValue] = useState(nomArticle);
  const [newPrixArticle, setPrixValue] = useState(prixArticle);
  const [newDescriptionArticle, setDescriptionValue] = useState(
    descriptionArticle
  );

  return (
    <form>
      {/* Vérifie si le dialogue a été ouvert pour un ajout d'article ou une modification */}
      {(() => {
        if (!editCard) {
          nomArticle = "";
        }
      })()}

      <Typography variant="h6" style={{ paddingBottom: "10px" }}>
        Informations
      </Typography>

      {/* Champ de text pour le nom */}
      <TextField
        id="nom-article"
        label="Nom de l'article"
        defaultValue={nomArticle}
        margin="normal"
        className={classes.textField}
        inputProps={{
          nom: "nom",
        }}
        onChange={(e) => setNomValue(e.target.value)} //On 'set' la valeur entrée dans textfield
        InputLabelProps={{
          shrink: true,
          classes: {
            root: classes.labelRoot,
            focused: classes.labelFocused,
          },
        }}
      />

      {/* Champ de text pour le prix */}
      <TextField
        id="nom-article"
        label="Prix de l'article"
        defaultValue={prixArticle}
        margin="normal"
        InputProps={{
          startAdornment: <InputAdornment position="start">$</InputAdornment>,
        }}
        onChange={(e) => setPrixValue(+e.target.value)}
        InputLabelProps={{
          shrink: true,
          classes: {
            root: classes.labelRoot,
            focused: classes.labelFocused,
          },
        }}
      />

      {/* Champ de text pour la description*/}
      <TextField
        id="description-article"
        label="Description de l'article"
        defaultValue={descriptionArticle}
        fullWidth
        multiline
        margin="normal"
        onChange={(e) => setDescriptionValue(e.target.value)} //On 'set' la valeur entrée dans textfield
        InputLabelProps={{
          shrink: true,
          classes: {
            root: classes.labelRoot,
            focused: classes.labelFocused,
          },
        }}
      />

      <Divider className={classes.section} />

      <Typography variant="h6" style={{ paddingBottom: "10px" }}>
        Image
      </Typography>

      {/*
        *Component de Material UI permettant de téléverser une image en cherchant dans l'explorateur
         de fichier OU en fessant un 'drag and drop'.

        * Affiche également un 'preview' de l'image 
       */}
      <ImageUploadComponent editCard={editCard} />

      {/* Bouton retour pour retourner à la section précedente */}
      <Button
        disabled={activeStep === 0}
        onClick={handleBack}
        className={classes.backButton}
      >
        Retour
      </Button>

      {/* Bouton suivant pour aller vers la section suivante */}
      <Button
        className={classes.backButton}
        variant="contained"
        color="primary"
        //On set les nouvelles valeurs pour la construction de l'objet article temporaire
        onClick={() => {
          handleNext({
            nom: newNomArticle,
            description: newDescriptionArticle,
            prix: newPrixArticle,
          });
        }}
      >
        {isLastStep ? "Finaliser" : "Suivant"}
      </Button>
    </form>
  );
};
