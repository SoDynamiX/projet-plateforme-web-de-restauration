import React, { useState } from "react";
import { ICategorieVariante } from "../../interfaces";

// Material-UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { TransitionProps } from "@material-ui/core/transitions";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
  labelFocused: {},
}));

// Animation du "pop-up"
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

// Interface utilisée par un "dialog" pour un article
interface IProps {
  editCard: boolean;
  handleConfirmCategorieVariante: (categorieVariante: any) => void;
  categorieVariante: ICategorieVariante;
  open: boolean;
  onClose: () => void;
}

/**
 * Retourne un "dialog" pour une catégorie variante.
 * @param editCard Pour savoir si on est en "Create" ou en "Update".
 * @param handleConfirmCategorieVariante Méthode d'ajout d'une catégorie variante à un article.
 * @param categorieVariante La catégorie variante pour qui le "dialog" est fait.
 * @param open Méthode d'ouverture du "DialogArticle".
 * @param onClose Méthode de fermeture du "DialogArticle".
 */
export const DialogCategorieVariante: React.FC<IProps> = ({
  editCard,
  handleConfirmCategorieVariante,
  categorieVariante,
  open,
  onClose,
}) => {
  const styles = useStyles();

  // Variables temporaires
  var nom = "";
  var varianteMin = categorieVariante.varianteMinimum;
  var varianteMax = categorieVariante.varianteMaximum;

  // "useState" pour enregistrer le contenu des "TextBox"
  const [champNom, setNomValue] = useState("");
  const [champVarianteMin, setChampVarianteMin] = useState(0);
  const [champVarianteMax, setChampVarianteMax] = useState(0);

  // La catégorie qui sera envoyée dans la requête HTTP
  const categorieVarianteTemporaire: any = {
    _id: categorieVariante._id,
    nom: champNom,
    varianteMinimum: champVarianteMin,
    varianteMaximum: champVarianteMax,
    listeVariante: categorieVariante.listeVariante,
  };

  return (
    <form>
      {(() => {
        if (editCard) {
          nom = categorieVariante.nom;
        }
      })()}

      <Dialog
        className={styles.dialog}
        open={open}
        onClose={onClose}
        fullWidth={true}
        TransitionComponent={Transition}
      >
        <DialogTitle>Modification d'un item</DialogTitle>
        <DialogContent style={{ overflowX: "hidden" }} dividers>
          {/* Champ de texte pour le nom */}
          <Typography variant="h6" style={{ paddingBottom: "10px" }}>
            Informations
          </Typography>
          <TextField
            id="nom-categorie"
            label="Nom"
            fullWidth
            defaultValue={nom}
            margin="normal"
            className={styles.textField}
            onChange={(e) => setNomValue(e.target.value)}
            InputLabelProps={{
              shrink: true,
              classes: {
                root: styles.labelRoot,
                focused: styles.labelFocused,
              },
            }}
          />
          {/* Champ de texte pour la variante minimum */}
          <TextField
            id="min-categorie"
            label="Variante minimum"
            defaultValue={varianteMin}
            margin="normal"
            className={styles.textField}
            onChange={(e) => setChampVarianteMin(+e.target.value)}
            InputLabelProps={{
              shrink: true,
              classes: {
                root: styles.labelRoot,
                focused: styles.labelFocused,
              },
            }}
          />
          {/* Champ de texte pour la variante maximum */}
          <TextField
            id="max-categorie"
            label="Variante maximum"
            defaultValue={varianteMax}
            margin="normal"
            className={styles.textField}
            onChange={(e) => setChampVarianteMax(+e.target.value)}
            InputLabelProps={{
              shrink: true,
              classes: {
                root: styles.labelRoot,
                focused: styles.labelFocused,
              },
            }}
          />
        </DialogContent>
        {/* Boutons */}
        <DialogActions>
          <Button onClick={onClose}>Annuler</Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleConfirmCategorieVariante(categorieVarianteTemporaire);
              onClose();
            }}
          >
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    </form>
  );
};
