import React from "react";
import { DialogSmallCard } from "./DialogSmallCard";
import { IExtra, IContrainte, IVariante } from "../../interfaces";

// Material-UI
import Card from "@material-ui/core/Card";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  card: {
    width: "150px",
    display: "flex",
    flexDirection: "column",
    marginLeft: "10px",
    marginBottom: "20px",
    marginTop: "10px",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "center",
    marginTop: "10px",
  },
  button: {
    fontSize: 65,
    color: "secondary",
  },
}));

// Interface utilisée par une "smallCard" ("component" du UI)
interface IProps {
  handleConfirmExtra: (extra: IExtra) => void;
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  handleConfirmVariante: (variante: IVariante) => void;
  type: string;
  nomItem: string;
  prixItem: number;
}

/**
 * Retourne un "component" de "smallCard" ("component" du UI)
 * @param handleConfirmExtra Méthode d'ajout d'un extra à un article.
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param handleConfirmVariante Méthode d'ajout d'une variante à une catégorie variante.
 * @param type Une valeur "string" pour indiquer pour quel "component" une "smallCard" est faite.
 * @param nomItem Le nom de l'item à mettre dans la "smallCard".
 * @param prixItem Le prix de l'item à mettre dans la "smallCard".
 */
export const AddSmallCard: React.FC<IProps> = ({
  handleConfirmExtra,
  handleConfirmContrainte,
  handleConfirmVariante,
  type,
  nomItem,
  prixItem,
}) => {
  const styles = useStyles();

  // État du "dialog"
  const [open, setOpen] = React.useState(false);

  // Gestion de l'ouverture et de la fermeture du "DialogExtra"
  const handleDialogExtra = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Card className={styles.card} elevation={3} square>
        <CardContent className={styles.cardContent}>
          <IconButton
            aria-label="add"
            onClick={handleDialogExtra}
            color="primary"
          >
            <AddOutlinedIcon className={styles.button} />
          </IconButton>
          <DialogSmallCard
            handleConfirmExtra={handleConfirmExtra}
            handleConfirmContrainte={handleConfirmContrainte}
            handleConfirmVariante={handleConfirmVariante}
            nomItem={nomItem}
            prixItem={prixItem}
            open={open}
            onClose={handleDialogExtra}
            editCard={false}
            type={type}
          />
        </CardContent>
      </Card>
    </React.Fragment>
  );
};
