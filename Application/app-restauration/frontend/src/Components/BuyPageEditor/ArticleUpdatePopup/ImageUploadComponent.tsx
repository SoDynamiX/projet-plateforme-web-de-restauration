import { Component } from "react";

// Material-UI
import { DropzoneArea } from "material-ui-dropzone"; //Source : https://www.npmjs.com/package/material-ui-dropzone

//Interface utlisé par le component de téléversement d'image
interface IProps {
  editCard: boolean;
}

export class ImageUploadComponent extends Component<IProps> {
  constructor(props: any) {
    super(props);
    this.state = {
      files: [],
    };
  }

  handleChange(files: any) {
    this.setState({
      files: files,
    });
  }
  render() {
    return (
      <DropzoneArea
        onChange={this.handleChange.bind(this)}
        //Si on modifie un article, l'image actuelle sera passé au component DropzoneArea, sinon, la proprieté est vide.
        initialFiles={
          this.props.editCard
            ? [
                "https://images.unsplash.com/photo-1455619452474-d2be8b1e70cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80",
              ]
            : []
        }
        acceptedFiles={["image/jpeg", "image/png", "image/bmp"]} //Le type d'image accepté
        dropzoneText="Glisser-déposer une image ou cliquer" // Le texte écrit dans la zone
        filesLimit={1} // La limite d'image qu'on peut téléverser
      />
    );
  }
}
