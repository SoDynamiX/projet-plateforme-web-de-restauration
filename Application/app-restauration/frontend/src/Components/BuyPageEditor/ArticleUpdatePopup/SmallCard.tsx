import React from "react";
import { DialogConfirmDelete } from "../DialogConfirmDelete";
import { DialogSmallCard } from "./DialogSmallCard";
import { IExtra, IContrainte, IVariante } from "../../interfaces";

// Material-UI
import Card from "@material-ui/core/Card";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditRoundedIcon from "@material-ui/icons/EditRounded";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  card: {
    width: "155px",
    display: "flex",
    flexDirection: "column",
    marginLeft: "10px",
    marginBottom: "20px",
    marginTop: "10px",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "left",
  },
  button: {
    paddingTop: "20",
  },
}));

// Interface utilisée pour une "smallCard" ("component" du UI)
interface IProps {
  handleConfirmExtra: (extra: IExtra) => void;
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  handleConfirmVariante: (variante: IVariante) => void;
  handleDeleteVariante: (variante: IVariante) => void;
  handleDeleteExtra: (extra: IExtra) => void;
  handleDeleteContrainte: (contrainte: IContrainte) => void;
  nomItem: string;
  prixItem: number;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  type: string;
}

/**
 * Retourne un "component" de "smallCard" ("component" du UI)
 * @param handleConfirmExtra Méthode d'ajout d'un extra à un article.
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param handleConfirmVariante Méthode d'ajout d'une variante à une catégorie variante.
 * @param handleDeleteVariante Méthode de suppression d'une variante d'une catégorie variante.
 * @param handleDeleteExtra Méthode de suppression d'un extra d'un article.
 * @param handleDeleteContrainte Méthode de suppression d'une contrainte d'un article
 * @param nomItem Le nom de l'item à mettre dans la "smallCard".
 * @param prixItem Le prix de l'item à mettre dans la "smallCard".
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param type Une valeur "string" pour indiquer pour quel "component" une "smallCard" est faite.
 */
export const SmallCard: React.FC<IProps> = ({
  handleConfirmExtra,
  handleConfirmContrainte,
  handleConfirmVariante,
  handleDeleteVariante,
  handleDeleteExtra,
  handleDeleteContrainte,
  nomItem,
  prixItem,
  setHasChanged,
  type,
}) => {
  const styles = useStyles();

  // États des "dialogs"
  const [open, setOpen] = React.useState(false);
  const [openDel, setOpenDel] = React.useState(false);

  // Gestion de l'ouverture et de la fermeture du "DialogSmallCard"
  const handleDialogSmallCard = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  // Gestion de l'ouverture et de la fermeture du "DialogConfirmDelete"
  const handleDialogDelete = () => {
    if (open) {
      setOpenDel(false);
    } else {
      setOpenDel(true);
    }
  };

  // À ajouter: la gestion du choix de "handleDelete" en fonction du "type"
  var deleteRequest;

  return (
    <React.Fragment>
      <Card className={styles.card} elevation={3} square>
        <CardContent className={styles.cardContent}>
          <Typography variant="subtitle1" component="h2">
            {nomItem}
          </Typography>
          <Typography variant="subtitle1" component="h2">
            ${prixItem}
          </Typography>
        </CardContent>
        <CardActions className={styles.button}>
          <IconButton onClick={handleDialogSmallCard}>
            <EditRoundedIcon color="primary" />
          </IconButton>
          <DialogSmallCard
            nomItem={nomItem}
            prixItem={prixItem}
            handleConfirmExtra={handleConfirmExtra}
            handleConfirmContrainte={handleConfirmContrainte}
            handleConfirmVariante={handleConfirmVariante}
            open={open}
            onClose={handleDialogSmallCard}
            editCard={true}
            type={type}
          />

          {/* Bouton de suppression */}
          <IconButton
            style={{ marginRight: "auto" }}
            onClick={handleDialogDelete}
            aria-label="delete"
          >
            <DeleteIcon color="secondary" />
          </IconButton>
          <DialogConfirmDelete
            open={openDel}
            onClose={handleDialogDelete}
            nom={nomItem}
            deleteRequest={deleteRequest}
            setHasChanged={setHasChanged}
          />
        </CardActions>
      </Card>
    </React.Fragment>
  );
};
