import React from "react";
import { ICategorie } from "../../interfaces";

// Material-UI
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";

// Interface utilisée pour le groupe de "RadioButton" pour le choix d'une catégorie
interface IProps {
  handleCategorieChecked: (categorieSelectionne: string) => void;
  currentCategorie: ICategorie;
  listeCategories: ICategorie[];
}

/**
 * Retourne un "component" de "RadioButton" pour le choix d'une catégorie pour un article.
 * @param handleCategorieChecked La valeur "string" du nom de la catégorie sélectionnée.
 * @param currentCategorie La catégorie en cours.
 * @param listeCategories La liste des catégories pour qui les "Radio Buttons" sont faits.
 */
export const CategorieRadioButton: React.FC<IProps> = ({
  handleCategorieChecked,
  currentCategorie,
  listeCategories,
}) => {
  const [value, setValue] = React.useState("1");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);

    // Envoie l'id du "RadioButton" selectionné
    handleCategorieChecked(event.target.value);
  };

  return (
    <FormControl component="fieldset">
      <RadioGroup
        name="categorie"
        defaultValue={currentCategorie.nom}
        onChange={handleChange}
      >
        {/* Liste des "RadioButtons" */}
        {listeCategories.map((categorie: ICategorie) => {
          return (
            <FormControlLabel
              value={categorie._id}
              control={<Radio />}
              label={categorie.nom}
            />
          );
        })}
      </RadioGroup>
    </FormControl>
  );
};
