import React from "react";
import { DialogCategorieVariante } from "./DialogCategorieVariante";
import { SmallCard } from "./SmallCard";
import { AddSmallCard } from "./AddSmallCard";
import { AccordionCategorieVariante } from "./AccordionCategorieVariante";
import {
  ICategorieVariante,
  IArticle,
  IExtra,
  IContrainte,
  IVariante,
} from "../../interfaces";

// Material-UI
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    overflow: "auto",
  },
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  button: {
    color: "secondary",
  },
  backButton: {
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
  labelFocused: {},
}));

// Interface utilisée pour la section 2 du dialogue d'ajout et de modification d'un article
interface IProps {
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  activeStep: number;
  handleBack: () => void;
  handleNext: any;
  handleConfirmExtra: (extra: IExtra) => void;
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  handleConfirmVariante: (variante: IVariante) => void;
  handleDeleteExtra: (extra: IExtra) => void;
  handleDeleteContrainte: (contrainte: IContrainte) => void;
  handleDeleteVariante: (variante: IVariante) => void;
  handleConfirmCategorieVariante: (
    categorieVariante: ICategorieVariante
  ) => void;
  isLastStep: boolean;
  articlePreview: IArticle;
}

/**
 * Retourne un "dialog" pour la section 2 du "StepperMenu".
 * @param activeStep La valeur de l'index du "StepperMenu"
 * @param handleBack Méthode de gestion lorsqu'on recule dans le "StepperMenu."
 * @param handleNext Méthode de gestion lorsqu'on avance dans le "StepperMenu."
 * @param isLastStep Pour savoir si on est à la fin du "StepperMenu".
 * @param handleConfirmExtra Méthode d'ajout d'un extra à un article.
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param handleConfirmVariante Méthode d'ajout d'une variante à une catégorie variante.
 * @param handleConfirmCategorieVariante Méthode d'ajout d'une catégorie variante à un article.
 * @param handleDeleteExtra Méthode de suppression d'un extra d'un article.
 * @param handleDeleteContrainte Méthode de suppression d'une contrainte d'un article
 * @param handleDeleteVariante Méthode de suppression d'une variante d'une catégorie variante.
 * @param articlePreview Article à prévisualiser pour afficher certains éléments existants.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 */
export const DialogContentSection2: React.FC<IProps> = ({
  activeStep,
  handleBack,
  handleNext,
  isLastStep,
  handleConfirmExtra,
  handleConfirmContrainte,
  handleConfirmVariante,
  handleConfirmCategorieVariante,
  handleDeleteExtra,
  handleDeleteContrainte,
  handleDeleteVariante,
  articlePreview,
  setHasChanged,
}) => {
  const styles = useStyles();

  // Set les listes avec les données de l'article en cours
  var listeExtra = articlePreview.listeExtra;
  var listeCategorieVariante = articlePreview.listeCategorieVariante;

  const categorieVariante: ICategorieVariante = {
    _id: "",
    nom: "",
    listeVariante: [],
    varianteMinimum: 0,
    varianteMaximum: 0,
  };

  // État du "dialog"
  const [open, setOpen] = React.useState(false);

  // Gestion de l'ouverture et de la fermeture du "DialogContentSection2"
  const handleDialogCategorie = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  return (
    <form>
      <Typography variant="h6" style={{ paddingBottom: "15px" }}>
        Extras
      </Typography>

      {/* Carte d'ajout d'un extra */}
      <Grid item xs container spacing={2}>
        <AddSmallCard
          handleConfirmExtra={handleConfirmExtra}
          handleConfirmContrainte={handleConfirmContrainte}
          handleConfirmVariante={handleConfirmVariante}
          nomItem={""}
          prixItem={0}
          type={"extra"}
        />

        {/* Carte pour chacun des extras */}
        {listeExtra.map((extra: IExtra) => {
          console.log(extra);
          return (
            <SmallCard
              handleDeleteExtra={handleDeleteExtra}
              handleDeleteContrainte={handleDeleteContrainte}
              handleDeleteVariante={handleDeleteVariante}
              key={extra._id}
              handleConfirmExtra={handleConfirmExtra}
              handleConfirmContrainte={handleConfirmContrainte}
              handleConfirmVariante={handleConfirmVariante}
              nomItem={extra.nom}
              prixItem={extra.prix}
              setHasChanged={setHasChanged}
              type={"extra"}
            />
          );
        })}
      </Grid>

      <Divider className={styles.section} />

      <Typography variant="h6" style={{ paddingBottom: "15px" }}>
        Catégories Variantes
      </Typography>

      {/* Liste des catégories variantes */}
      {listeCategorieVariante.map((categorieVariante: ICategorieVariante) => {
        return (
          <AccordionCategorieVariante
            handleDeleteVariante={handleDeleteVariante}
            handleDeleteContrainte={handleDeleteContrainte}
            handleDeleteExtra={handleDeleteExtra}
            key={categorieVariante._id}
            handleConfirmExtra={handleConfirmExtra}
            handleConfirmContrainte={handleConfirmContrainte}
            handleConfirmVariante={handleConfirmVariante}
            handleConfirmCategorieVariante={handleConfirmCategorieVariante}
            categorieVariante={categorieVariante}
            setHasChanged={setHasChanged}
          />
        );
      })}

      {/**Section 'Paper' contenant un bouton qui ouvre le dialogue d'ajout d'une catégorie variante */}
      <Paper>
        <IconButton onClick={handleDialogCategorie} aria-label="add">
          <AddOutlinedIcon className={styles.button} color="primary" />
        </IconButton>
        <DialogCategorieVariante
          open={open}
          onClose={handleDialogCategorie}
          handleConfirmCategorieVariante={handleConfirmCategorieVariante}
          editCard={false}
          categorieVariante={categorieVariante}
        />
      </Paper>

      {/**Bouton retour pour retourner à la section précédente */}
      <Button
        disabled={activeStep === 0}
        onClick={handleBack}
        className={styles.backButton}
      >
        Retour
      </Button>

      {/**Bouton suivant pour aller vers la section suivante */}
      <Button
        className={styles.backButton}
        variant="contained"
        color="primary"
        onClick={() => {
          handleNext();
        }}
      >
        {isLastStep ? "Finaliser" : "Suivant"}
      </Button>
    </form>
  );
};
