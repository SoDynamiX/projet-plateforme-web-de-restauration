import React from "react";
import { IArticle } from "../../interfaces";

// Material-UI
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(2),
  },
  card: {
    width: "50%",
    display: "flex",
    flexDirection: "column",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "left",
  },
  cardMedia: {
    paddingTop: "56.25%", // Ratio 16:9
  },
  cardAction: {
    marginTop: "auto",
    paddingTop: 0,
    paddingBottom: 7,
  },
}));

// Interface utilisée pour le "preview" d'une carte article
interface IProps {
  article: IArticle;
}

/**
 * Retourne un "component" de carte pour prévisualiser un article à la fin du "StepperMenu".
 * @param article L'article pour qui le "preview" est fait.
 */
export const PreviewArticleCard: React.FC<IProps> = ({ article }) => {
  const styles = useStyles();

  return (
    <div className={styles.root}>
      <Card className={styles.card}>
        <CardMedia
          className={styles.cardMedia}
          // Image temporaire en attendant le déploiement sur un serveur
          image="https://images.unsplash.com/photo-1455619452474-d2be8b1e70cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
        />

        <Box>
          <CardContent style={{ paddingBottom: 2 }}>
            <Typography gutterBottom variant="h5" align="left">
              {article.nom}
            </Typography>
            <Divider style={{ marginBottom: 5 }} />
            <Typography gutterBottom variant="subtitle1" align="left">
              {article.description}
            </Typography>
            <Typography gutterBottom variant="h6" align="left">
              ${article.prix}
            </Typography>
          </CardContent>
        </Box>
        <CardActions className={styles.cardAction}>
          <Button size="small" color="primary" variant="outlined">
            Sélectionner
          </Button>
        </CardActions>
      </Card>
    </div>
  );
};
