import React from "react";
import { CategorieRadioButton } from "./CategorieRadioButton";
import { RouteComponentProps } from "react-router-dom";
import {
  IArticle,
  ICategorie,
  IContrainte,
  IExtra,
  IVariante,
} from "../../interfaces";

// Material-UI
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { CardComponentContrainte } from "./CardComponentContrainte";
import { AddContrainteCard } from "./AddContrainteCard";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
  labelFocused: {},
}));

// Interface utilisée pour la section 3 du dialogue d'ajout et de modification d'un article
interface IProps {
  handleCategorieChecked: (categorieSelectionne: string) => void;
  currentCategorie: ICategorie;
  listeCategories: ICategorie[];
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  handleConfirmExtra: (extra: IExtra) => void;
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  handleConfirmVariante: (variante: IVariante) => void;
  activeStep: number;
  handleBack: () => void;
  handleNext: (values: any) => void;
  isLastStep: boolean;
  articlePreview: IArticle;
  routeProps: RouteComponentProps;
}

/**
 * Retourne un "dialog" pour la section 3 du "StepperMenu".
 * @param activeStep La valeur de l'index du "StepperMenu"
 * @param handleBack Méthode de gestion lorsqu'on recule dans le "StepperMenu."
 * @param handleNext Méthode de gestion lorsqu'on avance dans le "StepperMenu."
 * @param isLastStep Pour savoir si on est à la fin du "StepperMenu".
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param handleConfirmExtra Méthode d'ajout d'un extra à un article.
 * @param handleConfirmVariante Méthode d'ajout d'une variante à une catégorie variante.
 * @param handleCategorieChecked La valeur "string" de la catégorie en cours.
 * @param currentCategorie La catégorie en cours.
 * @param listeCategories La liste des catégories du menu.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param articlePreview Article à prévisualiser pour afficher certains éléments existants.
 * @param routeProps Les propriétés des routes de l'application.
 */
export const DialogContentSection3: React.FC<IProps> = ({
  activeStep,
  handleBack,
  handleNext,
  isLastStep,
  handleConfirmContrainte,
  handleConfirmExtra,
  handleConfirmVariante,
  handleCategorieChecked,
  currentCategorie,
  listeCategories,
  setHasChanged,
  articlePreview,
  routeProps,
}) => {
  const styles = useStyles();

  // Set les listes avec les données de l'article en cours
  var listeContrainte = articlePreview.listeContrainte;

  return (
    <form>
      <Typography variant="h6" style={{ paddingBottom: "15px" }}>
        Contraintes
      </Typography>

      {/* Carte pour chacune des contraintes */}
      <Grid item xs container spacing={2}>
        {listeContrainte.map((contrainte: IContrainte) => {
          return (
            <CardComponentContrainte
              key={contrainte._id}
              handleConfirmContrainte={handleConfirmContrainte}
              handleConfirmExtra={handleConfirmExtra}
              handleConfirmVariante={handleConfirmVariante}
              nomItem={contrainte.nom}
              prixItem={0}
              setHasChanged={setHasChanged}
              currentCategorie={currentCategorie}
              routeProps={routeProps}
              article={articlePreview}
              contrainte={contrainte}
            />
          );
        })}

        {/* Carte d'ajout d'une contrainte */}
        <AddContrainteCard
          handleConfirmContrainte={handleConfirmContrainte}
          nomItem={""}
          setHasChanged={setHasChanged}
        />
      </Grid>

      <Divider className={styles.section} />

      <Typography variant="h6" style={{ paddingBottom: "15px" }}>
        Categorie de l'article
      </Typography>

      <CategorieRadioButton
        handleCategorieChecked={handleCategorieChecked}
        currentCategorie={currentCategorie}
        listeCategories={listeCategories}
      />

      <Divider className={styles.section} />

      {/**Bouton retour pour retourner à la section précédente */}
      <Button disabled={activeStep === 0} onClick={handleBack}>
        Retour
      </Button>

      {/**Bouton suivant pour aller vers la section suivante */}
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          handleNext(articlePreview);
        }}
      >
        {isLastStep ? "Finaliser" : "Suivant"}
      </Button>
    </form>
  );
};
