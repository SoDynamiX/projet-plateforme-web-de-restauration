import React from "react";
import { StepperMenu } from "./StepperMenu";
import { RouteComponentProps } from "react-router";
import { IMenu, IArticle, ICategorie } from "../../interfaces";

// Material-UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import { TransitionProps } from "@material-ui/core/transitions";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
  labelFocused: {},
}));

// Animation du "pop-up"
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

// Interface utilisée par un "dialog" pour un article
interface IProps {
  editCard: boolean;
  menu: IMenu;
  currentCategorie: ICategorie;
  article: IArticle;
  open: boolean;
  onClose: () => void;
  routeProps: RouteComponentProps;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
}

/**
 * Retourne un "StepperMenu" pour un article.
 * @param editCard Pour savoir si on est en "Create" ou en "Update".
 * @param menu Le menu du restaurant.
 * @param article L'article pour qui le "dialog" est fait.
 * @param currentCategorie La catégorie en cours.
 * @param open Méthode d'ouverture du "DialogArticle".
 * @param onClose Méthode de fermeture du "DialogArticle".
 * @param routeProps Les propriétés des routes de l'application.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 */
export const DialogArticle: React.FC<IProps> = ({
  editCard,
  menu,
  article,
  currentCategorie,
  open,
  onClose,
  routeProps,
  setHasChanged,
}) => {
  const styles = useStyles();

  return (
    <form>
      <Dialog
        className={styles.dialog}
        open={open}
        onClose={onClose}
        fullWidth={true}
        TransitionComponent={Transition}
      >
        {/* Ouverture d'un "StepperMenu" pour l'article */}
        <DialogTitle>Modification d'un article</DialogTitle>
        <DialogContent style={{ overflowX: "hidden" }} dividers>
          <StepperMenu
            currentCategorie={currentCategorie}
            routeProps={routeProps}
            setHasChanged={setHasChanged}
            menu={menu}
            editCard={editCard}
            article={article}
            onClose={onClose}
          />
        </DialogContent>
      </Dialog>
    </form>
  );
};
