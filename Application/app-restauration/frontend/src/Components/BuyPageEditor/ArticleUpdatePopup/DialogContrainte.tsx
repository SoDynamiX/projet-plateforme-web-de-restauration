import React, { useState } from "react";
import { IContrainte } from "../../interfaces";

// Material-UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { TransitionProps } from "@material-ui/core/transitions";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
  labelFocused: {},
}));

// Animation du "pop-up"
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

// Interface utilisée pour un "dialog" d'une contrainte
interface IProps {
  open: boolean;
  onClose: () => void;
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  editCard: boolean;
  contrainte: IContrainte;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
}

/**
 * Retourne un "dialog" pour une contrainte
 * @param open Méthode d'ouverture du "DialogArticle".
 * @param onClose Méthode de fermeture du "DialogArticle".
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param editCard Pour savoir si on est en "Create" ou en "Update".
 * @param contrainte La contrainte pour qui le "dialog" est fait.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 */
export const DialogContrainte: React.FC<IProps> = ({
  open,
  onClose,
  handleConfirmContrainte,
  editCard,
  contrainte,
  setHasChanged,
}) => {
  const styles = useStyles();

  // Valeurs temporaires
  var nom = "";

  // "useState" pour enregistrer le contenu du champ de texte
  const [champNom, setChampNom] = useState("");

  // La contrainte qui sera envoyée dans la requête HTTP
  const contrainteTemporaire: IContrainte = {
    _id: contrainte._id,
    nom: champNom,
    cheminImage: contrainte.cheminImage,
  };

  return (
    <form>
      {/* Si en "update", on change la valeur du champ par le nom de la contrainte. */}
      {(() => {
        if (editCard) {
          nom = contrainte.nom;
        }
      })()}

      <Dialog
        className={styles.dialog}
        open={open}
        onClose={onClose}
        fullWidth={true}
        TransitionComponent={Transition}
      >
        {/* Champ de texte pour le nom */}
        <DialogTitle>Modification d'un item</DialogTitle>
        <DialogContent style={{ overflowX: "hidden" }} dividers>
          <Typography variant="h6" style={{ paddingBottom: "10px" }}>
            Informations
          </Typography>
          <TextField
            id="nom-article"
            label="Nom"
            fullWidth
            defaultValue={nom}
            margin="normal"
            className={styles.textField}
            onChange={(e) => setChampNom(e.target.value)}
            InputLabelProps={{
              shrink: true,
              classes: {
                root: styles.labelRoot,
                focused: styles.labelFocused,
              },
            }}
          />
        </DialogContent>

        {/* Boutons */}
        <DialogActions>
          <Button onClick={onClose}>Annuler</Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleConfirmContrainte(contrainteTemporaire);
              setHasChanged(true);
              onClose();
            }}
          >
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    </form>
  );
};
