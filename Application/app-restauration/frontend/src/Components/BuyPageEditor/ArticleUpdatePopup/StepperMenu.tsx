import React, { useEffect } from "react";
import { RouteComponentProps } from "react-router-dom";
import { PreviewArticleCard } from "./PreviewArticleCard";
import { DialogContentSection1 } from "./DialogContentSection1";
import { DialogContentSection2 } from "./DialogContentSection2";
import { DialogContentSection3 } from "./DialogContentSection3";
import {
  IArticle,
  ICategorie,
  ICategorieVariante,
  IContrainte,
  IExtra,
  IMenu,
  IVariante,
} from "../../interfaces";

// Material-UI
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  section: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  previewCard: {
    width: "100%",
  },
}));

// Classe servant à faire une requête HTTP "post" pour ajouter un article au menu
class PostHTTP {
  async post(url: string, article: IArticle) {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(article),
    });

    return response;
  }
}

// Classe servant à faire une requête HTTP "put" pour modifier un article du menu
class PutHTTP {
  async put(url: string, fields: any) {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(fields),
    });

    return response;
  }
}

// Classe servant à faire une requête HTTP "delete" pour supprimer un article du menu
class DeleteHTTP {
  async delete(url: string) {
    await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
    });
  }
}

/*
  * Classe servant à l'envoi de l'objet "temporaire" qui sera envoyé à la BD à 
    la fin des dialogues d'ajout et de modification d'un article
  */
class Article implements IArticle {
  _id: string;
  nom: string;
  description: string;
  cheminImage: string;
  prix: number;
  listeContrainte: IContrainte[];
  listeExtra: IExtra[];
  listeCategorieVariante: ICategorieVariante[];
  disponibilite: boolean;
  salleSeulement: boolean;

  constructor(articleValues: IArticle) {
    this._id = "";
    this.nom = articleValues.nom;
    this.description = articleValues.description;
    this.cheminImage = articleValues.cheminImage;
    this.prix = articleValues.prix;
    this.listeContrainte = articleValues.listeContrainte;
    this.listeExtra = articleValues.listeExtra;
    this.listeCategorieVariante = articleValues.listeCategorieVariante;
    this.disponibilite = articleValues.disponibilite;
    this.salleSeulement = articleValues.salleSeulement;
  }
}

/**
 * Code Material UI
 * Fonction servant à définir les étapes affichées en haut des dialogues
 */
function getSteps() {
  return [
    "Information générale",
    "Extras et Variantes",
    "Contraintes et Categorie",
  ];
}

/**
 * Fonction qui servira à naviguer à travers les différentes sections du 'Stepper Menu'
 * @param menu Le menu du restaurant.
 * @param stepIndex La valeur de l'index du "StepperMenu"
 * @param editCard Pour savoir si on est en "Create" ou en "Update".
 * @param handleBack Méthode de gestion lorsqu'on recule dans le "StepperMenu."
 * @param handleNext Méthode de gestion lorsqu'on avance dans le "StepperMenu."
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param handleConfirmExtra Méthode d'ajout d'un extra à un article.
 * @param handleConfirmVariante Méthode d'ajout d'une variante à une catégorie variante.
 * @param handleConfirmCategorieVariante Méthode d'ajout d'une catégorie variante à un article.
 * @param handleCategorieChecked Une valeur "string" de la catégorie sélectionnée dans le "StepperMenu".
 * @param isLastStep Pour savoir si on est à la fin du "StepperMenu".
 * @param articleValues L'article temporaire.
 * @param routeProps Les propriétés des routes de l'application.
 * @param currentCategorie La catégorie en cours.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param handleDeleteExtra Méthode de suppression d'un extra d'un article.
 * @param handleDeleteContrainte Méthode de suppression d'une contrainte d'un article
 * @param handleDeleteVariante Méthode de suppression d'une variante d'une catégorie variante.
 */
function getStepContent(
  menu: IMenu,
  stepIndex: number,
  editCard: boolean,
  handleBack: () => void,
  handleNext: (values: any) => void,
  handleConfirmContrainte: (contrainte: IContrainte) => void,
  handleConfirmExtra: (extra: IExtra) => void,
  handleConfirmVariante: (variante: IVariante) => void,
  handleConfirmCategorieVariante: (
    categorieVariante: ICategorieVariante
  ) => void,
  handleCategorieChecked: (categorie: string) => void,
  isLastStep: boolean,
  articleValues: IArticle,
  routeProps: RouteComponentProps,
  currentCategorie: ICategorie,
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>,
  handleDeleteExtra: (extra: IExtra) => void,
  handleDeleteContrainte: (contrainte: IContrainte) => void,
  handleDeleteVariante: (variante: IVariante) => void
) {
  switch (stepIndex) {
    case 0:
      return (
        <DialogContentSection1
          nomArticle={articleValues.nom}
          prixArticle={articleValues.prix}
          descriptionArticle={articleValues.description}
          editCard={editCard}
          activeStep={stepIndex}
          handleBack={handleBack}
          handleNext={handleNext}
          isLastStep={isLastStep}
        />
      );
    case 1:
      return (
        <DialogContentSection2
          activeStep={stepIndex}
          handleBack={handleBack}
          handleNext={handleNext}
          handleConfirmExtra={handleConfirmExtra}
          handleConfirmContrainte={handleConfirmContrainte}
          handleConfirmVariante={handleConfirmVariante}
          handleConfirmCategorieVariante={handleConfirmCategorieVariante}
          handleDeleteContrainte={handleDeleteContrainte}
          handleDeleteExtra={handleDeleteExtra}
          handleDeleteVariante={handleDeleteVariante}
          isLastStep={isLastStep}
          articlePreview={articleValues}
          setHasChanged={setHasChanged}
        />
      );
    case 2:
      return (
        <DialogContentSection3
          handleCategorieChecked={handleCategorieChecked}
          currentCategorie={currentCategorie}
          listeCategories={menu.listeCategorie}
          routeProps={routeProps}
          activeStep={stepIndex}
          handleBack={handleBack}
          handleNext={handleNext}
          isLastStep={isLastStep}
          handleConfirmContrainte={handleConfirmContrainte}
          handleConfirmExtra={handleConfirmExtra}
          handleConfirmVariante={handleConfirmVariante}
          articlePreview={articleValues}
          setHasChanged={setHasChanged}
        />
      );
  }
}

// Interface utilisée pour le "StepperMenu"
interface IProps {
  menu: IMenu;
  currentCategorie: ICategorie;
  article: IArticle;
  routeProps: RouteComponentProps;
  editCard: boolean;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  onClose: () => void;
}

/**
 * Retourne un "StepperMenu" pour un article.
 * @param menu Le menu du restaurant.
 * @param article L'article pour qui le "StepperMenu" est fait.
 * @param routeProps Les propriétés des routes de l'application.
 * @param editCard Pour savoir si on est en "Create" ou en "Update"
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param currentCategorie La catégorie en cours.
 * @param onClose Méthode de fermeture du "dialog" du "StepperMenu".
 */
export const StepperMenu: React.FC<IProps> = ({
  menu,
  article,
  routeProps,
  editCard,
  setHasChanged,
  currentCategorie,
  onClose,
}) => {
  const classes = useStyles();

  // URL modifié pour qu'il puisse être utilisé par nos requêtes
  let fixedURL = routeProps.match.url.replaceAll("/edit", "");

  // Liste vide pour initialiser les listes lorsqu'elles sont 'undefined'
  const listeVide: [] = [];

  // 'Hook' servant à savoir à quelle étape on est rendu dans le menu.
  const [activeStep, setActiveStep] = React.useState(0);

  // "useState" servant à savoir dans quel catégorie l'article créé va être ajouté
  const [categorieChoisie, setCategorieChoisie] = React.useState("");

  //Objet qui sera construit lorsque l'utilisateur remplira les dialogues d'ajout ou de modification
  //disponibilite, cheminImage et salleSeulement hardcoded à true temporairement du a des contraintes de temps.
  const [articleValues, setArticleValues] = React.useState({
    nom: article.nom || "",
    prix: article.prix || 0,
    description: article.description || "",
    listeContrainte: article.listeContrainte || [],
    listeExtra: article.listeExtra || [],
    listeCategorieVariante: article.listeCategorieVariante || [],
    _id: "",
    cheminImage: "C/uneImage",
    disponibilite: true,
    salleSeulement: true,
  });

  const steps = getSteps();

  /**
   * Handler utilisé lorsqu'on passe au dialogue suivant.
   * @param newValues *La nouvelle valeur provenant du dialogue qui sera ajouté à l'article temporaire
   */
  const handleNext = (newValues: any) => {
    setArticleValues({ ...articleValues, ...newValues });
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  /*
    Vérification pour quand on ajoute un extra, catégorie variante ou contrainte à partir d'un article qui n'a pas encore été créer
    On update pour qu'il y ait une liste vide au lieu d'un "undefined" (fait panter la fonction 'concat')
    */
  useEffect(() => {
    if (editCard) {
      setArticleValues({
        ...articleValues,
        listeExtra: listeVide,
        listeCategorieVariante: listeVide,
        listeContrainte: listeVide,
      });
    }
  }, [
    article.listeExtra,
    article.listeCategorieVariante,
    article.listeContrainte,
  ]);

  // Ajoute l'extra donné à la liste d'extras.
  const handleConfirmExtra = (extra: IExtra) => {
    setArticleValues((articleValues) => ({
      ...articleValues,
      listeExtra: articleValues.listeExtra.concat(extra),
    }));
  };

  // Ajoute la contrainte donnée à la liste de catégories variantes
  const handleConfirmCategorieVariante = (
    nouvelleCategorieVariante: ICategorieVariante
  ) => {
    setArticleValues((articleValues) => ({
      ...articleValues,
      listeCategorieVariante: articleValues.listeCategorieVariante.concat(
        nouvelleCategorieVariante
      ),
    }));
  };

  // Ajoute la variante donnée à la liste de variantes d'une catégorie variante
  const handleConfirmVariante = (
    nouvelleVariante: IVariante
    // categorieVariante: ICategorieVariante
  ) => {
    // var categorieVarianteChoisie = articleValues.listeCategorieVariante.find((categorieVarianteArticle) => categorieVarianteArticle === categorieVariante);

    // setArticleValues(() => ({
    //   articleValues.listeCategorieVariante.indexOf(categorieVarianteChoisie)
    // }));
    console.log(
      "Sorry, il faut encore implémenter les catégories :'(",
      nouvelleVariante
    );
  };

  // Ajoute la contraintes donnée à la liste de contraintes.
  const handleConfirmContrainte = (contrainte: any) => {
    setArticleValues((articleValues) => ({
      ...articleValues,
      listeContrainte: articleValues.listeContrainte.concat(contrainte),
    }));

    const postRequest = new PostHTTP();
    let response = postRequest
      .post(
        `http://localhost:5000${fixedURL}/categories/${currentCategorie._id}/articles/${article._id}/contraintes`,
        articleValues
      )
      .then(() => {
        setHasChanged(true);
      });
    console.log(response);
  };

  // Méthode de suppression d'un extra
  const handleDeleteExtra = async (extra: IExtra) => {
    const deleteRequest = new DeleteHTTP();
    await deleteRequest
      .delete(
        `http://localhost:5000${fixedURL}/categories/${currentCategorie._id}/articles/${article._id}/extras/${extra._id}`
      )
      .then(() => {
        setHasChanged(true);
      });
  };

  // Méthode de suppression d'une contrainte
  const handleDeleteContrainte = async (contrainte: IContrainte) => {
    const deleteRequest = new DeleteHTTP();
    await deleteRequest
      .delete(
        `http://localhost:5000${fixedURL}/categories/${currentCategorie._id}/articles/${article._id}/contraintes/${contrainte._id}`
      )
      .then(() => {
        setHasChanged(true);
      });
  };

  // Méthode de suppression d'une variante
  const handleDeleteVariante = async (variante: IVariante) => {
    // const deleteRequest = new DeleteHTTP();
    // await deleteRequest
    //   .delete(
    //     `http://localhost:5000${fixedURL}/categories/${currentCategorie._id}/articles/${article._id}/extras/${extra._id}`
    //   )
    //   .then(() => {
    //     setHasChanged(true);
    //   });
    console.log("Désolé, il faut implémenter ça :'(", variante);
  };

  /**
   * Handler utilisé lorsqu'on recule dans le 'Stepper Menu'
   */
  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  // Réinitialise le "stepper" pour qu'il reparte du début lors de sa prochaine utilisation
  const handleReset = () => {
    setActiveStep(0);
  };

  // set la valeur de la "checkbox" comme étant sélectionnée
  const handleCategorieChecked = (categorieSelectionne: string) => {
    setCategorieChoisie(categorieSelectionne);
  };

  // Méthode pour faire la requête HTTP "post" de l'article
  const handleConfirmFinal = async () => {
    const article = new Article({
      ...articleValues,
    });

    /**
     * Si le dialogue a été ouvert à partir de la carte article 'placeholder' pour ajouter un article, on envoie une requête "POST'".
     * Si le dialogue a été ouvert à partir du bouton de modification d'un article, on envoi une requête "PUT'.
     */
    if (!editCard) {
      const postRequest = new PostHTTP();
      await postRequest
        .post(
          `http://localhost:5000${fixedURL}/categories/${categorieChoisie}/articles`,
          article
        )
        .then(() => {
          setHasChanged(true);
          onClose();
        });
    } else {
      const fieldsToUpdate = {
        nom: article.nom,
        description: article.description,
        cheminImage: article.cheminImage,
        prix: article.prix,
        disponibilite: article.disponibilite,
        salleSeulement: article.salleSeulement,
      };
      const putRequest = new PutHTTP();
      await putRequest
        .put(
          `http://localhost:5000${fixedURL}/categories/${categorieChoisie}/articles/${article._id}`,
          fieldsToUpdate
        )
        .then(() => {
          setHasChanged(true);
          onClose();
        });
    }
  };

  const isLastStep = activeStep === steps.length - 1;

  return (
    /**
     * Les éléments suivants sont retourné à la dernière étape du "stepper Menu'", donc lorsque on est passé à travers 
       les 3 autres sections définies dans le "switch case" plus haut.
     */
    <div className={classes.root}>
      {console.log("article : ", articleValues)}
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div className={classes.previewCard}>
            <Typography variant="h6">Apercu de l'article</Typography>
            <Typography variant="subtitle1">
              <em>Confirmer pour sauvegarder les changements</em>
            </Typography>
            <PreviewArticleCard article={articleValues} />
            <Divider className={classes.section} />

            <Button
              disabled={activeStep === 0}
              onClick={handleReset}
              className={classes.backButton}
            >
              Retour
            </Button>

            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                handleConfirmFinal();
                setHasChanged(true);
                onClose();
              }}
            >
              Confirmer
            </Button>
          </div>
        ) : (
          <div>
            <Typography className={classes.instructions}>
              {getStepContent(
                menu,
                activeStep,
                editCard,
                handleBack,
                handleNext,
                handleConfirmContrainte,
                handleConfirmExtra,
                handleConfirmVariante,
                handleConfirmCategorieVariante,
                handleCategorieChecked,
                isLastStep,
                articleValues,
                routeProps,
                currentCategorie,
                setHasChanged,
                handleDeleteExtra,
                handleDeleteContrainte,
                handleDeleteVariante
              )}
            </Typography>
          </div>
        )}
      </div>
    </div>
  );
};
