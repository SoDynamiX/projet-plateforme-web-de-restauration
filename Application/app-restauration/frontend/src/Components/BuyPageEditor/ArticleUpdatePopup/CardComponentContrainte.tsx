import React from "react";
import { DialogConfirmDelete } from "../DialogConfirmDelete";
import { DialogSmallCard } from "./DialogSmallCard";
import { RouteComponentProps } from "react-router";
import {
  ICategorie,
  IArticle,
  IContrainte,
  IExtra,
  IVariante,
} from "../../interfaces";

// Material-UI
import Card from "@material-ui/core/Card";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditRoundedIcon from "@material-ui/icons/EditRounded";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  card: {
    width: "155px",
    display: "flex",
    flexDirection: "column",
    marginLeft: "10px",
    marginBottom: "20px",
    marginTop: "10px",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "left",
  },
  button: {
    paddingTop: "20",
  },
}));

// Classe servant à faire une requête HTTP "delete" pour supprimer une contrainte
class DeleteHTTP {
  async delete(url: string) {
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
    });
  }
}

// Interface utilisée par une carte de contrainte
interface IProps {
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  handleConfirmVariante: (variante: IVariante) => void;
  handleConfirmExtra: (extra: IExtra) => void;
  nomItem: string;
  prixItem: number;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  routeProps: RouteComponentProps;
  currentCategorie: ICategorie;
  article: IArticle;
  contrainte: IContrainte;
}
/**
 * Retourne un "component" de carte pour une contrainte.
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param handleConfirmVariante Méthode d'ajout d'une variante à une catégorie variante.
 * @param handleConfirmExtra Méthode d'ajout d'un extra à un article.
 * @param nomItem Le nom de l'item à mettre dans la "smallCard".
 * @param prixItem Le prix de l'item à mettre dans la "smallCard".
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param routeProps Les propriétés des routes de l'application.
 * @param currentCategorie La catégorie en cours.
 * @param article L'article pour qui les contraintes sont faites.
 * @param contrainte La contrainte.
 */

export const CardComponentContrainte: React.FC<IProps> = ({
  handleConfirmContrainte,
  handleConfirmVariante,
  handleConfirmExtra,
  nomItem,
  prixItem,
  setHasChanged,
  routeProps,
  currentCategorie,
  article,
  contrainte,
}) => {
  const styles = useStyles();

  // États des "dialogs"
  const [open, setOpen] = React.useState(false);
  const [openDel, setOpenDel] = React.useState(false);

  // URL modifié pour qu'il puisse être utilisé par nos requêtes
  let fixedURL = routeProps.match.url.replaceAll("/edit", "");

  const handleDelete = async () => {
    const deleteRequest = new DeleteHTTP();
    await deleteRequest.delete(
      `http://localhost:5000${fixedURL}/categories/${currentCategorie._id}/articles/${article._id}/contraintes/${contrainte._id}`
    );
  };

  // Gestion de l'ouverture et de la fermeture du "DialogContrainte"
  const handleDialogContrainte = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  // Gestion de l'ouverture et de la fermeture du "DialogConfirmDelete"
  const handleDialogDelete = () => {
    if (open) {
      setOpenDel(false);
    } else {
      setOpenDel(true);
    }
  };

  return (
    <React.Fragment>
      <Card className={styles.card} elevation={3} square>
        <CardContent className={styles.cardContent}>
          <Typography variant="subtitle1" component="h2">
            {nomItem}
          </Typography>
        </CardContent>
        <CardActions className={styles.button}>
          {/* Bouton de modification d'une contrainte */}
          <IconButton onClick={handleDialogContrainte}>
            <EditRoundedIcon color="primary" />
          </IconButton>
          <DialogSmallCard
            nomItem={nomItem}
            prixItem={prixItem}
            handleConfirmContrainte={handleConfirmContrainte}
            handleConfirmExtra={handleConfirmExtra}
            handleConfirmVariante={handleConfirmVariante}
            open={open}
            onClose={handleDialogContrainte}
            editCard={true}
            type={"contrainte"}
          />

          {/* Bouton de suppression d'une contrainte */}
          <IconButton
            style={{ marginRight: "auto" }}
            onClick={handleDialogContrainte}
            aria-label="delete"
          >
            <DeleteIcon color="secondary" />
          </IconButton>
          <DialogConfirmDelete
            open={openDel}
            onClose={handleDialogDelete}
            nom={nomItem}
            deleteRequest={handleDelete}
            setHasChanged={setHasChanged}
          />
        </CardActions>
      </Card>
    </React.Fragment>
  );
};
