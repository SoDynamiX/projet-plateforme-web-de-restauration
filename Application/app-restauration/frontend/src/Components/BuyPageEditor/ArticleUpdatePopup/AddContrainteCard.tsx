import React from "react";
import { DialogContrainte } from "./DialogContrainte";
import { IContrainte } from "../../interfaces";

// Material-UI
import Card from "@material-ui/core/Card";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  card: {
    width: "150px",
    display: "flex",
    flexDirection: "column",
    marginLeft: "10px",
    marginBottom: "20px",
    marginTop: "10px",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "center",
    marginTop: "10px",
  },
  button: {
    fontSize: 65,
    color: "secondary",
  },
}));

// Interface utilisée pour la création d'une nouvelle contrainte
interface IProps {
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  nomItem: string;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
}

/**
 * Retourne un "component" de carte pour une nouvelle catégorie.
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param nomItem Le nom de la catégorie à ajouter.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 */
export const AddContrainteCard: React.FC<IProps> = ({
  handleConfirmContrainte,
  nomItem,
  setHasChanged,
}) => {
  const styles = useStyles();

  // État du "dialog"
  const [open, setOpen] = React.useState(false);

  // La contrainte qui sera envoyée dans la requête HTTP
  const contrainteTemporaire: IContrainte = {
    _id: "",
    nom: nomItem,
    cheminImage: "",
  };

  // Gestion de l'ouverture et de la fermeture du "DialogContrainte"
  const handleDialogContrainte = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Card className={styles.card} elevation={3} square>
        <CardContent className={styles.cardContent}>
          <IconButton
            onClick={handleDialogContrainte}
            aria-label="add"
            color="primary"
          >
            <AddOutlinedIcon className={styles.button} />
          </IconButton>
          <DialogContrainte
            open={open}
            onClose={handleDialogContrainte}
            handleConfirmContrainte={handleConfirmContrainte}
            editCard={false}
            contrainte={contrainteTemporaire}
            setHasChanged={setHasChanged}
          />
        </CardContent>
      </Card>
    </React.Fragment>
  );
};
