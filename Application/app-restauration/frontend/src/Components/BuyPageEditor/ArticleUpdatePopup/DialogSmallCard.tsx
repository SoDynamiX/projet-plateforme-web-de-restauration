import React, { useState } from "react";
import { IExtra, IContrainte, IVariante } from "../../interfaces";

// Material-UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { TransitionProps } from "@material-ui/core/transitions";

const useStyles = makeStyles((theme) => ({
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
  labelFocused: {},
}));

// Animation du "pop-up"
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

// Interface utilisée pour un "dialog" d'une "SmallCard"
interface IProps {
  handleConfirmExtra: (extra: IExtra) => void;
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  handleConfirmVariante: (variante: IVariante) => void;
  nomItem: string;
  prixItem: number;
  open: boolean;
  onClose: () => void;
  editCard: boolean;
  type: string;
}

/**
 * Retourne un "dialog" pour une contrainte
 * @param handleConfirmExtra Méthode d'ajout d'un extra à un article.
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param handleConfirmVariante Méthode d'ajout d'une variante à une catégorie variante.
 * @param nomItem Le nom de l'item.
 * @param prixItem Le prix de l'item.
 * @param open Méthode d'ouverture du "DialogArticle".
 * @param onClose Méthode de fermeture du "DialogArticle".
 * @param editCard Pour savoir si on est en "Create" ou en "Update".
 * @param type Une valeur "string" pour indiquer pour quel "component" une "smallCard" est faite.
 */
export const DialogSmallCard: React.FC<IProps> = ({
  handleConfirmExtra,
  handleConfirmContrainte,
  handleConfirmVariante,
  nomItem,
  prixItem,
  open,
  onClose,
  editCard,
  type,
}) => {
  const styles = useStyles();

  // "useState" pour enregistrer le contenu des "TextBox"
  const [nomTemporaire, setNomTemporaire] = useState(nomItem);
  const [prixTemporaire, setPrixTemporaire] = useState(prixItem);

  // Variables temporaires
  var extraTemporaire: IExtra;
  var contrainteTemporaire: IContrainte;
  var varianteTemporaire: IVariante;

  // Selon le "type", on choisi les données qu'on veut envoyer à la "SmallCard"
  if (type == "extra") {
    if (editCard) {
      extraTemporaire = {
        _id: "",
        nom: nomTemporaire,
        prix: prixTemporaire,
      };
    } else {
      extraTemporaire = {
        _id: "",
        nom: "",
        prix: 0,
      };
    }
  } else if (type == "contrainte") {
    if (editCard) {
      contrainteTemporaire = {
        _id: "",
        nom: nomTemporaire,
        cheminImage: "",
      };
    } else {
      contrainteTemporaire = {
        _id: "",
        nom: "",
        cheminImage: "",
      };
    }
  } else if (type == "variante") {
    if (editCard) {
      varianteTemporaire = {
        _id: "",
        nom: nomTemporaire,
        prix: prixTemporaire,
      };
    } else {
      varianteTemporaire = {
        _id: "",
        nom: "",
        prix: 0,
      };
    }
  }

  return (
    <form>
      <Dialog
        className={styles.dialog}
        open={open}
        onClose={onClose}
        fullWidth={true}
        TransitionComponent={Transition}
      >
        <DialogTitle>Modification d'un item</DialogTitle>

        <DialogContent style={{ overflowX: "hidden" }} dividers>
          <Typography variant="h6" style={{ paddingBottom: "10px" }}>
            Informations
          </Typography>

          {/* Champ de texte pour le nom */}
          <TextField
            id="nom-item"
            label="Nom de l'item"
            defaultValue={nomItem}
            margin="normal"
            className={styles.textField}
            onChange={(e) => setNomTemporaire(e.target.value)}
            InputLabelProps={{
              shrink: true,
              classes: {
                root: styles.labelRoot,
                focused: styles.labelFocused,
              },
            }}
          />

          {/* Champ de texte pour le prix */}
          <TextField
            id="prix-item"
            label="Prix de l'item"
            defaultValue={prixItem}
            margin="normal"
            onChange={(e) => setPrixTemporaire(+e.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">$</InputAdornment>
              ),
            }}
            InputLabelProps={{
              shrink: true,
              classes: {
                root: styles.labelRoot,
                focused: styles.labelFocused,
              },
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Annuler</Button>
          <Button
            variant="contained"
            color="primary"
            //On set les nouvelles valeurs pour la construction de l'objet article temporaire
            onClick={() => {
              if (type == "extra") {
                handleConfirmExtra(extraTemporaire);
              } else if (type == "contrainte") {
                handleConfirmContrainte(contrainteTemporaire);
              } else if (type == "variante") {
                handleConfirmVariante(varianteTemporaire);
              }
            }}
          >
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    </form>
  );
};
