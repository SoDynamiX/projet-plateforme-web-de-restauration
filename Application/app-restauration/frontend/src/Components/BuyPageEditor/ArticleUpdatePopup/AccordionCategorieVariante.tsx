import React from "react";
import { SmallCard } from "./SmallCard";
import { AddSmallCard } from "./AddSmallCard";
import { DialogConfirmDelete } from "../DialogConfirmDelete";
import { DialogCategorieVariante } from "./DialogCategorieVariante";
import {
  ICategorieVariante,
  IVariante,
  IExtra,
  IContrainte,
} from "../../interfaces";

// Material-UI
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Grid from "@material-ui/core/Grid";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditRoundedIcon from "@material-ui/icons/EditRounded";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
  },
  accordionDetails: {},
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

// Interface utilisée pour l'accordéon de variantes
interface IProps {
  handleConfirmExtra: (extra: IExtra) => void;
  handleConfirmContrainte: (contrainte: IContrainte) => void;
  handleConfirmVariante: (variante: IVariante) => void;
  handleDeleteVariante: (variante: IVariante) => void;
  handleDeleteContrainte: (contrainte: IContrainte) => void;
  handleDeleteExtra: (extra: IExtra) => void;
  handleConfirmCategorieVariante: (
    categorieVariante: ICategorieVariante
  ) => void;
  categorieVariante: ICategorieVariante;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
}

/**
 * Retourne un "accordion" de catégorie variante.
 * @param handleConfirmExtra Méthode d'ajout d'un extra à un article.
 * @param handleConfirmContrainte Méthode d'ajout d'une contrainte à un article.
 * @param handleConfirmVariante Méthode d'ajout d'une variante à une catégorie variante.
 * @param handleConfirmCategorieVariante Méthode d'ajout d'une catégorie variante à un article.
 * @param handleDeleteVariante Méthode de suppression d'une variante d'une catégorie variante.
 * @param handleDeleteContrainte Méthode de suppression d'une contrainte d'un article
 * @param handleDeleteExtra Méthode de suppression d'un extra d'un article.
 * @param categorieVariante La catégorie variante pour qui l'accordéon est fait.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 */
export const AccordionCategorieVariante: React.FC<IProps> = ({
  handleConfirmExtra,
  handleConfirmContrainte,
  handleConfirmVariante,
  handleConfirmCategorieVariante,
  handleDeleteVariante,
  handleDeleteContrainte,
  handleDeleteExtra,
  categorieVariante,
  setHasChanged,
}) => {
  const styles = useStyles();

  // États des "dialogs"
  const [open, setOpen] = React.useState(false);
  const [openDel, setOpenDel] = React.useState(false);

  // Gestion de l'ouverture et de la fermeture du "DialogCategorieVariante"
  const handleDialogCategorieVariante = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  // Gestion de l'ouverture et de la fermeture du "DialogConfirmDelete"
  const handleDialogDelete = () => {
    if (open) {
      setOpenDel(false);
    } else {
      setOpenDel(true);
    }
  };

  // "Placeholder". Devrait être une méthode de suppression d'une catégorie variante.
  const handleDeleteCategorieVariante = () => {};

  return (
    <div>
      <Accordion className={styles.root}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          style={{ marginTop: "-12px", marginBottom: "-12px" }}
        >
          {categorieVariante.nom}
          {/* Bouton de modification d'une catégorie variante */}
          <IconButton
            style={{ marginLeft: "auto" }}
            onClick={handleDialogCategorieVariante}
          >
            <EditRoundedIcon color="primary" />
          </IconButton>

          <DialogCategorieVariante
            open={open}
            onClose={handleDialogCategorieVariante}
            editCard={true}
            categorieVariante={categorieVariante}
            handleConfirmCategorieVariante={handleConfirmCategorieVariante}
          />
          {/* Bouton de suppression d'une catégorie variante */}
          <IconButton onClick={handleDialogDelete} aria-label="delete">
            <DeleteIcon color="secondary" />
          </IconButton>
          <DialogConfirmDelete
            open={openDel}
            onClose={handleDialogDelete}
            deleteRequest={handleDeleteCategorieVariante}
            nom={categorieVariante.nom}
            setHasChanged={setHasChanged}
          />
        </AccordionSummary>
        <AccordionDetails className={styles.accordionDetails}>
          <Grid item xs container spacing={2}>
            {/* Cartes pour chacune des variantes */}
            {categorieVariante.listeVariante.map((variante: IVariante) => {
              return (
                <SmallCard
                  handleConfirmExtra={handleConfirmExtra}
                  handleConfirmContrainte={handleConfirmContrainte}
                  handleConfirmVariante={handleConfirmVariante}
                  handleDeleteVariante={handleDeleteVariante}
                  handleDeleteContrainte={handleDeleteContrainte}
                  handleDeleteExtra={handleDeleteExtra}
                  nomItem={variante.nom}
                  prixItem={variante.prix}
                  setHasChanged={setHasChanged}
                  type={"variante"}
                />
              );
            })}
            {/* Carte pour ajouter une variante */}
            <AddSmallCard
              handleConfirmExtra={handleConfirmExtra}
              handleConfirmContrainte={handleConfirmContrainte}
              handleConfirmVariante={handleConfirmVariante}
              nomItem={""}
              prixItem={0}
              type={"variante"}
            />
          </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};
