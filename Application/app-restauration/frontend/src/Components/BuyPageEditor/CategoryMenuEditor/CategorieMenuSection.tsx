import React from "react";
import { CategorieMenuCards } from "./CategorieMenuCards";
import { ICategorie } from "../../interfaces";
import { RouteComponentProps } from "react-router-dom";

// Material-UI
import CategoryRoundedIcon from "@material-ui/icons/CategoryRounded";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  root: {
    overflow: "auto",
    height: "97vh",
  },
  paper: {
    margin: theme.spacing(6, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  CategoryRoundedIcon: {
    margin: theme.spacing(0),
  },
  submit: {
    margin: theme.spacing(3, 0, 1),
  },
}));

// Interface utilisée pour la section d'affichage des catégories
interface IProps {
  listeCategories: ICategorie[];
  changementCategorie: (categorieSelectionne: ICategorie) => void;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  routeProps: RouteComponentProps;
}

/**
 * Retourne un "component" de section pour les catégories.
 * @param listeCategories La liste des catégories pour qui la section est faite.
 * @param changementCategorie Méthode de changement de la catégorie en cours.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param routeProps Les propriétés des routes de l'application.
 */
export const CategorieMenuSection: React.FC<IProps> = ({
  listeCategories,
  changementCategorie,
  setHasChanged,
  routeProps,
}) => {
  const styles = useStyles();

  return (
    <Box display={{ xs: "none", md: "block" }} className={styles.root}>
      <Grid container component="main">
        <CssBaseline />
        <Grid component={Paper} elevation={6} square>
          <div className={styles.paper}>
            <CategoryRoundedIcon
              className={styles.CategoryRoundedIcon}
              style={{ marginBottom: 15 }}
            ></CategoryRoundedIcon>
            <Typography
              component="h1"
              variant="h5"
              style={{ paddingBottom: 15 }}
            >
              Catégories
            </Typography>
            {/* Grille des catégories */}
            <CategorieMenuCards
              listeCategorie={listeCategories}
              changementCategorie={changementCategorie}
              setHasChanged={setHasChanged}
              routeProps={routeProps}
            />
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};
