import React from "react";
import { RouteComponentProps } from "react-router-dom";
import { DialogCategorie } from "./DialogCategorie";
import { ICategorie } from "../../interfaces";

// Material-UI
import Card from "@material-ui/core/Card";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  card: {
    width: "250px",
    marginLeft: "10px",
    marginBottom: "20px",
    marginTop: "10px",
    maxHeight: "110px",
    flexDirection: "column",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "center",
  },
  button: {
    fontSize: 55,
    color: "secondary",
  },
}));

// Classe servant à faire une requête HTTP "post" pour ajouter une catégorie au menu
class PostHTTP {
  async post(url: string, fields: any) {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(fields),
    });

    return response;
  }
}

// Interface utilisée pour la création d'une nouvelle catégorie
interface IProps {
  routeProps: RouteComponentProps;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
}

/**
 * Retourne un "component" de carte pour une nouvelle catégorie.
 * @param routeProps Les propriétés des routes de l'application.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 */
export const AddCardComponentCategorie: React.FC<IProps> = ({
  routeProps,
  setHasChanged,
}) => {
  const styles = useStyles();

  // Pour savoir quand le "dialog" de création de la catégorie est ouvert
  const [open, setOpen] = React.useState(false);

  // URL modifié pour qu'il puisse être utilisé par nos requêtes
  let fixedURL = routeProps.match.url.replaceAll("/edit", "");

  // Gestion de l'ouverture et de la fermeture du "DialogCategorie"
  const handleDialogCategorie = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  /**
   * Méthode servant à faire une requête HTTP "post" pour une catégorie.
   * @param categorie La catégorie pour qui la requête est faite.
   */
  const handleCreateCategorie = async (categorie: ICategorie) => {
    const postRequest = new PostHTTP();
    await postRequest
      .post(`http://localhost:5000${fixedURL}/categories/`, categorie)
      .then(() => {
        setHasChanged(true);
        setOpen(false);
      });
  };

  // Initialisation d'une catégorie vide pour la requête HTTP "post"
  const categorieVide: ICategorie = {
    _id: "",
    nom: "",
    cheminImage: "",
    listeArticle: [],
    listeHoraireCategorie: [],
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Card className={styles.card} elevation={3} square>
        <CardContent className={styles.cardContent}>
          <IconButton
            onClick={handleDialogCategorie}
            aria-label="add"
            color="primary"
          >
            <AddOutlinedIcon className={styles.button} />
          </IconButton>
          {/* "Dialog" de création de la catégorie */}
          <DialogCategorie
            open={open}
            onClose={handleDialogCategorie}
            setHasChanged={setHasChanged}
            categorie={categorieVide}
            editCard={false}
            request={handleCreateCategorie}
          />
        </CardContent>
      </Card>
    </React.Fragment>
  );
};
