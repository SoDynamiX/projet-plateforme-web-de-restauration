import React from "react";
import { ICategorie } from "../../interfaces";
import { CardComponentCategorie } from "./CardComponentCategorie";
import { AddCardComponentCategorie } from "./AddCardComponentCategorie";
import { RouteComponentProps } from "react-router-dom";

// Interface utilisée pour la section des cartes de catégories
interface IProps {
  listeCategorie: ICategorie[];
  changementCategorie: (categorieSelectionne: ICategorie) => void;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  routeProps: RouteComponentProps;
}

/**
 * Retourne un "component" de grille pour les cartes de catégories.
 * @param listeCategories La liste des catégories pour qui la grille est faite.
 * @param changementCategorie Méthode de changement de la catégorie en cours.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param routeProps Les propriétés des routes de l'application.
 */
export const CategorieMenuCards: React.FC<IProps> = ({
  listeCategorie,
  changementCategorie,
  setHasChanged,
  routeProps,
}) => {
  return (
    <div>
      {/* Carte pour ajouter une nouvelle catégorie */}
      <AddCardComponentCategorie
        routeProps={routeProps}
        setHasChanged={setHasChanged}
      />
      {/* Cartes des catégories */}
      {Object.keys(listeCategorie).map((key: any, index) => {
        return (
          <CardComponentCategorie
            key={index}
            categorie={listeCategorie[key]}
            changementCategorie={changementCategorie}
            setHasChanged={setHasChanged}
            routeProps={routeProps}
          />
        );
      })}
    </div>
  );
};
