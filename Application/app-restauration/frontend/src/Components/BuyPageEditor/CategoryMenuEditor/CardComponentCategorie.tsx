import React from "react";
import { ICategorie } from "../../interfaces";
import { RouteComponentProps } from "react-router-dom";
import { DialogConfirmDelete } from "../DialogConfirmDelete";
import { DialogCategorie } from "./DialogCategorie";

// Material-UI
import Card from "@material-ui/core/Card";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditRoundedIcon from "@material-ui/icons/EditRounded";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  card: {
    width: "250px",
    display: "flex",
    flexDirection: "column",
    marginLeft: "10px",
    marginBottom: "20px",
    marginTop: "10px",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "left",
  },
  button: {
    justifyContent: "center",
  },
}));

// Classe servant à faire une requête HTTP "put" pour modifier une catégorie du menu
class PutHTTP {
  async put(url: string, fields: any) {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(fields),
    });

    return response;
  }
}

// Classe servant à faire une requête HTTP "delete" pour supprimer une catégorie du menu
class DeleteHTTP {
  async delete(url: string) {
    await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
    });
  }
}

// Interface utilisée par une carte de catégorie
interface IProps {
  categorie: ICategorie;
  changementCategorie: (categorieSelectionne: ICategorie) => void;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  routeProps: RouteComponentProps;
}

/**
 * Retourne un "component" de carte pour une catégorie du menu.
 * @param categorie La catégorie pour qui la carte est créée.
 * @param changementCategorie Méthode de changement de la catégorie en cours.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param routeProps Les propriétés des routes de l'application.
 */
export const CardComponentCategorie: React.FC<IProps> = ({
  categorie,
  changementCategorie,
  setHasChanged,
  routeProps,
}) => {
  const styles = useStyles();

  const [open, setOpen] = React.useState(false);
  const [openDel, setOpenDel] = React.useState(false);

  // URL modifié pour qu'il puisse être utilisé par nos requêtes
  let fixedURL = routeProps.match.url.replaceAll("/edit", "");

  // Gestion de l'ouverture et de la fermeture du "DialogCategorie"
  const handleDialogCategorie = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  // Gestion de l'ouverture et de la fermeture du "DeleteConfirmDialog"
  const handleDialogDelete = () => {
    if (openDel) {
      setOpenDel(false);
    } else {
      setOpenDel(true);
    }
  };

  // Méthode de suppression de la catégorie
  const handleUpdateCategorie = async (categorieTemporaire: ICategorie) => {
    const fieldsToUpdate = {
      nom: categorieTemporaire.nom,
    };
    const putRequest = new PutHTTP();
    await putRequest
      .put(
        `http://localhost:5000${fixedURL}/categories/${categorie._id}`,
        fieldsToUpdate
      )
      .then(() => {
        setHasChanged(true);
        setOpen(false);
      });
  };

  // Méthode de suppression de la catégorie
  const handleDeleteCategorie = async () => {
    const deleteRequest = new DeleteHTTP();
    await deleteRequest
      .delete(`http://localhost:5000${fixedURL}/categories/${categorie._id}`)
      .then(() => {
        setHasChanged(true);
        setOpen(false);
      });
  };

  return (
    <React.Fragment>
      <Card className={styles.card} elevation={3} square>
        <CardContent className={styles.cardContent}>
          <Typography variant="h6" component="h2">
            {categorie.nom}
          </Typography>
        </CardContent>
        {/* Bouton de changement de catégorie */}
        <CardActions>
          <Button
            className={styles.button}
            size="small"
            color="primary"
            variant="outlined"
            onClick={() => changementCategorie(categorie)}
          >
            Voir Catégorie
          </Button>
          {/* Bouton de modification de la catégorie */}
          <IconButton onClick={handleDialogCategorie}>
            <EditRoundedIcon color="primary" />
          </IconButton>
          <DialogCategorie
            open={open}
            onClose={handleDialogCategorie}
            setHasChanged={setHasChanged}
            categorie={categorie}
            editCard={true}
            request={handleUpdateCategorie}
          />
          {/* Bouton de suppression de la catégorie */}
          <IconButton
            onClick={() => {
              handleDialogDelete();
            }}
            aria-label="delete"
          >
            <DeleteIcon color="secondary" />
          </IconButton>
          <DialogConfirmDelete
            open={openDel}
            onClose={handleDialogDelete}
            deleteRequest={handleDeleteCategorie}
            nom={categorie.nom}
            setHasChanged={setHasChanged}
          />
        </CardActions>
      </Card>
    </React.Fragment>
  );
};
