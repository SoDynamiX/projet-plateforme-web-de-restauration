import React, { useState } from "react";
import { ICategorie } from "../../interfaces";
import { RouteComponentProps } from "react-router-dom";

// Material-UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { TransitionProps } from "@material-ui/core/transitions";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  textField: {
    marginRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  section: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  labelRoot: {
    fontSize: 20,
    "&$labelFocused": {
      color: "primary",
    },
  },
}));

// Animation du "pop-up"
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

// Interface utilisée pour la fenêtre de création d'une catégorie
interface IProps {
  open: boolean;
  onClose: () => void;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  categorie: ICategorie;
  editCard: boolean;
  request: (categorie: ICategorie) => void;
}

/**
 * Retourne un "dialog" de modification d'une catégorie.
 * @param open Méthode d'ouverture de la fenêtre de création d'une catégorie.
 * @param onClose Méthode de fermeture de la fenêtre de création d'une catégorie.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param categorie La catégorie pour qui les modifications sont apportées.
 * @param editCard Pour différencier un "Create" d'un "Update".
 * @param request La requête HTTP à faire lors de la confirmation des informations de la catégorie.
 */
export const DialogCategorie: React.FC<IProps> = ({
  open,
  onClose,
  setHasChanged,
  categorie,
  editCard,
  request,
}) => {
  const styles = useStyles();

  // "useState" pour enregistrer le contenu des champs de texte
  const [champNom, setChampNom] = useState(categorie.nom);

  // La catégorie qui sera envoyée dans la requête HTTP
  const categorieTemporaire: ICategorie = {
    _id: categorie._id,
    nom: champNom,
    cheminImage: categorie.cheminImage,
    listeArticle: categorie.listeArticle,
    listeHoraireCategorie: categorie.listeHoraireCategorie,
  };

  return (
    <form>
      {/* Si en "update", on change la valeur du champ par le nom de la catégorie. */}
      {() => {
        if (editCard) {
          setChampNom(categorie.nom);
        }
      }}

      <Dialog
        className={styles.dialog}
        open={open}
        onClose={onClose}
        fullWidth={true}
        TransitionComponent={Transition}
      >
        <DialogTitle>Modification d'un item</DialogTitle>
        <DialogContent style={{ overflowX: "hidden" }} dividers>
          <Typography variant="h6" style={{ paddingBottom: "10px" }}>
            Informations
          </Typography>
          <TextField
            id="nom-article"
            label="Nom"
            fullWidth
            defaultValue={categorie.nom}
            margin="normal"
            className={styles.textField}
            onChange={(e) => setChampNom(e.target.value)}
            InputLabelProps={{
              shrink: true,
              classes: {
                root: styles.labelRoot,
              },
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Annuler</Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              request(categorieTemporaire);
              setHasChanged(true);
              onClose();
            }}
          >
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    </form>
  );
};
