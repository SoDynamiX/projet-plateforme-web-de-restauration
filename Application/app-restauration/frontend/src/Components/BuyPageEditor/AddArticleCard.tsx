import React from "react";
import { DialogArticle } from "./ArticleUpdatePopup/DialogArticle";
import { RouteComponentProps } from "react-router";
import { IArticle, ICategorie, IMenu } from "../interfaces";

// Material-UI
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "center",
    paddingBottom: 2,
  },
  button: {
    color: "secondary",
    height: 310,
    width: "50%",
  },
}));

// Interface utilisée pour un "dialog" d'ajout d'un article
interface IProps {
  routeProps: RouteComponentProps;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
  menu: IMenu;
  currentCategorie: ICategorie;
}

/**
 * Retourne un "dialog" pour l'ajout d'un article au menu.
 * @param routeProps Les propriétés des routes de l'application.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 * @param menu Le menu du restaurant.
 * @param currentCategorie La catégorie en cours.
 */
export const AddArticleCard: React.FC<IProps> = ({
  routeProps,
  setHasChanged,
  menu,
  currentCategorie,
}) => {
  const styles = useStyles();

  // État du "dialog"
  const [open, setOpen] = React.useState(false);

  // Article temporaire
  const article: IArticle = {
    _id: "",
    nom: "",
    description: "",
    cheminImage: "",
    prix: 0,
    listeContrainte: [],
    listeExtra: [],
    listeCategorieVariante: [],
    disponibilite: false,
    salleSeulement: false,
  };

  // Gestion de l'ouverture et de la fermeture du "ArticleFormComponent"
  const handleDialogCreate = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Grid className={styles.cardGrid} item xs={12} sm={6} md={3}>
        <Card className={styles.card}>
          <CardContent className={styles.cardContent}>
            <IconButton
              aria-label="add"
              onClick={handleDialogCreate}
              color="primary"
            >
              <AddOutlinedIcon className={styles.button} />
            </IconButton>
            <DialogArticle
              article={article}
              currentCategorie={currentCategorie}
              menu={menu}
              open={open}
              onClose={handleDialogCreate}
              routeProps={routeProps}
              setHasChanged={setHasChanged}
              editCard={false}
            />
          </CardContent>
        </Card>
      </Grid>
    </React.Fragment>
  );
};
