import React from "react";
import { IArticle, ICategorie, IMenu } from "../interfaces";
import { RouteComponentProps } from "react-router-dom";
import { DialogArticle } from "./ArticleUpdatePopup/DialogArticle";
import { DialogConfirmDelete } from "./DialogConfirmDelete";

// Material-UI
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Divider from "@material-ui/core/Divider";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditRoundedIcon from "@material-ui/icons/EditRounded";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardContent: {
    flexGrow: 1,
    textAlign: "left",
  },
  cardMedia: {
    paddingTop: "56.25%", // Ratio 16:9
  },
  cardAction: {
    marginTop: "auto",
    paddingTop: 0,
    paddingBottom: 7,
  },
}));

// Classe servant à faire une requête HTTP "delete" pour supprimer un article du menu
class DeleteHTTP {
  async delete(url: string) {
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
    });
  }
}

// Interface utilisée pour une carte d'article
interface IProps {
  article: IArticle;
  menu: IMenu;
  routeProps: RouteComponentProps;
  currentCategorie: ICategorie;
  setHasChanged: React.Dispatch<React.SetStateAction<boolean>>;
}

/**
 * Retourne un "component" de carte d'article.
 * @param article L'article pour qui la carte d'article est faite.
 * @param menu Le menu du restaurant.
 * @param routeProps Les propriétés des routes de l'application.
 * @param currentCategorie La catégorie en cours.
 * @param setHasChanged Méthode pour annoncer au menu qu'une modification a été apportée.
 */
export const CardComponentArticle: React.FC<IProps> = ({
  article,
  menu,
  routeProps,
  currentCategorie,
  setHasChanged,
}) => {
  const styles = useStyles();

  // États des "dialogs"
  const [open, setOpen] = React.useState(false);
  const [openDel, setOpenDel] = React.useState(false);

  // URL modifié pour qu'il puisse être utilisé par nos requêtes
  let fixedURL = routeProps.match.url.replaceAll("/edit", "");

  // Gestion de l'ouverture et de la fermeture du "ArticleFormComponent"
  const handleDialogUpdate = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  // Gestion de l'ouverture et de la fermeture du "DeleteConfirmDialog"
  const handleDialogDelete = () => {
    if (openDel) {
      setOpenDel(false);
    } else {
      setOpenDel(true);
    }
  };

  // Méthode de suppression d'un article
  const handleDelete = async () => {
    const deleteRequest = new DeleteHTTP();
    await deleteRequest
      .delete(
        `http://localhost:5000${fixedURL}/categories/${currentCategorie._id}/articles/${article._id}`
      )
      .then(() => {
        setHasChanged(true);
        setOpenDel(false);
      });
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Grid className={styles.cardGrid} item xs={12} sm={6} md={3}>
        <Card className={styles.card}>
          <CardMedia
            className={styles.cardMedia}
            // Image temporaire en attendant le déploiement sur un serveur
            image="https://images.unsplash.com/photo-1455619452474-d2be8b1e70cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
          />
          <Box>
            <CardContent style={{ paddingBottom: 2 }}>
              <Typography gutterBottom variant="h5" align="left">
                {article.nom}
              </Typography>
              <Divider style={{ marginBottom: 5 }} />
              <Typography gutterBottom variant="subtitle1" align="left">
                {article.description}
              </Typography>
              <Typography gutterBottom variant="h6" align="left">
                ${article.prix.toFixed(2)}
              </Typography>
            </CardContent>
          </Box>

          {/* Bouton de modification d'un article */}
          <CardActions className={styles.cardAction}>
            <IconButton onClick={handleDialogUpdate}>
              <EditRoundedIcon color="primary" />
            </IconButton>
            <DialogArticle
              article={article}
              menu={menu}
              currentCategorie={currentCategorie}
              open={open}
              onClose={handleDialogUpdate}
              routeProps={routeProps}
              setHasChanged={setHasChanged}
              editCard={true}
            />

            {/* Bouton de suppression d'un article */}
            <IconButton onClick={handleDialogDelete}>
              <DeleteIcon color="secondary" />
            </IconButton>
            <DialogConfirmDelete
              open={openDel}
              onClose={handleDialogDelete}
              nom={article.nom}
              deleteRequest={handleDelete}
              setHasChanged={setHasChanged}
            />
          </CardActions>
        </Card>
      </Grid>
    </React.Fragment>
  );
};
