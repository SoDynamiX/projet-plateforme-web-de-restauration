import React, { useState } from "react";
import { CategorieMenuSection } from "./CategoryMenuEditor/CategorieMenuSection";
import { AddArticleCard } from "./AddArticleCard";
import { RouteComponentProps } from "react-router-dom";
import { IMenu, ICategorie, IArticle } from "../interfaces";
import { CardComponentArticle } from "./CardComponentArticle";
import { NavbarComponent } from "../Navbar";
import { FooterComponent } from "../Footer";

// Material-UI
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import LinearProgress from "@material-ui/core/LinearProgress";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  articlesTopMargin: {
    marginTop: "30px",
  },
}));

export const BuyMenuEditor: React.FC<RouteComponentProps> = (
  routeProps: RouteComponentProps
) => {
  // Menu temporaire en attendant le chargement de celui du restaurant
  const menuVide: IMenu = {
    _id: "",
    nom: "",
    listeCategorie: [
      {
        _id: "",
        nom: "",
        cheminImage: "",
        listeArticle: [],
        listeHoraireCategorie: [],
      },
    ],
    disponibilite: false,
  };

  const styles = useStyles();
  const [unMenu, setUnMenu] = useState<IMenu>(menuVide);
  const [currentCategorie, setCurrentCategorie] = useState<ICategorie>(
    menuVide.listeCategorie[0]
  );
  const [initialSetup, setInitialSetup] = useState<boolean>(true);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [listeCategorie, setListeCategorie] = useState([] as ICategorie[]);

  // Pour savoir qui quelque chose à changer sur le menu (pour le refetch)
  const [hasChanged, setHasChanged] = useState<boolean>(false);

  // URL modifié pour qu'il puisse être utilisé par nos requêtes
  let fixedURL = routeProps.match.url.replaceAll("/edit", "");

  // "Hook" permettant d'aller chercher le menu dans la BD
  React.useEffect(() => {
    if (!unMenu.nom || hasChanged) {
      fetch(`http://localhost:5000${fixedURL}`)
        .then((response) => response.json())
        .then((data) => setUnMenu(data));
      setHasChanged(false);
    } else {
      if (initialSetup) {
        setCurrentCategorie(unMenu?.listeCategorie[0]);
        setInitialSetup(false);
      }
      setIsLoading(false);
      setListeCategorie(unMenu.listeCategorie);
    }
  }, [hasChanged, unMenu]);

  /**
   * Change la catégorie en cours.
   * @param categorie La catégorie qui a été sélectionnée.
   */
  const handleChangementCategorie = (categorie: ICategorie) => {
    setCurrentCategorie(categorie);
  };

  // Barre de chargement si la liste des articles n'est pas chargée
  if (isLoading) return <LinearProgress />;

  return (
    <div style={{ paddingTop: 0 }}>
      <NavbarComponent />
      <Grid container spacing={1} style={{ marginBottom: "20px" }}>
        <CategorieMenuSection
          listeCategories={listeCategorie}
          changementCategorie={handleChangementCategorie}
          setHasChanged={setHasChanged}
          routeProps={routeProps}
        />
        <Grid item xs>
          <Container className={styles.articlesTopMargin}>
            <Grid container spacing={5}>
              <AddArticleCard
                menu={unMenu}
                currentCategorie={currentCategorie}
                routeProps={routeProps}
                setHasChanged={setHasChanged}
              />
              {currentCategorie?.listeArticle.map((article: IArticle) => {
                return (
                  <CardComponentArticle
                    article={article}
                    menu={unMenu}
                    currentCategorie={currentCategorie}
                    routeProps={routeProps}
                    setHasChanged={setHasChanged}
                    key={article._id}
                  />
                );
              })}
            </Grid>
          </Container>
        </Grid>
      </Grid>
      <FooterComponent />
    </div>
  );
};
