import React from "react";
import { DetailsArticleComponent } from "./DialogArticle/DialogArticle";
import { IArticle } from "../interfaces";
import { IArticlePanier } from "./MainBuyMenu";

// Material-UI
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Divider from "@material-ui/core/Divider";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  grilleCarte: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  carte: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  contenuCarte: {
    flexGrow: 1,
    textAlign: "left",
    alignItems: "flex-end",
    justifyContent: "center",
  },
  mediaCarte: {
    paddingTop: "56.25%", // ratio 16:9
  },
  actionCarte: {
    paddingTop: 0,
    paddingBottom: 7,
    marginTop: "auto",
    alignItems: "center",
  },
}));

// Interface utilisée pour les cartes d'articles
interface IProps {
  article: IArticle;
  ajoutAuPanier: (articleSelectionne: IArticlePanier) => void;
}

/**
 * Retourne un "component" de carte d'article pour une page d'achat du menu
 * d'un restaurant.
 * @param article L'article pour qui la carte est créée
 * @param ajoutAuPanier Méthode d'ajout d'un article au panier
 */
export const CardComponentArticle: React.FC<IProps> = ({
  article,
  ajoutAuPanier,
}) => {
  const styles = useStyles();

  // Gestion de l'affichage de la carte d'article
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Grid className={styles.grilleCarte} item xs={12} sm={6} md={3}>
        <Card className={styles.carte}>
          <CardMedia
            className={styles.mediaCarte}
            // Image d'article temporaire en attendant le déploiement sur le serveur
            image="https://images.unsplash.com/photo-1455619452474-d2be8b1e70cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
          />
          <Box>
            <CardContent className={styles.contenuCarte}>
              <Typography gutterBottom variant="h5" align="left">
                {article.nom}
              </Typography>
              <Divider style={{ marginBottom: 5 }} />
              <Typography gutterBottom variant="subtitle1" align="left">
                {article.description}
              </Typography>
              <Typography gutterBottom variant="h6" align="left">
                ${article.prix.toFixed(2)}
              </Typography>
            </CardContent>
          </Box>
          <CardActions className={styles.actionCarte}>
            <Button
              size="small"
              color="primary"
              variant="outlined"
              onClick={handleClickOpen}
            >
              Sélectionner
            </Button>
            <DetailsArticleComponent
              open={open}
              onClose={handleClose}
              article={article}
              ajoutAuPanier={ajoutAuPanier}
            />
          </CardActions>
        </Card>
      </Grid>
    </React.Fragment>
  );
};
