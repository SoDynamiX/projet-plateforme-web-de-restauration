import React, { useState } from "react";
import { IMenu, ICategorie, IArticle, IVariante } from "../interfaces";
import { RouteComponentProps } from "react-router-dom";
import { CategorieSectionComponent } from "./CategoryMenu/CategorieMenuSection";
import { PanierComponent } from "./Panier";
import { CardComponentArticle } from "./CardComponentArticle";
import { NavbarComponent } from "../Navbar";
import { FooterComponent } from "../Footer";

// Material-UI
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import LinearProgress from "@material-ui/core/LinearProgress";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  margeCarteArticles: {
    marginTop: "30px",
    marginBottom: "30px",
  },
}));

// Interface utilisée pour les articles du panier
export interface IArticlePanier {
  article: IArticle;
  quantite: number;
  listeVariantes: IVariante[];
  prixTotal: number;
}

/**
 * Retourne un "component" de menu d'achat pour restaurant
 * @param routeProps Les propriétés des routes de l'application
 */
export const BuyMenu: React.FC<RouteComponentProps> = (
  routeProps: RouteComponentProps
) => {
  const styles = useStyles();

  const [unMenu, setUnMenu] = useState<IMenu>();
  const [enChargement, setEnChargement] = useState<boolean>(true);
  const [articlesPanier, setArticlesPanier] = useState([] as IArticlePanier[]);
  const [categorieActuelle, setCategorieActuelle] = useState<ICategorie>();
  const [listeCategorie, setListeCategorie] = useState([] as ICategorie[]);

  // "Hook" permettant d'aller chercher le menu dans la BD
  React.useEffect(() => {
    if (!unMenu?.listeCategorie) {
      fetch(`http://localhost:5000${routeProps.match.url}`)
        .then((response) => response.json())
        .then((data) => setUnMenu(data));
    } else {
      setEnChargement(false);
      setListeCategorie(unMenu.listeCategorie);
      setCategorieActuelle(unMenu?.listeCategorie[0]);
    }
  }, [unMenu]);

  /**
   * Retourne le nombre total d'articles dans le panier
   * @param articles Les articles dans le panier
   */
  const getTotalArticlesPanier = (articles: IArticlePanier[]) =>
    articles.reduce((acc: number, article) => acc + article.quantite, 0);

  /**
   * Méthode pour ajouter un article au panier. Si l'article est déjà dans le
   * panier, sa quantité sera plutôt incrémentée de un.
   * @param clickedItem L'article sélectionné à ajouter au panier
   */
  const handleAjoutAuPanier = (clickedItem: IArticlePanier) => {
    setArticlesPanier((prev) => {
      const isItemInCart = prev.find((article) => article === clickedItem);

      if (isItemInCart) {
        return prev.map((item) =>
          item === clickedItem ? { ...item, quantite: item.quantite + 1 } : item
        );
      }

      return [...prev, { ...clickedItem, quantite: 1 }];
    });
  };

  /**
   * Méthode pour enlever un article du panier. Si la quantité de l'article est
   * supérieure à "1", décrémente sa quantité.
   * @param id Le "ID" de l'article à supprimer du panier
   */
  const handleSupprimerDuPanier = (article: IArticlePanier) => {
    setArticlesPanier((prev) =>
      prev.reduce((acc, item) => {
        if (item === article) {
          if (item.quantite === 1) {
            return acc;
          } else {
            return [...acc, { ...item, quantite: item.quantite - 1 }];
          }
        } else {
          return [...acc, item];
        }
      }, [] as IArticlePanier[])
    );
  };

  /**
   * Méthode pour changer la catégorie à afficher sur le menu d'achat d'un
   * restaurant.
   * @param clickedItem La catégorie sélectionnée à afficher
   */
  const handleChangementCategorie = (clickedItem: ICategorie) => {
    setCategorieActuelle(clickedItem);
  };

  // Barre de chargement si la liste des articles n'est pas chargée
  if (enChargement) return <LinearProgress />;

  return (
    <div>
      <NavbarComponent />
      <Grid container spacing={1}>
        {/* Section Catégorie */}
        <CategorieSectionComponent
          routeProps={routeProps}
          listeCategories={listeCategorie}
          changementCategorie={handleChangementCategorie}
        />
        <Grid item xs>
          <Container>
            <Grid container spacing={5} className={styles.margeCarteArticles}>
              {/* Ajouter une validation pour les articles "salleSeulement" */}
              {categorieActuelle?.listeArticle.map((article: IArticle) => {
                return (
                  <CardComponentArticle
                    key={article._id}
                    article={article}
                    ajoutAuPanier={handleAjoutAuPanier}
                  />
                );
              })}
            </Grid>
          </Container>
        </Grid>
        {/* Section Panier */}
        <PanierComponent
          articlesPanier={articlesPanier}
          ajoutAuPanier={handleAjoutAuPanier}
          supprimerDuPanier={handleSupprimerDuPanier}
        />
      </Grid>
      <FooterComponent />
    </div>
  );
};
