import React from "react";
import { IArticlePanier } from "./MainBuyMenu";
import { ArticlePanierComponent } from "./ArticlePanier";

// Material-UI
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  paper: {
    margin: theme.spacing(6, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  iconePanier: {
    margin: theme.spacing(0),
  },
}));

// Interface qu'utilisera le "component" panier
interface IProps {
  articlesPanier: IArticlePanier[];
  ajoutAuPanier: (articleSelectionne: IArticlePanier) => void;
  supprimerDuPanier: (articleASupprimer: IArticlePanier) => void;
}

/**
 * Calcul le prix total du panier
 * @param articles Les articles dans le panier
 */
const calculPrixTotal = (articles: IArticlePanier[]) =>
  articles.reduce(
    (acc: number, article) => acc + article.quantite * article.prixTotal,
    0
  );

// Retourne un "component" de panier pour la page d'achat du menu d'un restaurant
export const PanierComponent: React.FC<IProps> = ({
  articlesPanier,
  ajoutAuPanier,
  supprimerDuPanier,
}) => {
  const styles = useStyles();

  return (
    <Box display={{ xs: "none", md: "block" }}>
      <Grid container component="main" justify={"flex-end"}>
        <CssBaseline />
        <Grid component={Paper} elevation={6} square>
          <div className={styles.paper}>
            <ShoppingCartIcon
              className={styles.iconePanier}
              style={{ marginBottom: 15 }}
            ></ShoppingCartIcon>
            <Typography
              component="h1"
              variant="h5"
              style={{ paddingBottom: 15 }}
            >
              Panier
            </Typography>
            {articlesPanier.length === 0 ? <p>Le panier est vide</p> : null}
            {articlesPanier.map((article) => {
              return (
                <ArticlePanierComponent
                  key={article.article._id}
                  article={article}
                  ajoutAuPanier={ajoutAuPanier}
                  supprimerDuPanier={supprimerDuPanier}
                />
              );
            })}
            <h2>Total: ${calculPrixTotal(articlesPanier).toFixed(2)}</h2>
            <Button
              color="primary"
              type="submit"
              variant="contained"
              style={{ marginTop: 15 }}
            >
              Checkout
            </Button>
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};
