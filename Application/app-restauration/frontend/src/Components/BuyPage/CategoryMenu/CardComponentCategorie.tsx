import React from "react";
import { ICategorie } from "../../interfaces";

// Material-UI
import Card from "@material-ui/core/Card";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import { RouteComponentProps } from "react-router-dom";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  carte: {
    width: "250px",
    display: "flex",
    flexDirection: "column",
    marginLeft: "10px",
    marginBottom: "20px",
    marginTop: "10px",
  },
  contenuCarte: {
    flexGrow: 1,
    textAlign: "left",
  },
  bouton: {
    justifyContent: "center",
  },
}));

// Interface utilisée pour les cartes de catégories
interface IProps {
  categorie: ICategorie;
  routeProps: RouteComponentProps;
  changementCategorie: (categorieSelectionne: ICategorie) => void;
}

/**
 * Retourne un "component" de carte de catégorie pour une page d'achat du menu
 * d'un restaurant.
 * @param categorie La catégorie pour qui faire la carte.
 * @param changementCategorie Méthode de changement de la catégorie en cours.
 */
export const CardComponentCategorie: React.FC<IProps> = ({
  categorie,
  changementCategorie,
  routeProps,
}) => {
  const styles = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Card className={styles.carte} elevation={3} square>
        <CardContent className={styles.contenuCarte}>
          <Typography variant="h6" component="h2">
            {categorie.nom}
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            className={styles.bouton}
            size="small"
            color="primary"
            variant="outlined"
            onClick={() => changementCategorie(categorie)}
          >
            Voir Catégorie
          </Button>
        </CardActions>
      </Card>
    </React.Fragment>
  );
};
