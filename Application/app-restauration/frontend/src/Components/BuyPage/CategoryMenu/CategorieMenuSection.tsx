import React from "react";
import { CategorieGridComponent } from "./CategorieMenuCards";
import { ICategorie } from "../../interfaces";

// Material-UI
import CategoryRoundedIcon from "@material-ui/icons/CategoryRounded";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import { RouteComponentProps } from "react-router-dom";

// Création des styles JSS
const useStyles = makeStyles((theme) => ({
  root: {
    overflow: "auto",
    height: "90vh",
  },
  paper: {
    margin: theme.spacing(6, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  iconeCategorie: {
    margin: theme.spacing(0),
  },
}));

// Interface utilisée pour la section des catégories du menu d'achat d'un restaurant
interface IProps {
  listeCategories: ICategorie[];
  routeProps: RouteComponentProps;
  changementCategorie: (categorieSelectionne: ICategorie) => void;
}

/**
 * Retourne un "component" de section pour l'affichage des catégories du menu
 * d'achat d'un restaurant.
 * @param listeCategorie La liste de catégorie pour qui faire des cartes.
 * @param changementCategorie Méthode de changement de la catégorie en cours.
 */
export const CategorieSectionComponent: React.FC<IProps> = ({
  listeCategories,
  changementCategorie,
  routeProps,
}) => {
  const styles = useStyles();

  return (
    <Box display={{ xs: "none", md: "block" }} className={styles.root}>
      <Grid container component="main">
        <CssBaseline />
        <Grid component={Paper} elevation={6} square>
          <div className={styles.paper}>
            <CategoryRoundedIcon
              className={styles.iconeCategorie}
              style={{ marginBottom: 15 }}
            ></CategoryRoundedIcon>
            <Typography
              component="h1"
              variant="h5"
              style={{ paddingBottom: 15 }}
            >
              Catégories
            </Typography>
            <CategorieGridComponent
              routeProps={routeProps}
              listeCategorie={listeCategories}
              changementCategorie={changementCategorie}
            />
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};
