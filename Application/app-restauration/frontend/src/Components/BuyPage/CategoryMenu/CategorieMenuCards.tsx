import React from "react";
import { RouteComponentProps } from "react-router-dom";
import { ICategorie } from "../../interfaces";
import { CardComponentCategorie } from "./CardComponentCategorie";

// Interface utilisée pour les cartes de catégories
interface IProps {
  listeCategorie: ICategorie[];
  routeProps: RouteComponentProps;
  changementCategorie: (categorieSelectionne: ICategorie) => void;
}

/**
 * Retourne un "component" de grille pour les cartes de catégories.
 * @param listeCategorie La liste de catégorie pour qui faire des cartes.
 * @param changementCategorie Méthode de changement de la catégorie en cours.
 */
export const CategorieGridComponent: React.FC<IProps> = ({
  listeCategorie,
  changementCategorie,
  routeProps,
}) => {
  return (
    <div>
      {Object.keys(listeCategorie).map((key: any, index) => {
        return (
          <CardComponentCategorie
            key={index}
            routeProps={routeProps}
            categorie={listeCategorie[key]}
            changementCategorie={changementCategorie}
          />
        );
      })}
    </div>
  );
};
