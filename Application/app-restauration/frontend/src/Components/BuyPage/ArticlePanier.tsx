import { IArticlePanier } from "./MainBuyMenu";
import { IExtra, IVariante, IArticle } from "../interfaces";

// Material-UI
import Button from "@material-ui/core/Button";

// Interface utilisée pour les cartes d'article du panier
interface IProps {
  article: IArticlePanier;
  ajoutAuPanier: (articleSelectionne: IArticlePanier) => void;
  supprimerDuPanier: (articleASupprimer: IArticlePanier) => void;
}

/**
 * Retourne un "component" de carte d'article pour une page d'achat du menu
 * d'un restaurant.
 * @param article L'article pour qui la carte est créée
 * @param ajoutAuPanier Méthode d'ajout d'un article au panier
 * @param supprimerDuPanier Méthode de suppression d'un article du panier
 */
export const ArticlePanierComponent: React.FC<IProps> = ({
  article,
  ajoutAuPanier,
  supprimerDuPanier,
}) => {
  return (
    <div>
      <h3>{article.article.nom}</h3>
      <div className="information">
        <p>Price: ${article.article.prix.toFixed(2)}</p>
        {article.listeVariantes.map((variante: IVariante) => {
          return (
            <p>
              {variante.nom}: {`$${variante.prix.toFixed(2)}`}
            </p>
          );
        })}
        <p>Total: ${(article.quantite * article.prixTotal).toFixed(2)}</p>
      </div>
      <div className="buttons">
        <Button
          size="small"
          disableElevation
          variant="contained"
          onClick={() => supprimerDuPanier(article)}
        >
          -
        </Button>
        <p>{article.quantite}</p>
        <Button
          size="small"
          disableElevation
          variant="contained"
          onClick={() => ajoutAuPanier(article)}
        >
          +
        </Button>
      </div>
    </div>
  );
};
