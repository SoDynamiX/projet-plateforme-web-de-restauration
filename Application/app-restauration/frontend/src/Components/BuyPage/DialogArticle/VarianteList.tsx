import React from "react";
import { ICategorieVariante, IVariante } from "../../interfaces";
import { ICategorieVarianteMenu } from "./VerticalTabs";

// Material-UI
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import List from "@material-ui/core/List";
import Paper from "@material-ui/core/Paper";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    marginBottom: "15px",
  },
}));

// Interface utilisée pour l'affichage des variantes d'une catégorie variantes
interface IProps {
  handleArticleComplet: () => void;
  categorieVarianteMenu: ICategorieVarianteMenu;
  setVariantesActuelles: React.Dispatch<React.SetStateAction<number>>;
  variantesTemporaires: IVariante[];
  setVariantesTemporaires: React.Dispatch<React.SetStateAction<IVariante[]>>;
  checked: string[];
  setChecked: React.Dispatch<React.SetStateAction<string[]>>;
}

/**
 * Retourne un "component" de carte d'article pour une page d'achat du menu
 * d'un restaurant.
 * @param categorieVarianteMenu La catégorie variante pour qui afficher les variantes.
 * @param setVariantesActuelles Méthode pour pouvoir changer le nombre "variantesActuelles".
 * @param variantesTemporaires Liste "buffer" des variantes sélectionnées pour un article.
 * @param setVariantesTemporaires Méthode pour pouvoir changer la liste "variantesTemporaires".
 * @param checked Liste contenant le nom des valeurs des "checkbox" de variantes cochées.
 * @param setChecked Méthode pour pouvoir changer la liste "checked".
 */
export const VarianteListComponent: React.FC<IProps> = ({
  handleArticleComplet,
  categorieVarianteMenu,
  setVariantesActuelles,
  variantesTemporaires,
  setVariantesTemporaires,
  checked,
  setChecked,
}) => {
  const styles = useStyles();

  /**
   * Méthode de gestion des fonctions qui doivent être lancées lorsqu'une
   * variante est ajoutée à un article.
   * @param variante La variante ajoutée pour qui la routine est lancée.
   */
  const handleAjoutVariante = (variante: IVariante) => {
    setVariantesTemporaires([...variantesTemporaires, { ...variante }]);
    setVariantesActuelles(categorieVarianteMenu.variantesActuelles + 1);
  };

  /**
   * Méthode de gestion des fonctions qui doivent être lancées lorsqu'une
   * variante est retirée d'un article.
   * @param variante La variante ajoutée pour qui la routine est lancée.
   */
  const handleSupprimerVariante = (variante: IVariante) => {
    setVariantesTemporaires(
      variantesTemporaires.filter(
        (varianteTemporaire) => varianteTemporaire.nom !== variante.nom
      )
    );
    setVariantesActuelles(categorieVarianteMenu.variantesActuelles - 1);
  };

  /**
   * Méthode de gestion des "checkbox" lorsque celle-ci sont manipulées.
   * @param valeur La valeur inscrite dans la "checkbox" sélectionnée.
   */
  const handleToggle = (valeur: string) => {
    const indexActuel = checked.indexOf(valeur);
    const nouveauChecked = [...checked];

    // Est-ce que l'article est coché?
    // Si non, le coche:
    if (indexActuel === -1) {
      handleAjoutVariante(
        categorieVarianteMenu.categorieVariante.listeVariante.filter(
          (variante) => variante.nom === valeur
        )[0]
      );
      nouveauChecked.push(valeur);
    }
    // Si oui, vérifie qu'il est coché pour le décocher:
    else {
      handleSupprimerVariante(
        categorieVarianteMenu.categorieVariante.listeVariante.filter(
          (variante) => variante.nom === valeur
        )[0]
      );
      nouveauChecked.splice(indexActuel, 1);
    }

    setChecked(nouveauChecked);
    handleArticleComplet();
  };

  /**
   * Méthode pour rendre inaccessibles les "checkbox" non-sélectionnées lorsque
   * le maximum de variantes a été atteint pour la catégorie variante donnée.
   * @param valeur La valeur inscrite dans la "checkbox".
   * @param categorieVariante La catégorie variante pour qui faire la validation.
   */
  const disableCheckbox = (
    valeur: string,
    categorieVariante: ICategorieVariante
  ) => {
    var nombreItem = 0;

    categorieVariante.listeVariante.map((variante: IVariante) => {
      if (checked.find((string) => string === variante.nom)) {
        nombreItem++;
      }
    });

    return (
      nombreItem >= categorieVariante.varianteMaximum &&
      checked.indexOf(valeur) === -1
    );
  };

  return (
    <List className={styles.root}>
      {handleArticleComplet()}
      {categorieVarianteMenu.categorieVariante.listeVariante.map(
        (variante: any) => {
          const labelId = `checkbox-list-label-${variante.nom}`;
          return (
            <Paper style={{ marginBottom: "10px" }} square elevation={3}>
              <ListItem
                key={variante.index}
                role={undefined}
                button
                onClick={() => {
                  handleToggle(variante.nom);
                }}
                disabled={disableCheckbox(
                  variante.nom,
                  categorieVarianteMenu.categorieVariante
                )}
              >
                {/* Icône de carré de la "checkbox" */}
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    checked={checked.indexOf(variante.nom) !== -1}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemIcon>
                {/* Valeur primaire et secondaire à afficher dans la "checkbox" */}
                <ListItemText
                  id={labelId}
                  primary={variante.nom}
                  secondary={`+${variante.prix.toFixed("2")}$`}
                />
              </ListItem>
            </Paper>
          );
        }
      )}
    </List>
  );
};
