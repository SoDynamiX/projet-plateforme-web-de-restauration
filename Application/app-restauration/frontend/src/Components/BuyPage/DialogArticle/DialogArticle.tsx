import React, { useState } from "react";
import {
  IArticle,
  IVariante,
  IContrainte,
  IExtra,
  ICategorieVariante,
} from "../../interfaces";
import { IArticlePanier } from "../MainBuyMenu";
import { VerticalTabsComponent } from "./VerticalTabs";

// Material-UI
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import { TransitionProps } from "@material-ui/core/transitions";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  dialog: {
    display: "flex",
    flexDirection: "column",
  },
  image: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "300px",
    objectFit: "cover",
    objectPosition: "0 50%",
  },
}));

// Méthode pour créer l'animation de la fenêtre de détails d'un article
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

// Interface utilisée pour les fenêtre de détails des articles
interface IProps {
  open: boolean;
  onClose: () => void;
  article: IArticle;
  ajoutAuPanier: (articleSelectionne: IArticlePanier) => void;
}

/**
 * Retourne un "component" de détails pour l'article d'un menu d'achat d'un
 * restaurant.
 * @param open Méthode d'ouverture de la fenêtre de détails
 * @param onClose Méthode de fermeture de la fenêtre de détails
 * @param article L'article pour qui les détails sont affichés
 * @param ajoutAuPanier Méthode d'ajout d'un article au panier
 */
export const DetailsArticleComponent: React.FC<IProps> = ({
  open,
  onClose,
  article,
  ajoutAuPanier,
}) => {
  const styles = useStyles();

  const [articleComplet, setArticleComplet] = useState<boolean>(false);
  const [variantesTemporaires, setVariantesTemporaire] = useState(
    [] as IVariante[]
  );
  const [extrasTemporaires, setExtrasTemporaires] = useState([] as IExtra[]);
  const [checked, setChecked] = useState([] as string[]);

  /**
   * Méthode pour valider que chacune des limites de variantes de chacunes des
   * catégories variantes sont respectées. Si c'est le cas, indique que
   * l'article est complet.
   */
  const handleArticleComplet = () => {
    var estComplet = true;

    article.listeCategorieVariante.map(
      (categorieVariante: ICategorieVariante) => {
        var nombreItem = 0;

        categorieVariante.listeVariante.map((variante: IVariante) => {
          if (checked.find((string) => string === variante.nom)) {
            nombreItem++;
          }
        });

        if (
          nombreItem < categorieVariante.varianteMinimum ||
          nombreItem > categorieVariante.varianteMaximum
        ) {
          estComplet = false;
        }
      }
    );

    setArticleComplet(estComplet);
  };

  /**
   * Méthode pour calculer le sous-total d'un article en incluant le prix des
   * variantes et des extras qui lui sont appliqués.
   * @param article L'article pour qui valider le prix.
   * @param listeVariantes La liste de variantes appliquées à l'article.
   * @param listeExtras La liste d'extras appliqués à l'article.
   */
  const handlePrixTotal = (
    article: IArticle,
    listeVariantes: IVariante[],
    listeExtras: IExtra[]
  ) => {
    return (
      listeVariantes.reduce((acc: number, variante) => acc + variante.prix, 0) +
      listeExtras.reduce((acc: number, extra) => acc + extra.prix, 0) +
      article.prix
    );
  };

  /**
   * Méthode pour faire l'affichage des contraintes d'un article.
   * @param article L'article pour qui les contraintes sont affichées.
   */
  const handleAffichageContraintes = (article: IArticle) => {
    if (article.listeContrainte.length >= 1) {
      return (
        <DialogContentText>
          Contraintes:{" "}
          {article.listeContrainte.map((contrainte: IContrainte) => {
            return contrainte.nom + ", ";
          })}
        </DialogContentText>
      );
    }
  };

  return (
    <div>
      <Dialog
        className={styles.dialog}
        open={open}
        onClose={() => {
          onClose();
          setVariantesTemporaire([]);
        }}
        fullWidth={true}
        TransitionComponent={Transition}
      >
        {/*Image d'article temporaire en attendant le déploiement sur le serveur*/}
        <img
          className={styles.image}
          src="https://images.unsplash.com/photo-1455619452474-d2be8b1e70cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
        />
        <DialogTitle>{article.nom}</DialogTitle>
        <DialogContent style={{ overflowY: "hidden" }} dividers>
          <DialogContentText>{article.description}</DialogContentText>
          {handleAffichageContraintes(article)}
        </DialogContent>
        {/* Ajout des "tabs" pour les catégories variantes */}
        <VerticalTabsComponent
          article={article}
          variantesTemporaires={variantesTemporaires}
          setVariantesTemporaires={setVariantesTemporaire}
          checked={checked}
          setChecked={setChecked}
          handleArticleComplet={handleArticleComplet}
        />
        {/* Bouton de confirmation d'ajout de l'article au panier */}
        <DialogActions>
          <Button
            autoFocus
            onClick={() => {
              ajoutAuPanier({
                article: article,
                quantite: 1,
                listeVariantes: variantesTemporaires,
                prixTotal: handlePrixTotal(
                  article,
                  variantesTemporaires,
                  extrasTemporaires
                ),
              });
              onClose();
              setVariantesTemporaire([]);
              setChecked([]);
            }}
            variant="contained"
            color="secondary"
            disabled={!articleComplet}
          >
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
