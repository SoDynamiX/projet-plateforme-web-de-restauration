import React from "react";

// Material-UI
import Card from "@material-ui/core/Card";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";

// Création des styles JSS
const useStyles = makeStyles(() => ({
  carte: {
    width: "150px",
    display: "flex",
    flexDirection: "column",
    marginLeft: "10px",
    marginBottom: "20px",
    marginTop: "10px",
  },
  contenuCarte: {
    flexGrow: 1,
    textAlign: "left",
  },
  bouton: {
    paddingTop: "20",
  },
}));

/**
 * Retourne un "component" de petite carte pour l'affichage d'extra (et
 * possiblement d'autres choses).
 * @param nom Le nom de la chose à afficher sur la petite carte.
 * @param prix Le prix de la chose à afficher sur la petite carte.
 */
export const SmallCardComponent: React.FC<any> = (nom, prix) => {
  const styles = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <Card className={styles.carte} elevation={3} square>
        <CardContent className={styles.contenuCarte}>
          <Typography variant="subtitle1" component="h2">
            {nom}
          </Typography>
          <Typography variant="subtitle1" component="h2">
            {`+${prix.toFixed("2")}$`}
          </Typography>
        </CardContent>
        <CardActions className={styles.bouton}>
          <Button size="small" color="primary" variant="outlined">
            Ajouter
          </Button>
        </CardActions>
      </Card>
    </React.Fragment>
  );
};
