import React, { useState } from "react";
import { SmallCardComponent } from "./SmallCard";
import { VarianteListComponent } from "./VarianteList";
import {
  IArticle,
  ICategorieVariante,
  IExtra,
  IVariante,
} from "../../interfaces";

// Material-UI
import { makeStyles, Theme } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import WarningIcon from "@material-ui/icons/Warning";

// Création des styles JSS
const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: 400,
    textAlign: "left",
    overflowX: "hidden",
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    minWidth: "150px",
  },
}));

// Truc de Material-UI
interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

// Truc de Material-UI
const TabPanel: React.FC<TabPanelProps> = ({
  children,
  value,
  index,
  ...other
}) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
};

// Truc de Material-UI
function a11yProps(index: any) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

// Interface utilisée pour les onglets des catégories variantes dans le menu
// d'achat d'un article
export interface ICategorieVarianteMenu {
  categorieVariante: ICategorieVariante;
  variantesActuelles: number;
}

// Interface utilisée pour les onglets des catégories variantes dans le menu
// d'achat d'un article
interface IProps {
  handleArticleComplet: () => void;
  article: IArticle;
  variantesTemporaires: IVariante[];
  setVariantesTemporaires: React.Dispatch<React.SetStateAction<IVariante[]>>;
  checked: string[];
  setChecked: React.Dispatch<React.SetStateAction<string[]>>;
}

/**
 * Retourne un "component" de carte d'article pour une page d'achat du menu
 * d'un restaurant.
 * @param article L'article pour qui afficher les catégories variantes.
 * @param variantesTemporaires Méthode pour pouvoir changer le nombre "variantesActuelles".
 * @param setVariantesTemporaires Liste "buffer" des variantes sélectionnées pour un article.
 * @param checked Liste contenant le nom des valeurs des "checkbox" de variantes cochées.
 * @param setChecked Méthode pour pouvoir changer la liste "checked".
 */
export const VerticalTabsComponent: React.FC<IProps> = ({
  handleArticleComplet,
  article,
  variantesTemporaires,
  setVariantesTemporaires,
  checked,
  setChecked,
}) => {
  const styles = useStyles();
  const [value, setValue] = useState(0);

  // Le nombre de variantes étant présentement cochées
  const [variantesActuelles, setVariantesActuelles] = useState(0);

  var currentIndex = 0;

  /**
   * Méthode de gestion des "tabs" lorsque celle-ci sont manipulées. Les
   * paramètres sont privés et n'ont pas à être appelé avec la fonction.
   */
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  /**
   * Méthode pour imprimer un texte indiquant le nombre de variantes à sélectionner.
   * @param varianteMinimum Le nombre minimum de variantes de la catégorie variante.
   * @param varianteMaximum Le nombre maximum de variantes de la catégorie variante.
   */
  function itemChoices(varianteMinimum: number, varianteMaximum: number) {
    if (varianteMinimum == 0) {
      return <p>Veuillez choisir jusqu'a {varianteMaximum} item.</p>;
    } else if (varianteMaximum == 1) {
      return <p>Veuillez choisir {varianteMaximum} item.</p>;
    } else {
      return (
        <p>
          Veuillez choisir entre {varianteMinimum} et {varianteMaximum} items.
        </p>
      );
    }
  }

  /**
   * Méthode pour ajouter une icône d'avertissement sur les catégories
   * variantes où un choix de variante est requis pour pouvoir ajouter
   * l'article au panier.
   * @param categorieVariante La catégorie variante où une icône pourrait être apposée.
   */
  const handleIconeRequis = (categorieVariante: ICategorieVariante) => {
    if (categorieVariante.varianteMinimum >= 1) {
      return <WarningIcon />;
    }
  };

  return (
    <div className={styles.root}>
      {/* Affichage des onglets de catégories variantes et de l'onglet d'extras */}
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={styles.tabs}
      >
        {article.listeCategorieVariante.map(
          (categorieVariante: ICategorieVariante, index: any) => {
            return (
              <Tab
                icon={handleIconeRequis(categorieVariante)}
                label={categorieVariante.nom}
                {...a11yProps(index)}
                key={categorieVariante._id}
              />
            );
          }
        )}
        <Tab label="Extras" {...a11yProps(2)} />
      </Tabs>

      {/* Affichage individuel des catégories variantes et listage des variantes */}
      {article.listeCategorieVariante.map(
        (categorieVariante: ICategorieVariante, index: any) => {
          currentIndex += 1;
          return (
            <TabPanel value={value} index={index} key={categorieVariante._id}>
              {itemChoices(
                categorieVariante.varianteMinimum,
                categorieVariante.varianteMaximum
              )}
              <VarianteListComponent
                handleArticleComplet={handleArticleComplet}
                categorieVarianteMenu={{
                  categorieVariante: categorieVariante,
                  variantesActuelles: variantesActuelles,
                }}
                setVariantesActuelles={setVariantesActuelles}
                variantesTemporaires={variantesTemporaires}
                setVariantesTemporaires={setVariantesTemporaires}
                checked={checked}
                setChecked={setChecked}
              />
            </TabPanel>
          );
        }
      )}

      {/* Affichage de l'onglet d'extra et listage des extras */}
      <TabPanel value={value} index={currentIndex + 1}>
        {article.listeExtra.map((extra: IExtra) => {
          return (
            <SmallCardComponent
              key={extra._id}
              nom={extra.nom}
              prix={extra.prix}
            />
          );
        })}
      </TabPanel>
    </div>
  );
};
