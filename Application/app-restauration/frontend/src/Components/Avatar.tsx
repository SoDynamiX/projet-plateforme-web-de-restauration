import React from "react";

// Material-UI
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Box, IconButton } from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle"; // REF : https://material-ui.com/components/material-icons/

// Retourne un "component" d'avatar pour les comptes utilisateurs
export const AvatarComponent: React.FC = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  // Gestion de la fermeture de la fenêtre de l'avatar
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box flexGrow={1} textAlign="right">
      <IconButton
        aria-controls="simple-menu"
        color="inherit"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <AccountCircleIcon />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>Profile</MenuItem>
        <MenuItem onClick={handleClose}>My account</MenuItem>
        <MenuItem onClick={handleClose}>Logout</MenuItem>
      </Menu>
    </Box>
  );
};
